<?php

/**
 * Page to create a new UW class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/enrol/wisc/create_ui.class.php');
require_once($CFG->dirroot.'/enrol/wisc/ui_components.php');
require_once($CFG->dirroot.'/enrol/wisc/accesslib.php');

if (!enrol_is_enabled('wisc')) {
    throw new Exception("WISC enrolment plugin not enabled");
}

$createid = optional_param('createid', false, PARAM_ALPHANUM);

$url = new moodle_url('/enrol/wisc/create.php');
$PAGE->set_url($url);
$PAGE->set_pagelayout('admin');

$cc = enrol_wisc_create_ui::load_controller($createid);
if (!$cc) {
    $cc = new enrol_wisc_controller($USER->id, enrol_wisc_controller::TYPE_COURSE);
}

// check security
require_login();
$capability = wisc_can_create_course();
if ($capability !== enrol_wisc_plugin::CREATECOURSE_LIMITED && $capability !== enrol_wisc_plugin::CREATECOURSE_FULL) {
    throw new Exception(get_string('nocoursecreatoraccess','enrol_wisc'));
}
$PAGE->set_context(context_system::instance());

$create = new enrol_wisc_create_ui($cc);
$create->process();
$create->save_controller();

$heading = get_string('createcourse', 'enrol_wisc');

$PAGE->set_title($heading.': '.$create->get_stage_name());
$PAGE->set_heading($heading);
$PAGE->navbar->add($create->get_stage_name());

$create->initialize_js($PAGE);

$renderer = $PAGE->get_renderer('enrol_wisc');

echo $OUTPUT->header();
echo $renderer->progress_bar($create->get_progress_bar());


if ($create->get_stage() == enrol_wisc_create_ui::STAGE_COMPLETE) {
    $create->execute();
}

echo $create->display();
unset($create);
echo $OUTPUT->footer();
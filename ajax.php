<?php

/**
 * AJAX callbacks for course creator
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/enrol/wisc/ra_datastore.class.php');

$term      = required_param('term', PARAM_INT);
$action    = required_param('action', PARAM_ACTION);

$PAGE->set_url(new moodle_url('/enrol/ajax.php', array('term'=>$term, 'action'=>$action)));
$PAGE->set_course($SITE);

require_login();
//require_capability('moodle/course:create', $context);
//require_sesskey();

echo $OUTPUT->header(); // send headers

$datastore = new wisc_ra_datastore();

$outcome = new stdClass;
$outcome->success = true;
$outcome->response = new stdClass;
$outcome->error = '';

switch ($action) {
    case 'getorgs':
        $outcome->response = $datastore->getSchoolCollegesMenu($term);
        asort($outcome->response);
        break;
    case 'getsubjects':
        $org = required_param('org', PARAM_ALPHA);
        $outcome->response = $datastore->getSubjectsMenu($term, $org);
        asort($outcome->response);
        break;
    case 'getcourses':
        $subject  = required_param('subject', PARAM_INT);
        $outcome->response = $datastore->getSubjectCoursesMenu($term, $subject);
        asort($outcome->response);
        break;
    default:
        throw new enrol_ajax_exception('unknowajaxaction');
}

header('Content-type: application/json');
echo json_encode($outcome);
die();

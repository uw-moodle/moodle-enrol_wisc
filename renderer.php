<?php

/**
 * Course creator UI renderer class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class enrol_wisc_renderer extends plugin_renderer_base {

    public function progress_bar(array $items) {
        foreach ( $items as &$item ) {
            $text = $item ['text'];
            unset ( $item ['text'] );
            if (array_key_exists ( 'link', $item )) {
                $link = $item ['link'];
                unset ( $item ['link'] );
                $item = html_writer::link ( $link, $text, $item );
            } else {
                $item = html_writer::tag ( 'span', $text, $item );
            }
        }
        return html_writer::tag ( 'div', join ( get_separator (), $items ), array ('class' => 'create_progress clearfix' ) );
    }

    /**
     * Displays the term selection page
     */
    public function render_creatorintro() {
        $output = '';
        $instructions = get_config('enrol_wisc', 'creatorintro' );
        if (!empty($instructions)) {
            $options = array('trusted' => true);
            $output .= $this->output->box ( format_text( get_config('enrol_wisc', 'creatorintro' ), FORMAT_HTML, $options), 'creatorintro');
        }
        return $output;
    }

    /**
     * Renders the roster associations object
     *
     * @param import_course_search $component
     * @return string
     */
    public function render_wisc_course_selector_base(wisc_course_selector_base $selector, $canapprove = false) {
        $groups = $selector->get_grouped_classes();
        $name = $selector->get_name ();

        $output = html_writer::start_tag ( 'div', array ('id' => $name . '_wrapper', 'class' => 'wiscselector' ) );
        $output .= html_writer::start_tag ( 'select', array ('id' => $name, 'name' => $name . '[]', 'multiple' => 'multiple', 'size' => 15 ) );

        // Populate the select.
        $status = array ();
        $output .= $this->helper_course_selector_output_options ( $groups, $status, false );
        $output .= html_writer::end_tag ( 'select' );
        if ($status ['haspending']) {
            $output .= html_writer::start_tag ( 'div');
            $output .= html_writer::tag ( 'span', get_string ( 'sectionpending_key', 'enrol_wisc' ), array ('class' => 'pending' ) );
            $output .= $this->output->help_icon ( 'pending', 'enrol_wisc' );
            $output .= html_writer::end_tag ( 'div' );
            if ($canapprove) {
                $output .= '<input type="submit" name="approve" id="approve" value="'.get_string('approve', 'enrol_wisc').'" />';
            }
        }
        $output .= html_writer::end_tag ( 'div' );
        return $output;
    }

    public function render_wisc_course_selector_summary_changes(wisc_course_selector_new $selector) {
        $changes = $selector->get_grouped_adds_and_removes();
        $output = '';
        $output .= html_writer::start_tag ( 'div', array ('class' => 'mappingstatic' ));
        $status = array();
        $haspending = false;
        if (!empty($changes->adds)) {
            $output .= html_writer::start_tag ( 'div', array('class'=>'mappingchange'));
            $output .= html_writer::tag ( 'div', get_string('mappingstoadd', 'enrol_wisc'), array('class'=>'mappingchange-title') );
            $output .= html_writer::start_tag ( 'div', array('class'=>'mappingchange-adds') );
            $output .= $this->render_grouped_classes_static($changes->adds, $status);
            $haspending |= $status ['haspending'];
            $output .= html_writer::end_tag ( 'div' );
            $output .= html_writer::end_tag ( 'div' );
        }
        if (!empty($changes->removes)) {
            $output .= html_writer::start_tag ( 'div', array('class'=>'mappingchange'));
            $output .= html_writer::tag ( 'div', get_string('mappingstoremove', 'enrol_wisc'), array('class'=>'mappingchange-title') );
            $output .= html_writer::start_tag ( 'div', array('class'=>'mappingchange-removes') );
            $output .= $this->render_grouped_classes_static($changes->removes, $status);
            $haspending |= $status ['haspending'];
            $output .= html_writer::end_tag ( 'div' );
            $output .= html_writer::end_tag ( 'div' );
        }
        if (!empty($changes->approves)) {
            $output .= html_writer::start_tag ( 'div', array('class'=>'mappingchange'));
            $output .= html_writer::tag ( 'div', get_string('mappingstoapprove', 'enrol_wisc'), array('class'=>'mappingchange-title') );
            $output .= html_writer::start_tag ( 'div', array('class'=>'mappingchange-approves') );
            $output .= $this->render_grouped_classes_static($changes->approves, $status);
            $haspending |= $status ['haspending'];
            $output .= html_writer::end_tag ( 'div' );
            $output .= html_writer::end_tag ( 'div' );
        }
        if (empty($changes->adds) && empty($changes->removes) && empty($changes->approves)) {
            $output .= html_writer::tag ( 'div', get_string('nochanges', 'enrol_wisc') );
        }
        $output .= html_writer::end_tag ( 'div' );
        if ($haspending) {
            $output .= html_writer::start_tag ( 'div', array('class' => 'pendingdisc') );
            $output .= html_writer::tag ( 'span', get_string ( 'sectionpending_key', 'enrol_wisc' ), array ('class' => 'pending' ) );
            $output .= $this->output->help_icon ( 'pending', 'enrol_wisc' );
            $output .= html_writer::end_tag ( 'div' );
        }
        return $output;
    }

    public function render_wisc_course_selector_summary(wisc_course_selector_new $selector) {
        $output = '';
        $status = array();
        $groups = $selector->get_grouped_classes();
        $output .= html_writer::start_tag ( 'div', array ('class' => 'mappingstatic' ));
        $output .= html_writer::start_tag ( 'div', array ('class' => 'mappingchange' ));
        $output .= $this->render_grouped_classes_static($groups, $status);
        $output .= html_writer::end_tag ( 'div' );
        $output .= html_writer::end_tag ( 'div' );
        if ($status['haspending']) {
            $output .= html_writer::start_tag ( 'div', array('class' => 'pendingdisc') );
            $output .= html_writer::tag ( 'span', get_string ( 'sectionpending_key', 'enrol_wisc' ), array ('class' => 'pending' ) );
            $output .= $this->output->help_icon ( 'pending', 'enrol_wisc' );
            $output .= html_writer::end_tag ( 'div' );
        }
        return $output;
    }

    /**
     * Renders an array of classes grouped by course
     */
    public function render_grouped_classes_static($groups, &$status) {
        $output = '';
        $output .= html_writer::start_tag ( 'ul', array('class' => 'courses') );
        // Populate the select.
        $status = array ();
        $output .= $this->helper_course_selector_output_options ( $groups, $status, true );
        $output .= html_writer::end_tag ( 'ul' );
        return $output;
    }

    public function render_wisc_course_mapper_base(wisc_course_mapper_base $mapper, moodle_url $action, $params = null, $previous = true) {
        $output = $this->output->box ( get_string ( 'wiscinfobox', 'enrol_wisc' ), 'wiscinfobox' );
        $output .= html_writer::start_tag ( 'div', array ('id' => 'wiscassoc' ) );
        $output .= html_writer::start_tag ( 'form', array ('id' => 'wiscassocform', 'method' => 'post', 'action' => $action->out_omit_querystring () ) );
        $output .= html_writer::empty_tag ( 'input', array ('type' => 'hidden', 'name' => '_wisc_mapper_form', 'value' => '1' ) );
        $output .= $this->helper_output_params ( $params );
        $output .= html_writer::start_tag ( 'div' );
        $output .= $this->helper_course_mapper_output ( $mapper );
        $output .= html_writer::end_tag ( 'div' );
        $output .= $this->helper_output_wizard_buttons (null, $previous);
        $output .= html_writer::end_tag ( 'form' );
        $output .= html_writer::end_tag ( 'div' );
        return $output;
    }

    /**
     * Displays the import course selector
     */
    public function render_import_course_selector(archive_course_search $courses, moodle_url $action, $params = null, $archiveid = null) {
        $archiverenderer = $this->page->get_renderer ( 'local_archive' );
        $output = html_writer::start_tag ( 'div', array ('class' => 'import-course-selector backup-restore' ) );
        $output .= html_writer::start_tag ( 'form', array ('method' => 'post', 'action' => $action->out () ) );
        $output .= html_writer::empty_tag ( 'input', array ('type' => 'hidden', 'name' => '_wisc_import_form', 'value' => '1' ) );
        $output .= $this->helper_output_params ( $params );
        $output .= html_writer::start_tag ( 'div', array ('class' => 'ics-existing-course backup-section' ) );
        $output .= $this->output->heading ( get_string ( 'importdatafrom' ), 2, array ('class' => 'header' ) );
        $output .= $archiverenderer->render_archive_course_search ( $courses, $archiveid );
        $output .= html_writer::end_tag ( 'div' );
        $output .= $this->helper_output_wizard_buttons ( null, true, true );
        $output .= html_writer::end_tag ( 'form' );
        $output .= html_writer::end_tag ( 'div' );
        return $output;
    }

    /**
     * Displays the import course selector choice (import / skip import)
     */
    public function render_import_choice(moodle_url $action, $params = null) {
        $output = $this->output->heading ( get_string ( 'askimportbox', 'enrol_wisc' ), 4 );
        $output .= html_writer::start_tag ( 'form', array ('method' => 'post', 'action' => $action->out () ) );
        $output .= html_writer::empty_tag ( 'input', array ('type' => 'hidden', 'name' => '_wisc_import_choice_form', 'value' => '1' ) );
        $output .= $this->helper_output_params ( $params );
        $output .= $this->helper_output_wizard_buttons ( get_string ( 'import' ), true, true, get_string ( 'skipimport', 'enrol_wisc' ) );
        $output .= html_writer::end_tag ( 'form' );
        return $output;
    }

    protected function helper_course_mapper_output(wisc_course_mapper_base $mapper) {
        $currentstr = get_string ( 'associations', 'enrol_wisc' );
        $currenthelp = $this->output->help_icon ( 'associations', 'enrol_wisc' );
        $currentselector = $this->render_wisc_course_selector_base ( $mapper->get_current_selector (), $mapper->can_approve_mappings());

        $potentialstr = get_string ( 'potentialassocs', 'enrol_wisc', \enrol_wisc\local\chub\timetable_util::get_term_name ( $mapper->get_term () ) );
        $potentialhelp = $this->output->help_icon ( 'potentialassocsbox', 'enrol_wisc' );
        $potentialselector = $this->render_wisc_course_selector_base ( $mapper->get_potential_selector () );

        $addcoursestr = get_string ( 'coursepickerbtn', 'enrol_wisc' );

        $addstr = get_string ( 'add' );
        $removestr = get_string ( 'remove' );
        $rarrowstr = $this->output->rarrow ();
        $larrowstr = $this->output->larrow ();

        $output = <<<EOF
            <table class="generaltable generalbox wiscassoctable boxaligncenter" summary="">
            <tr>
              <td id='existingcell'>
                  <p>
                    <label for="removeselect">$currentstr</label>
                    $currenthelp
                  </p>
              </td>
              <td>
              </td>
              <td id='potentialcell'>
                  <p>
                    <label for="addselect">$potentialstr</label>
                    $potentialhelp
                  </p>
              </td>
            </tr>
            <tr>
              <td id='existingcell'>
                  $currentselector
              </td>
              <td id='buttonscell'>
                <p class="arrow_button">
                    <input name="add" id="add" type="submit" value="$larrowstr &nbsp; $addstr" title="$addstr" /><br />
                    <input name="remove" id="remove" type="submit" value="$rarrowstr &nbsp; $removestr" title="$removestr" />
                </p>
              </td>
              <td id='potentialcell'>
                  $potentialselector
              </td>
            </tr>
            <tr>
              <td colspan=3>
                  <div class="coursepicker"><input name="addcourse" id="addcourse" type="submit" value="$addcoursestr" /></div>
              </td
            </tr>
            </table>
EOF;
        return $output;
    }

    protected function helper_output_params($params) {
        $output = html_writer::empty_tag ( 'input', array ('type' => 'hidden', 'name' => 'sesskey', 'value' => s ( sesskey () ) ) );
        if (is_array ( $params ) && count ( $params ) > 0) {
            foreach ( $params as $name => $value ) {
                $output .= html_writer::empty_tag ( 'input', array ('type' => 'hidden', 'name' => $name, 'value' => $value ) );
            }
        }
        return $output;
    }

    protected function helper_output_wizard_buttons($nextlabel = null, $previous = true, $skip = false, $skiplabel = null) {
        if (is_null ( $nextlabel )) {
            $nextlabel = get_string ( 'next' );
        }
        $output = html_writer::start_tag ( 'div', array ('class' => 'form-buttons') );
        if ($previous) {
            $output .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => get_string ( 'previous' ), 'name' => 'previous', 'class'=>'previousbutton' ) );
        }
        $output .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => $nextlabel, 'name' => 'next', 'class'=>'nextbutton form-submit' ) );
        if ($skip) {
            if (is_null ( $skiplabel )) {
                $skiplabel = get_string ( 'skipthisstage', 'enrol_wisc' );
            }
            $output .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => $skiplabel, 'name' => 'skipstage', 'class'=>'nextbutton' ) );
        }
        $output .= html_writer::empty_tag ( 'input', array ('type' => 'submit', 'value' => get_string ( 'cancel' ), 'name' => 'cancel', 'class' => 'confirmcancel cancelbutton' ) );
        $output .= html_writer::end_tag ( 'div' );
        return $output;
    }

    protected function helper_course_selector_output_options($groups, &$status, $static) {
        $status ['haspending'] = false;
        $status ['hasunavailable'] = false;
        // If $groups is empty, make a 'no matching classes' item.
        if (empty ( $groups )) {
            $groups = array (get_string ( 'none' ) => array () );
        }
        // Output each optgroup.
        ksort ( $groups ); // by name
        $output = '';
        foreach ( $groups as $coursename => $sections ) {
            $output .= $this->helper_course_selector_output_optgroup ( $coursename, $sections, $status, $static );
        }

        return $output;
    }

    protected function helper_course_selector_output_optgroup($coursename, $sections, &$status, $static) {
        $output = '';
        // Check if the optgroup label is really long because of a long subject
        // string
        if (strlen ( $coursename ) > 40 && ! $static) {
            $matches = array ();
            if (preg_match ( '/^([^0-9]+) *([0-9]+) *(.*)$/', $coursename, $matches )) {
                list ( , $subject, $number, $name ) = $matches;
                if (strlen ( $subject ) > 20) {
                    // put subject on its own line for readability
                    $output .= $this->helper_output_optgroup_start ( array ('label' => "$subject $number" ), $static );
                    $output .= $this->helper_output_optgroup_end ( $static );
                    $output .= $this->helper_output_optgroup_start ( array ('label' => "&#160;&#160;&#160;$name" ), $static );
                } else {
                    $output .= $this->helper_output_optgroup_start ( array ('label' => "$coursename" ), $static );
                }

            }
        } else {
            $output .= $this->helper_output_optgroup_start ( array ('label' => "$coursename" ), $static );
        }
        if (! empty ( $sections )) {
            $sorted = array ();
            foreach ( $sections as $section ) {
                $sorted [$section->sort] = $section;
            }
            ksort ( $sorted );
            foreach ( $sorted as $section ) {
                $name = $section->name;

                $classes = array ();
                if (! empty ( $section->pending )) {
                    $classes [] = 'pending';
                    $title = get_string ( 'sectionpending_title', 'enrol_wisc' );
                    $name = get_string ( 'sectionpendingname', 'enrol_wisc', $name );
                    $status ['haspending'] = true;
                }
                if (! empty ( $section->unavailable )) {
                    $classes [] = 'unavailable';
                    $title = get_string ( 'alreadyinuse_title', 'enrol_wisc' );
                    $name = get_string ( 'sectionunavailablename', 'enrol_wisc', $name );
                    $status ['hasunavailable'] = true;
                }

                $attributes = array ();
                if (! empty ( $section->disabled )) {
                    $attributes ['disabled'] = 'disabled';
                } else if ($section->new) {
                    $attributes ['selected'] = 'selected';
                }
                if (! empty ( $classes )) {
                    $attributes ['class'] = join ( ' ', $classes );
                }
                if (! empty ( $title )) {
                    $attributes ['title'] = $title;
                }
                //$attributes ['name'] = $name;
                $attributes ['value'] = $section->id;
                $output .= $this->helper_output_option ( s ( $name ), $attributes, $static );
            }
        } else {
            if (! $static) {
                $output .= $this->helper_output_option ( '&#160;', array ('disabled' => 'disabled' ), $static );
            }
        }
        $output .= $this->helper_output_optgroup_end ( $static );
        return $output;
    }

    protected function helper_output_optgroup_start($attributes, $static) {
        if (! $static) {
            $output = html_writer::start_tag ( 'optgroup', $attributes );
        } else {
            $label = $attributes ['label'];
            $output = html_writer::start_tag ( 'li', array ('class' => 'courses' ) );
            $output .= html_writer::tag ( 'span', $label, array ('class' => 'coursename' ) );
            $output .= html_writer::start_tag ( 'ul', array ('class' => 'sections' ) );
        }
        return $output;
    }
    protected function helper_output_optgroup_end($static) {
        if (! $static) {
            $output = html_writer::end_tag ( 'optgroup' );
        } else {
            $output = html_writer::end_tag ( 'ul' );
            $output .= html_writer::end_tag ( 'li' );
        }
        return $output;
    }
    protected function helper_output_option($contents, $attributes, $static) {
        if (! $static) {
            $output = html_writer::tag ( 'option', $contents, $attributes );
        } else {
            $liattributes = array ();
            if (! empty ( $attributes ['class'] )) {
                $liattributes ['class'] = $attributes ['class'];
            }
            $output = html_writer::tag ( 'li', $contents, $liattributes );
        }
        return $output;
    }

    /**
     * Display subject cohort permissions with buttons to edit them
     *
     * @param stdClass $permissions user cohort permissions
     * @return string html code
     */
    public function cohort_permissions_box($cohorts) {
        global $CFG, $DB;

        // display strings
        $stroperation = get_string('operation', 'enrol_wisc');
        $strcohort = get_string('cohort', 'enrol_wisc');

        $return = $this->output->heading(get_string('cohortpermissions', 'enrol_wisc'), 3, 'main', true);
        $return .= $this->output->box_start('generalbox');

        $return .= get_string('cohortpermissionhelp', 'enrol_wisc');

        $table = new html_table();
        $table->head = array($strcohort, $stroperation);
        $table->align = array('left', 'center');
        $table->width = '100%';
        $table->data = array();

        if (!empty($cohorts)) {
            foreach ($cohorts as $cohort) {
                $edit = "<a href=\"" . $CFG->wwwroot . "/enrol/wisc/cohort.php?cohortid=" . $cohort->id . "\">";
                $edit .= get_string('edit') . "</a>";

                $cohorturl = new moodle_url('/cohort/indev.php?id=' . $cohort->id);
                $cohortatag = html_writer::start_tag('a', array('href' => $cohorturl));
                $cohortatag .= $cohort->name;
                $cohortatag .= html_writer::end_tag('a');

                $row = array($cohortatag, $edit);
                $table->data[] = $row;
            }
            $return .= html_writer::table($table);
        } else {
            $return .= get_string('nocohorts', 'enrol_wisc');
        }

        $return .= $this->output->box_end();
        return $return;
    }


}

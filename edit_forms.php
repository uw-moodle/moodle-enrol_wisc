<?php

/**
 * Plugin forms
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/course/lib.php');

abstract class enrol_wisc_wizard_form extends moodleform {
    protected $ui;
    protected $params;

    function __construct($ui, $params, $action=null, $customdata=null, $method='post', $target='', $attributes=null, $editable=true) {
        $this->ui = $ui;
        $this->params = $params;
        parent::__construct($action, $customdata, $method, $target, $attributes, $editable);
    }

    /**
     * Add a cancel, next and previous button to the end of your form.
     */
    function add_wizard_action_buttons($cancel = true, $previous = true, $submitlabel=null, $submitclass='') {
        $mform = $this->_form;
        $params = $this->params;
        if (is_array($params) && count($params) > 0) {
            foreach ($params as $name=>$value) {
                $stage = $mform->addElement('hidden', $name, $value);
                $mform->setType($name, PARAM_RAW_TRIMMED);
            }
        }
        if (is_null($submitlabel)){
            $submitlabel = get_string('next');
        }
        $buttonarray=array();
        if ($previous) {
            $buttonarray[] = $mform->createElement('submit', 'previous', get_string('previous'), array('class'=>'previousbutton'));
            $mform->registerNoSubmitButton('previous');
        }
        $buttonarray[] = $mform->createElement('submit', 'submitbutton', $submitlabel, array('class'=>'nextbutton '.$submitclass));
        if ($cancel) {
            $buttonarray[] = $mform->createElement('cancel', 'cancel', get_string('cancel'), array('class'=>'cancelbutton confirmcancel'));
        }
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }

}

class enrol_wisc_term_form extends enrol_wisc_wizard_form {

    function definition() {
        $mform = $this->_form;
        list ($termmenu, $defaultterm, ) = enrol_wisc_create_ui_helper::get_term_menu($this->ui);
        if (empty($termmenu)) {
            throw new moodle_exception('noterms', 'enrol_wisc');
        }

        $title = get_string('selectaterm', 'enrol_wisc');
        $mform->addElement('header', '', $title);
        $mform->addElement('select', 'term', get_string('term', 'enrol_wisc'), $termmenu);
        $mform->setDefault('term', $defaultterm);
        $mform->addElement('html', get_string('term_desc', 'enrol_wisc'));
        $mform->addHelpButton('term', 'term', 'enrol_wisc');

        $this->add_wizard_action_buttons(true, false);
    }
}

class enrol_wisc_coursepicker_school_form extends enrol_wisc_wizard_form {
    function definition() {
        global $CFG;
        global $DB;

        $mform = $this->_form;

        $term   = $this->params['term'];

        $this->radatastore = wisc_ra_datastore::get();

        // exceptions here will be fatal
        $schools = $this->radatastore->getSchoolCollegesMenu($term);
        if (empty($schools)) {
            $schools = array(0=>get_string('noschools', 'enrol_wisc'));
        } else {
            $schools[0] = get_string('selectmenu', 'enrol_wisc');
        }
        $mform->addElement('header', 'header', get_string('addcourse', 'enrol_wisc'));
        $mform->addElement('select', 'school', get_string('schoolcollege', 'enrol_wisc'), $schools);
        $mform->setDefault('school', 0);
        $mform->addRule('school', null, 'required');

        $this->add_wizard_action_buttons(true, false);
        $mform->disabledIf('submitbutton', 'school', 'eq', 0);
    }
}

class enrol_wisc_coursepicker_subject_form extends enrol_wisc_wizard_form {
    function definition() {
        global $CFG;
        global $DB;

        $mform = $this->_form;

        $term   = $this->params['term'];
        $school = $this->params['school'];

        $this->radatastore = wisc_ra_datastore::get();

        // exceptions here will be fatal
        $subjects = $this->radatastore->getSubjectsMenu($term, $school);
        asort($subjects);
        if (empty($subjects)) {
            $subjects = array(0=>get_string('nosubjects', 'enrol_wisc'));
        } else {
            $subjects[0] = get_string('selectmenu', 'enrol_wisc');
        }
        $mform->addElement('header', 'header', get_string('addcourse', 'enrol_wisc'));
        $mform->addElement('select', 'subject', get_string('subject', 'enrol_wisc'), $subjects);
        $mform->setDefault('subject', 0);
        $mform->addRule('subject', null, 'required');

        $this->add_wizard_action_buttons(true, false);
        $mform->disabledIf('submitbutton', 'subject', 'eq', 0);
    }
}

class enrol_wisc_coursepicker_course_form extends enrol_wisc_wizard_form {
    function definition() {
        global $CFG;
        global $DB;

        $mform = $this->_form;

        $term     = $this->params['term'];
        $subject  = $this->params['subject'];

        $this->radatastore = wisc_ra_datastore::get();

        // exceptions here will be fatal
        $courses = $this->radatastore->getSubjectCoursesMenu($term, $subject);
        asort($courses);
        if (empty($courses)) {
            $courses = array(0=>get_string('nocourses', 'enrol_wisc'));
        } else {
            $courses[0] = get_string('selectmenu', 'enrol_wisc');
        }
        $mform->addElement('header', 'header', get_string('addcourse', 'enrol_wisc'));
        $mform->addElement('select', 'course', get_string('course', 'enrol_wisc'), $courses);
        $mform->setDefault('course', 0);
        $mform->addRule('course', null, 'required');

        $this->add_wizard_action_buttons(true, false);
        $mform->disabledIf('submitbutton', 'course', 'eq', 0);
    }
}

class enrol_wisc_create_course_settings_form extends enrol_wisc_wizard_form {
    protected $context;  // course category context for validation

    function definition() {
        global $CFG;

        $mform    = $this->_form;

        $settings = $this->_customdata['settings']; // this contains the default data of this form
        $catlist  = $this->_customdata['catlist']; // list of categories
        $edit     = $this->_customdata['edit']; // are we showing an editable form or a frozen form
        $editoroptions = $this->_customdata['editoroptions'];

/// form definition with new course defaults
//--------------------------------------------------------------------------------
        $mform->addElement('header','', get_string('general', 'form'));

        $mform->addElement('select', 'category', get_string('category'), $catlist);
        $mform->addHelpButton('category', 'coursecategory');
        $mform->addRule('category', get_string('missingcategory', 'enrol_wisc'), 'required', null, 'client');

        $mform->addElement('text','fullname', get_string('fullnamecourse'),'maxlength="254" size="60"');
        $mform->addHelpButton('fullname', 'fullnamecourse');
        $mform->addRule('fullname', get_string('missingfullname'), 'required', null, 'client');
        $mform->setType('fullname', PARAM_MULTILANG);

        $mform->addElement('text','shortname', get_string('shortnamecourse'),'maxlength="100" size="25"');
        $mform->addHelpButton('shortname', 'shortnamecourse');
        $mform->addRule('shortname', get_string('missingshortname'), 'required', null, 'client');
        $mform->setType('shortname', PARAM_MULTILANG);

        $courseformats = get_plugin_list('format');
        $formcourseformats = array();
        foreach ($courseformats as $courseformat => $formatdir) {
            $formcourseformats[$courseformat] = get_string('pluginname', "format_$courseformat");
        }
        $mform->addElement('select', 'format', get_string('format'), $formcourseformats);
        $mform->addHelpButton('format', 'format');

        for ($i=1; $i<=52; $i++) {
          $sectionmenu[$i] = "$i";
        }
        $mform->addElement('select', 'numsections', get_string('numberweeks'), $sectionmenu);

        $mform->addElement('date_selector', 'startdate', get_string('startdate'));
        $mform->addHelpButton('startdate', 'startdate');

        if (!$edit) {
            $mform->addElement('submit', 'editsettingsbtn', get_string('editcoursesettings', 'enrol_wisc'));
        }
        $mform->registerNoSubmitButton('editsettingsbtn');


        $mform->addElement('hidden', 'editsettings');
        $mform->setType('editsettings', PARAM_INT);
        $mform->setConstant('editsettings', $edit? 1: 0);

        // Description.
        $mform->addElement('header', 'descriptionhdr', get_string('description'));
        $mform->setExpanded('descriptionhdr');

        $mform->addElement('editor','summary_editor', get_string('coursesummary'), null, $editoroptions);
        $mform->addHelpButton('summary_editor', 'coursesummary');
        $mform->setType('summary_editor', PARAM_RAW);
        $mform->addRule('summary_editor', get_string('missingsummary', 'enrol_wisc'), 'required', null, 'client');

        $mform->addElement('header','', get_string('roles'));

        $taroles = enrol_wisc_create_ui_helper::get_tarole_menu();
        $mform->addElement('select', 'tarole', get_string('instancetarole', 'enrol_wisc'), $taroles);
        $mform->addHelpButton('tarole', 'instancetarole', 'enrol_wisc');

        $auditorroles = enrol_wisc_create_ui_helper::get_auditorrole_menu();
        $mform->addElement('select', 'auditorrole', get_string('instanceauditorrole', 'enrol_wisc'), $auditorroles);
        $mform->addHelpButton('auditorrole', 'instanceauditorrole', 'enrol_wisc');

        $mform->addElement('header','', get_string('availability'));
        $choices = array();
        $choices['0'] = get_string('courseavailablenot');
        $choices['1'] = get_string('courseavailable');
        $mform->addElement('select', 'visible', get_string('availability'), $choices);
        $mform->addHelpButton('visible', 'visible');

        $options = array(ENROL_INSTANCE_ENABLED  => get_string('yes'),
                         ENROL_INSTANCE_DISABLED => get_string('no'));
        $mform->addElement('select', 'enrol_guest_status_0', get_string('status', 'enrol_guest'), $options);
        $mform->addHelpButton('enrol_guest_status_0', 'status', 'enrol_guest');

//--------------------------------------------------------------------------------
        $this->add_wizard_action_buttons();
//--------------------------------------------------------------------------------

        if (!$edit) {
           $mform->freeze(array('fullname', 'shortname', 'format', 'numsections', 'startdate'));
        }

/// finally set the current form data
//--------------------------------------------------------------------------------
        $this->set_data($settings);
    }


/// perform some extra moodle validation
    function validation($data, $files) {
        global $DB, $CFG;

        $errors = parent::validation($data, $files);
        if ($foundcourses = $DB->get_records('course', array('shortname'=>$data['shortname']))) {
            if (!empty($data['id'])) {
                unset($foundcourses[$data['id']]);
            }
            if (!empty($foundcourses)) {
                foreach ($foundcourses as $foundcourse) {
                    $foundcoursenames[] = $foundcourse->fullname;
                }
                $foundcoursenamestring = implode(',', $foundcoursenames);
                $errors['shortname']= get_string('shortnametaken', '', $foundcoursenamestring);
            }
        }
        // UWMOODLE-857 - Categories can change during course creation, so this might turn out to be null.
        if (empty($data['category'])) {
            $errors['category']= get_string('missingcategory', 'enrol_wisc');
        }

        // We can't use the category to validate here because it may not exist.
        //$categorycontext = get_context_instance(CONTEXT_COURSECAT, $data['category']);
        //$errors = array_merge($errors, enrol_course_edit_validation($data, $categorycontext));

        return $errors;
    }

/// add extra button
    function add_action_buttons($cancel=true, $submitlabel=null, $showedit=false) {

        $mform = $this->_form;

        if (is_null($submitlabel)){
            $submitlabel = get_string('savechanges');
        }

        // elements in a row need a group
        $buttonarray = array();

        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', $submitlabel);
        if ($showedit) {
            $buttonarray[] = &$mform->createElement('submit', 'editsettings', get_string('editcoursesettings', 'enrol_wisc'));
        }
        $mform->registerNoSubmitButton('edit');

        if ($cancel) {
            $buttonarray[] = &$mform->createElement('cancel');
        }

        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->setType('buttonar', PARAM_RAW);
        $mform->closeHeaderBefore('buttonar');
    }
}

class enrol_wisc_create_instance_settings_form extends enrol_wisc_wizard_form {
    function definition() {
        global $CFG;

        $mform    = $this->_form;

        $mform->addElement('header','', get_string('roles'));
        $taroles = enrol_wisc_create_ui_helper::get_tarole_menu();
        $mform->addElement('select', 'tarole', get_string('instancetarole', 'enrol_wisc'), $taroles);
        $mform->addHelpButton('tarole', 'instancetarole', 'enrol_wisc');

        $auditorroles = enrol_wisc_create_ui_helper::get_auditorrole_menu();
        $mform->addElement('select', 'auditorrole', get_string('instanceauditorrole', 'enrol_wisc'), $auditorroles);
        $mform->addHelpButton('auditorrole', 'instanceauditorrole', 'enrol_wisc');

        $this->add_wizard_action_buttons();
    }
}

class enrol_wisc_create_review_base_form extends enrol_wisc_wizard_form {
    function definition() {
        global $CFG;

        $mform    = $this->_form;

        $settings    = $this->_customdata['settings'];
        $archive     = $this->_customdata['archive'];
        $archivedate = $this->_customdata['archivedate'];
        $mappinghtml = $this->_customdata['mappinghtml'];
        $message     = $this->_customdata['message'];

        $cc = $this->ui->get_controller();

        /// form definition
        //--------------------------------------------------------------------------------
        $mform->addElement('header','', get_string('confirm'));

        if (!empty($settings->fullname)) {
            $mform->addElement('static', 'static_fullname', get_string('fullnamecourse'), $settings->fullname);
        }
        $mform->addElement('static', 'static_mapping', get_string('rostermapping', 'enrol_wisc'), $mappinghtml);

        if ($archive !== false) {
            //$mform->addElement('header','import', get_string('archive', 'enrol_wisc'));
            $mform->addElement('static','archive', get_string('archivename', 'enrol_wisc'), $archive);
            $mform->addElement('static','archivedate', get_string('archivedate', 'enrol_wisc'), userdate($archivedate, '%m/%d/%y'));
        }

        $this->settings_definition();

        if ($archive !== false) {
            $mform->addElement('checkbox', 'customimport', get_string('customimport', 'enrol_wisc'));
            $mform->addHelpButton('customimport', 'customimport', 'enrol_wisc');
            $mform->setDefault('customimport', 0);
        }
        if (get_config('enrol_wisc', 'offlinecreate') && $cc->get_controllermode() == enrol_wisc_controller::TYPE_COURSE) {
            $mform->addElement('checkbox', 'backgroundcreate', get_string('backgroundcreate', 'enrol_wisc'));
            $mform->setDefault('backgroundcreate', 1);
            $mform->addHelpButton('backgroundcreate', 'backgroundcreate', 'enrol_wisc');
            $mform->disabledIf('backgroundcreate', 'customimport', 'checked');
        }

        // Notification options
        $notifyusers = enrol_wisc_controller_helper::get_teachers($cc);

        $mform->addElement('header','notifysection', get_string('notifications', 'enrol_wisc'));
        $mform->addElement('checkbox', 'notify', get_string('notifyteachers', 'enrol_wisc'));
        $mform->setDefault('notify', 0);

        if (!empty($notifyusers)) {
            $mform->addElement('static', 'static_notifyspecifiedusers', get_string('notifyspecifiedusers', 'enrol_wisc'));
            $mform->setAdvanced('static_notifyspecifiedusers');
        }
        foreach ($notifyusers as $userid => $name) {
            $field = 'notify_'.$userid;
            $mform->addElement('checkbox', $field, $name);
            $mform->setDefault($field, 1);
            $mform->setAdvanced($field);
            $mform->disabledIf($field, 'notify');
        }

        $mform->addElement('textarea','message', get_string('notifymessage', 'enrol_wisc'), array('rows'=> '6', 'cols'=>'70'));
        $mform->setType('message', PARAM_MULTILANG);
        $mform->setDefault('message', $message);
        $mform->setAdvanced('message');
        $mform->disabledIf('message', 'notify');

        if ($cc->get_controllermode() == enrol_wisc_controller::TYPE_COURSE) {
            $submitlabel = get_string('createnewcourse', 'enrol_wisc');
        } else if ($cc->get_controllermode() == enrol_wisc_controller::TYPE_INSTANCE){
            $submitlabel = get_string('createnewinstance', 'enrol_wisc');
        } else if ($cc->get_controllermode() == enrol_wisc_controller::TYPE_EDIT){
            $submitlabel = get_string('updateinstance', 'enrol_wisc');
        } else {
            throw new moodle_exception('invalidcontrollermode', 'enrol_wisc');
        }
        //--------------------------------------------------------------------------------
        $this->add_wizard_action_buttons(true, true, $submitlabel, 'createclassbutton');
        //--------------------------------------------------------------------------------
    }

    protected function settings_definition() {
    }
}

class enrol_wisc_create_review_course_form extends enrol_wisc_create_review_base_form {
}

class enrol_wisc_create_review_instance_form extends enrol_wisc_create_review_base_form {
    protected function settings_definition() {
        $mform    = $this->_form;
        // For instance forms, we add the TA config option here rather than showing a separate page
        $mform->addElement('header','', get_string('roles'));
        $taroles = enrol_wisc_create_ui_helper::get_tarole_menu();
        $mform->addElement('select', 'tarole', get_string('instancetarole', 'enrol_wisc'), $taroles);
        $mform->addHelpButton('tarole', 'instancetarole', 'enrol_wisc');
        $auditorroles = enrol_wisc_create_ui_helper::get_auditorrole_menu();
        $mform->addElement('select', 'auditorrole', get_string('instanceauditorrole', 'enrol_wisc'), $auditorroles);
        $mform->addHelpButton('auditorrole', 'instanceauditorrole', 'enrol_wisc');
    }
}

class enrol_wisc_buildcategories_form extends moodleform {
    function definition() {
        global $CFG;

        $mform    = $this->_form;

        /// form definition
        //--------------------------------------------------------------------------------
        $mform->addElement('header','options', get_string('settings'));

        $mform->addElement('checkbox', 'update', get_string('updatecategories', 'enrol_wisc'));
        $mform->addElement('checkbox', 'sort', get_string('resortcategories', 'enrol_wisc'));

        /// add term info
        $datastore = new \enrol_wisc\local\chub\chub_datasource();
        $terms = $datastore->getAvailableTerms();
        $mform->addElement('header','selectterms', get_string('selectterms', 'enrol_wisc'));

        $termoptions = array();
        foreach ($terms as $term) {
            $termoptions[$term->termCode] = $term->longDescription;
        }
        ksort($termoptions);
        foreach ($termoptions as $termCode=>$description) {
            $mform->addElement('checkbox', 'term_'.$termCode, $description);
        }


         //--------------------------------------------------------------------------------
        $this->add_action_buttons(true, get_string('buildcategories', 'enrol_wisc'));
        //--------------------------------------------------------------------------------
    }
}

class enrol_wisc_cohort_form extends moodleform {
    function definition() {
        global $CFG;

        require_once($CFG->dirroot.'/cohort/lib.php');

        $mform    = $this->_form;

        $cohort   = $this->_customdata['cohort'];
        $action   = $this->_customdata['action'];
        $subjects = $this->_customdata['subjects'];  // array of subjects as returned from wisc_ra_datastore->getAllSubjectsAndOrgs()
        $orgs     = $this->_customdata['orgs'];      // array of orgs as returned from wisc_ra_datastore->getAllSubjectsAndOrgs()

        // sort subjects by org
        $subjectsbyorg = array();
        foreach ($subjects as $subject) {
            $subjectsbyorg[$subject->academicOrgCode][$subject->subjectCode] = $subject->description;
        }
        ksort($orgs);

        /// form definition
        //--------------------------------------------------------------------------------
        $mform->addElement('header','options', get_string('cohort', 'cohort'));


        if ($action == 'create') {
            // Get the list of site cohorts
            $cohortmenu = array('' => get_string('choosedots'));
            $cohorts = cohort_get_cohorts(context_system::instance()->id, 0, 0);
            foreach ($cohorts['cohorts'] as $cohort) {
                $cohortmenu[$cohort->id] = format_string($cohort->name);
            }
            $mform->addElement('select', 'cohortid', get_string('cohort_key', 'enrol_wisc'), $cohortmenu);
            $mform->addRule('cohortid', null, 'required', null, 'client');
        } else {
            $mform->addElement('static', 'cohortlabel', get_string('cohort_key', 'enrol_wisc'), format_string($cohort->name));
            $mform->addElement('hidden', 'cohortid');
            $mform->setDefault('cohortid', $cohort->id);
        }

        $mform->addElement('hidden', 'action');
        $mform->setDefault('action', $action);

        $options = array(0                                           => '',
                         enrol_wisc_plugin::CREATECOURSE_FULL        => get_string('createcourse_full', 'enrol_wisc'),
                         enrol_wisc_plugin::CREATECOURSE_LIMITED     => get_string('createcourse_limited', 'enrol_wisc'));

        // org/subject settings
        ksort($orgs);
        foreach ($orgs as $org) {
            $mform->addElement('header','header_'.$org->academicOrgCode, $org->shortDescription);
            $mform->addElement('select', 'org_'.$org->academicOrgCode, get_string('access_org_key', 'enrol_wisc', $org->shortDescription), $options);

            asort($subjectsbyorg[$org->academicOrgCode]);
            foreach ($subjectsbyorg[$org->academicOrgCode] as $subjectCode => $subject) {
                $mform->addElement('select', 'subject_'.$subjectCode, "$subjectCode " . get_string('access_subject_key', 'enrol_wisc', $subject), $options);
                $mform->setAdvanced('subject_'.$subjectCode);
            }
        }

        //--------------------------------------------------------------------------------
        $this->add_action_buttons(true, get_string('update'));
        //--------------------------------------------------------------------------------
    }
}

<?php

/**
 * CLI sync for full WISC roster synchronisation.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 *
 * This script is meant to be called from a cronjob to sync moodle with the WISC roter data.
 *
 *   To force a sync even if the upstream datasource timestamp hasn't changed since the last sync,
 *   use the -f flag.  e.g. php sync.php -- -f
 *
 * Example cron entry:
 * # 5 minutes past 4am
 * 5 4 * * * /usr/bin/php5 -c /etc/php5/cli/php.ini /var/www/moodle/enrol/wisc/cli/sync.php
 *
 * Notes:
 *   - If you have a large number of users, you may want to raise the memory limits
 *     by passing -d momory_limit=256M
 *   - For debugging & better logging, you are encouraged to use in the command line:
 *     -d log_errors=1 -d error_reporting=E_ALL -d display_errors=0 -d html_errors=0
 *
 */

define('CLI_SCRIPT', true);

$arguments = getopt("f");
$force = isset($arguments['f']);

require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

// Ensure errors are well explained
$CFG->debug = DEBUG_NORMAL;

if (!enrol_is_enabled('wisc')) {
    error_log('[ENROL WISC] '.get_string('pluginnotenabled', 'enrol_wisc'));
    die;
}

// Update enrolments
$enrol = enrol_get_plugin('wisc');
$enrol->sync_enrolments($force);


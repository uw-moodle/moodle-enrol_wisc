<?php

/**
 * CLI script to remove empty course categories.  This isn't recursive, so
 * it needs to be run multiple times to remove empty category trees.
 *
 * Invoke with -f to actually remove the categories, otherwise this script just prints what would be done.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->libdir.'/clilib.php');

$arguments = getopt("f");
$force = isset($arguments['f']);

// Ensure errors are well explained
$CFG->debug = DEBUG_NORMAL;

$withcourses = $DB->get_records_menu('course', array(), '', 'id,category');
$withsubcategories = $DB->get_records_menu('course_categories', array(), '', 'id,parent');
$used = array_merge($withcourses, $withsubcategories);

$allcategories = $DB->get_records_menu('course_categories', array(), '', 'id,name');
$allcategories = array_keys($allcategories);

$allempty = array_diff($allcategories, $used);
$todelete = $DB->get_records_list('course_categories', 'id', $allempty);

// exclude default/temp categories
$exclude = array();
$exclude[get_config('enrol_wisc', 'category')] = true;
$exclude[get_config('enrol_wisc', 'tempcategory')] = true;
$sxclude[$CFG->defaultrequestcategory] = true;

foreach ($todelete as $category) {
    // don't delete excluded categories or categories with no idnumber
    if (empty($category->idnumber) || !empty($exclude[$category->id])) {
        continue;
    }
    echo "Deleting $category->name ($category->idnumber)\n";
    if ($force) {
        // User category_delete_move, just to be safe.
        category_delete_move($category, $CFG->defaultrequestcategory, false);
    }
}


<?php

/**
 * CLI script to create a moodle course based on an entry in the enrol_wisc_controllers table.
 *
 * This script is called by the course creation process.
 *
 * Usage:  /usr/bin/php createcourse.php -c 1234
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

define('CLI_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->libdir.'/clilib.php');
require_once($CFG->libdir.'/weblib.php');
require_once($CFG->dirroot.'/enrol/wisc/controller.class.php');
require_once($CFG->dirroot.'/enrol/wisc/create_ui.class.php');
require_once($CFG->dirroot.'/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot.'/backup/util/loggers/output_text_logger.class.php');

$opts = getopt("c:");
$controllerid = $opts['c'];

if (!$controllerid) {
    cli_error('Usage: createcourse.php -c 1234');
}

// Ensure errors are well explained
$CFG->debug = DEBUG_NORMAL;

if (!enrol_is_enabled('wisc')) {
    error_log('[ENROL WISC] '.get_string('pluginnotenabled', 'enrol_wisc'));
    die;
}

if (!($cc = enrol_wisc_create_ui::load_controller($controllerid))) {
    cli_error('Cannot load controller');
}

// setup user session
$user = $DB->get_record('user', array('id'=>$cc->get_userid()), '*', MUST_EXIST);
cron_setup_user($user);

$fullname = $cc->get_settings()->fullname;

$trace = new text_progress_trace();
$progress = new \core\progress\none();
$logger = new output_text_logger(backup::LOG_INFO);

$trace->output("[enrol_wisc] Background course create: \"$fullname\"");
$trace->output("[enrol_wisc] Controllerid=$controllerid");

// Create course
$cc->execute(array(), $progress, $logger);

$trace->output("[enrol_wisc] Finished creating course");

$trace->finished();

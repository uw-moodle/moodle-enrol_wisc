<?php
/**
 * CLI test script for CHUB access
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

    define('CLI_SCRIPT', true);

    require(dirname(__FILE__) . '/../../../config.php');

    require_once($CFG->dirroot.'/enrol/wisclib/peoplepicker.php');

    $timestart  = time();

    //mtrace("Server Time: ".date('r',$timestart)."<br/><br/>");

    try {
        mtrace("Opening datastore<br/>");

        $datastore = new \enrol_wisc\local\chub\chub_datasource();

        mtrace("Reading terms...");
        $terms = $datastore->getAvailableTerms();

        if (count($terms) == 0) {
            throw new Exception( 'No terms returned from datastore' );
        }
        mtrace("<br/>   got ".count($terms)." terms\n");


        foreach( $terms as $term ) {
            mtrace("<br/>Term: $term->longDescription ($term->termCode)\n");

            mtrace("  Reading departments... ","");
            $depts = $datastore->getSubjectsInTerm( $term->termCode );
            mtrace("got ".count($depts)." depts\n");

            //foreach( $depts as $dept ) {
            //    mtrace("    $dept->description ($dept->subjectCode)<br/>");
                //$crisCourses = $crisDataStore->getClassesByDepartment( $term['TERM'], $dept['SUBJECT_CODE'] );
                //mtrace(count($crisCourses)." courses");
            //}
        }
    } catch( Exception $e ) {
       mtrace("<br/><br/><strong>Test Failed: ".$e->getMessage()."</strong>");
       exit();
    }

    try {
        mtrace("<br/><br/>Opening peoplepicker<br/>");

        $pp = new wisc_peoplepicker();

        mtrace("Reading people...");
        $people = $pp->getPeopleByNetid('mjpetro');

        if (count($people) == 0) {
            throw new Exception( 'No people returned' );
        }
        mtrace("<br/>   got ".count($people)." people\n");

    } catch( Exception $e ) {
       mtrace("<br/><br/><strong>Test Failed: ".$e->getMessage()."</strong>");
       exit();
    }


    mtrace("<br/><br/><strong>Test Succeeded</strong>");
    mtrace("<br/><br/>elapsed time: ". (time() - $timestart) . "s");
?>

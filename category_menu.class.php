<?php

/**
 * Class to handle the category menu in the course creator.
 *
 * We use the category idnumber as the primary key in dealing with categories. Only the first category defined for each idnumber is used.
 *
 * For existing categories, we require permission enrol/wisc:createcourseincategory for a category to show up in the menu.
 * For new categories, we require permission enrol/wisc:createcourseincategory in the closest existing parent category
 * (or at the site level for top-level categories) for a category to show up in the menu.
 *
 * Categories in which the user has no permission are ignored when generating the category menu.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class enrol_wisc_category_menu {

    // Category tree
    protected $categories;

    // Are we allowed to create new categories?
    protected $createcategories;

    // User id for permissions checks
    protected $userid;

    public function __construct($createcategories = false, $userid = null) {
        global $USER;
        $this->createcategories = $createcategories;
        if ($userid) {
            $this->userid = $userid;
        } else {
            $this->userid = $USER->id;
        }
    }

    /**
     * Add a potential category to the tree by idnumber.  Parents should always be added before children.
     *
     * @param string $idnumber  The category idnumber
     * @param string $name  The category name, only used for new categories
     * @param string $parentidnumber  The idnumber of the poarent category, or '' for a root-level category
     * @param boolean $canhavecourses  Should this category show up in the menu?
     * @throws coding_exception
     */
    public function add_node($idnumber, $name, $parentidnumber, $canhavecourses) {
        if (!isset($this->categories[$idnumber])) {
            $newcategory = new enrol_wisc_category();
            $newcategory->idnumber = $idnumber;
            $newcategory->parentidnumber = $parentidnumber;
            $newcategory->canhavecourses = $canhavecourses;

            if (!empty($parentidnumber)) {
                $parent = $this->categories[$parentidnumber];
                if (!$parent) {
                    throw new coding_exception('Parent category does not exist in tree');
                }
            } else {
                $parent = null;
            }

            $moodlecategory = $this->get_category_by_idnumber($idnumber);
            if ($moodlecategory) {
                // This is an existing category
                $newcategory->moodlecategory = $moodlecategory;
                // use actual moodle permissions to see if the category is valid
                $context = context_coursecat::instance($moodlecategory->id);
                $valid = has_capability('enrol/wisc:createcourseincategory', $context, $this->userid);
                // Prefer categories where the user has capability 'enrol/wisc:enrolconfig', as that indicates they
                // are managing courses in the category.
                $preferred = has_capability('enrol/wisc:enrolconfig', $context, $this->userid);
            } else {
                // This is a new category
                if ($parent) {
                    // use inherited moodle permissions from existing parent category.
                    $valid = $parent->valid;
                    $preferred = $parent->preferred;
                } else {
                    // top-level category, so require permissions at the site level.
                    $valid = has_capability('enrol/wisc:createcourseincategory', context_system::instance(), $this->userid);
                    $preferred = false;
                }
                if (!$this->createcategories) {
                    $valid = false;
                }
            }
            $newcategory->valid = $valid;
            $newcategory->preferred = $preferred;
            // use real name when possible
            if ($newcategory->moodlecategory) {
                $newcategory->name = $newcategory->moodlecategory->name;
            } else {
                $newcategory->name = $name;
            }

            $this->categories[$idnumber] = $newcategory;
        } else {
            // Already added, so ignore
        }
    }

    /**
     * Are there any valid categories for the menu?
     *
     * @return boolean
     */
    public function is_empty() {
        foreach ($this->categories as $category) {
            if ($category->valid && $category->canhavecourses) {
                return false;
            }
        }
        return true;
    }

    protected function get_category_by_idnumber($idnumber) {
        global $DB;
        static $allcategories = null; // cache

        // for efficiency, cache all categories indexed by idnumber
        if (is_null($allcategories)) {
            $allcategories = array();
            $categories = $DB->get_records('course_categories');
            foreach ($categories as $category) {
                if (!empty($category->idnumber)) {
                    $allcategories[$category->idnumber] = $category;
                }
            }
        }
        if (isset($allcategories[$idnumber])) {
            return $allcategories[$idnumber];
        } else {
            return false;
        }
    }

    /**
     * Generate a sorted category array suitable for use in a select menu
     *
     * The values are category paths
     *  and the keys are either category id's for existing categories, or
     *  of the form "N".idnumber for categories that don't yet exist.
     *
     * @throws moodle_exception
     * @return array of category names
     */
    public function make_categories_list() {
        $preferredlist = array();
        $otherlist = array();
        foreach ($this->categories as $category) {
            if ($category->valid && $category->canhavecourses) {
                $categoryname = format_string($category->name);

                // build the path
                $path = array();
                $idnumber = $category->idnumber;
                while (!empty($idnumber)) {
                    $tempcategory = $this->categories[$idnumber];
                    if (isset($path[$tempcategory->idnumber])) {
                        throw new moodle_exception('Loop detected in make_categories_list');
                    }
                    $path[] = format_string($tempcategory->name);
                    $idnumber = $tempcategory->parentidnumber;
                }
                $path = array_reverse($path);

                // Key is either the category id, or "N".idnumber for categories that don't exist
                if (!empty($category->moodlecategory)) {
                    $key = $category->moodlecategory->id;
                } else {
                    $key = "N".$category->idnumber;
                }
                if ($category->preferred) {
                    $preferredlist[(string)$key] = $path;
                } else {
                    $otherlist[(string)$key] = $path;
                }
            }
        }
        // sort each path component independently
        $pathcompare = function(&$a,&$b) {
            for ($i = 0 ; $i < count($a); ++$i) {
                if ($i >= count($b)) {
                    return 1;
                }
                $res = strcmp($a[$i], $b[$i]);
                if ($res) {
                    return $res;
                }
            }
            return 0;
        };
        // Sort preferred and non-preferred list separately.
        uasort($preferredlist, $pathcompare);
        uasort($otherlist, $pathcompare);

        // merge the sorted lists in a way that's safe for numeric keys
        $list = $preferredlist;
        foreach($otherlist as $key=>$value) {
            $list[$key] = $value;
        }

        // Join the path componenets and return.
        return array_map(function($path){return join(' / ', $path);}, $list);
    }

    /**
     * Create a category, including all parents, identified by a key of the form "N".idnumber.
     *
     * @param string $key
     * @throws moodle_exception
     * @return integer the moodle id of the created category
     */
    public function create_selected_category($key) {
        global $DB;
        if (substr($key, 0, 1) === 'N') {
            $idnumber = substr($key, 1);
        } else {
            throw new moodle_exception('Unknown category key type');
        }
        if (!isset($this->categories[$idnumber])) {
            throw new moodle_exception('Unknown category key');
        }
        $category = $this->categories[$idnumber];
        if (!$category->valid || !$category->canhavecourses) {
            throw new moodle_exception('Invalid category');
        }
        // Loop and remember all parent categories until we reach an existing category
        $tocreate = array();
        $parentid = null;
        while (!empty($idnumber)) {
            if (!isset($this->categories[$idnumber])) {
                throw new moodle_exception('Missing parent category');
            }
            $category = $this->categories[$idnumber];
            if (!empty($category->moodlecategory)) {
                // Found an existing parent category
                $parentid = $category->moodlecategory->id;
                break;
            }
            $tocreate[] = $category;
            $idnumber = $category->parentidnumber;
        }
        if (empty($idnumber)) {
            $parentid = 0;  // last category was a root-level category
        }
        // Create the categories in reverse order
        $tocreate = array_reverse($tocreate);
        foreach ($tocreate as $category) {
            $newcategory = $DB->get_record('course_categories', array('idnumber'=>$category->idnumber));
            if (!$newcategory) {
                $newcategory = new stdClass();
                $newcategory->name = $category->name;
                $newcategory->parent = $parentid;
                $newcategory->idnumber = $category->idnumber;
                $newcategory = coursecat::create($newcategory);
            }
            $parentid = $newcategory->id;
        }
        return $parentid;
    }
}

class enrol_wisc_category {
    public $idnumber;        // The category idnumber
    public $parentidnumber;  // The parent category idnumber, or '' for top-level categories
    public $name;            // Category name
    public $valid;           // Do permissions allow this category?

    public $preferred;       // Should this category show toward the top of the menu?
                             // This is used for categories in which a user has manager rights.

    public $canhavecourses;  // Is this category eligible to show up in the menu?
    public $moodlecategory;  // The actual moodle course_category object, if any.
}

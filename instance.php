<?php
/**
 * Page to create a new UW enrollment instance
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/enrol/wisc/create_ui.class.php');
require_once($CFG->dirroot.'/enrol/wisc/ui_components.php');
require_once($CFG->dirroot.'/enrol/wisc/accesslib.php');

if (!enrol_is_enabled('wisc')) {
    throw new Exception("WISC enrolment plugin not enabled");
}

$createid = optional_param('createid', false, PARAM_ALPHANUM);

$cc = enrol_wisc_create_ui::load_controller($createid);
if (!$cc) {
    $courseid = required_param('courseid', PARAM_INT);
    $instanceid = optional_param('id', 0, PARAM_INT);
    if ($instanceid) {
        // editing existing instance
        $cc = new enrol_wisc_controller($USER->id, enrol_wisc_controller::TYPE_EDIT, $courseid, $instanceid);
    } else {
        // creating new instance
        $cc = new enrol_wisc_controller($USER->id, enrol_wisc_controller::TYPE_INSTANCE, $courseid);
    }
}

// check security
$courseid = $cc->get_courseid();
$instanceid = $cc->get_instanceid();
require_login($courseid);
$context = context_course::instance($courseid);
require_capability('enrol/wisc:config', $context);
require_capability('moodle/course:enrolconfig', $context);

$url = new moodle_url('/enrol/wisc/instance.php', array('courseid'=>$courseid, 'id'=>$instanceid));
$PAGE->set_url($url);
$PAGE->set_pagelayout('admin');

if ($instanceid) {
    $instance = $DB->get_record('enrol', array('id'=>$instanceid, 'courseid'=>$courseid, 'enrol'=>'wisc'), '*', MUST_EXIST);
}

$PAGE->set_context($context);

$create = new enrol_wisc_create_ui($cc);
$create->process();
$create->save_controller();

$heading = get_string('addenrolment', 'enrol_wisc');

$PAGE->set_title($heading.': '.$create->get_stage_name());
$PAGE->set_heading($heading);
$PAGE->navbar->add($create->get_stage_name());

$create->initialize_js($PAGE);

$renderer = $PAGE->get_renderer('enrol_wisc');

echo $OUTPUT->header();
echo $renderer->progress_bar($create->get_progress_bar());

if ($create->get_stage() == enrol_wisc_create_ui::STAGE_COMPLETE) {
    $create->execute();
}

echo $create->display();
unset($create);
echo $OUTPUT->footer();
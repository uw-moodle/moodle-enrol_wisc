<?php


defined('MOODLE_INTERNAL') || die();

// require course info providers
require_once($CFG->dirroot . '/enrol/wisc/lib/courseinfoproviders/moodle_courseinfo_provider.php');
require_once($CFG->dirroot . '/enrol/wisc/lib/courseinfoproviders/esb_courseinfo_provider.php');


/**
 * Class that implements course info providers to get a user's external courses
 *
 * @package enrol_wisc
 * @author John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin System - Board of Regents
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class external_user_courses{

    /** @var array $instances Instances of courseinfo providers */
    protected $instances = array();

    /** @var array $externalcourses Class holder for all external courses */
    public $externalcourses = array();

    /** @var array $errors Holds errors from couseinfo providers */
    protected $errors = array();

    /**
     * Get the user courses based on the userid
     *
     * @param string $userid The user's email netid@wisc.edu
     * @param array $localcourses Any local courses that we should filter out of the external ones
     * @param array $forceupdate Force a refresh of the external data.
     * @return array The externalcourses An array of external_course
     */
    public function get_user_courses($userid, $localcourses = array(), $forceupdate = false){

        // first check to see if the user's external courses exists in the cache
        $cache = cache::make('enrol_wisc', 'externalcourses');

        if(!$forceupdate && $usercourses = $cache->get($userid)){
            return $usercourses;
        }

        $this->setup_instances();

        $cachedata = true; // default cache data, but can be changed to false if a provider fails
        foreach($this->instances as $provider){

            try {
                $providercourses = $provider->get_courses($userid);
            }catch(Exception $e){
                // catch soap errors
                $this->errors[] = $e->getMessage();
                $cachedata = false; // don't cache data on this run
                $providercourses = array();
            }

            foreach($providercourses as $externalcourse){ // put the provider's courses into the general courses
                $this->externalcourses[] = $externalcourse;
            }
        }
        $this->filter_external_courses($localcourses); // filter returned external courses to remove any already existing local course
        if($cachedata){
            $cache->set($userid, $this->externalcourses); // set the user's external courses with a key of the userid
        } else {
            // Force any older data to be removed.
            $cache->delete($userid);
        }

        return $this->externalcourses;
    }

    /**
     * Sets up Web service instances
     *
     */
    protected function setup_instances(){

        // Get DSNs from wiscservices plugin.
        $dsnsconfig = get_config('enrol_wisc', 'usercoursesdsn');

        $dsns = explode(PHP_EOL, $dsnsconfig);

        // go through each dsn
        foreach($dsns as $dsn){

            // each dsn has a url and a token/username/password
            $dsnparts = explode(';', trim($dsn));
            if(!empty($dsnparts[0])){ // only get instance if
                // set up vars for the course info provider's soap clients
                $wsdl = trim($dsnparts[0]);
                $username = '';
                $password = '';
                if(!empty($dsnparts[1])){
                    $username = trim($dsnparts[1]);
                }
                if(!empty($dsnparts[2])){
                    $password = trim($dsnparts[2]);
                }

                $class = $this->determine_provider($wsdl);
                if($class){
                    try {
                        $this->instances[] = new $class($wsdl, $username, $password);
                    }catch(Exception $e){
                        $this->errors[] = $e->getMessage();
                    }
                }
            }
        }
    }


    /**
     * Determines the provider class dependent on the wsdl url
     *
     * @param string $wsdl The WSDL url for the webservice
     * @return string $class the class name of the provider
     */
    protected function determine_provider($wsdl){
        global $CFG;

        $class = false;

        if(strstr($wsdl, $CFG->wwwroot)){
            return $class; // return false for the wsdl if it's url is in the site's wwwroot
        }

        if(strstr($wsdl, 'moodle.wisc.edu')){
            $class = 'moodle_courseinfo_provider';
        }
        if(strstr($wsdl, 'esb')){
            $class = 'esb_courseinfo_provider';
        }

        return $class;
    }

    /**
     * Filters the external courses array to remove any course already
     * gotten because it exists locally
     *
     * @param array $localcourses
     */
    protected function filter_external_courses($localcourses){
        global $CFG;
        $newexternalcourses = array();

        foreach($this->externalcourses as $index => $ecourse){

            foreach($localcourses as $lcourse){
                if($ecourse->fullname == $lcourse->fullname && $ecourse->term = $lcourse->term){
                    // next check the url
                    if(strstr($ecourse->courseurl, $CFG->wwwroot)){
                        unset($this->externalcourses[$index]);
                        break; // skip searching through the rest of the local courses
                    }
                }
            }
        }

    }

    /**
     * Returns the $this->errors
     *
     * @return array
     */
    public function get_errors(){
        return $this->errors;
    }

}


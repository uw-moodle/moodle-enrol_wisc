<?php


defined('MOODLE_INTERNAL') || die();


/**
 * Class to hold an external course definition
 *
 * @package enrol_wisc
 * @author John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin System - Board of Regents
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class external_course{

    /** @var mixed $id This is a key to uniquely id this course.  "ext_" will be added as prefix for autogenerating ids */
    public $id;

    /** @var string $coursename The name of the external course*/
    public $coursename;

    /** @var string $courseurl the url for the external course */
    public $courseurl;

    /** @var string $term Term Code for the course */
    public $term;

    /** @var int $visible 0 or 1 depending on if it's visible */
    public $visible;

    /** @var string $LMS The name of the LMS the course comes from */
    public $LMS;


    function __construct($coursename, $courseurl, $term, $visible, $LMS, $id = NULL){
        $this->coursename = $coursename;
        $this->courseurl = $courseurl;
        $this->term = $term;
        $this->visible = $visible;
        $this->LMS = $LMS;

        if($id === NULL){
            // generate a random ID just for use with blocks and other things
            $this->id = uniqid('ext_');
        }else{
            $this->id = $id;
        }

    }


    public function __get($name){

        if($name == 'fullname'){ // accessor is looking for the coursename
            return $this->coursename;
        }else if($name == 'url'){
            return $this->courseurl;
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;

    }
}




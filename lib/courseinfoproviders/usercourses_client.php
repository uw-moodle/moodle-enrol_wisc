<?php

defined('MOODLE_INTERNAL') || die();

use local_wiscservices\local\soap\wss_soapclient;

/**
 * WISC usercourses soap client
 *  class for communication with the getting a user's courses
 *
 * @package enrol_wisc
 * @author John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin System - Board of Regents
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class usercourses_soapclient extends wss_soapclient {

    /**
     * Returns the singleton instance of this class
     *
     * @param string $wsdl The wsdl url for this instance
     * @param string $username
     * @param string $password
     * @exception SoapFault|Exception on error
     */
    public function __construct($wsdl, $username, $password) {

        $params = array( 'exceptions' => true,
            'trace' => true,
            'connection_timeout' => 20,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
        );

        parent::__construct($wsdl, $params);

        if(!empty($username) && !empty($password) ){
            $this->__setUsernameToken($username, $password);
        }

        $this->__setTimeout(5);
    }

}
<?php


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/enrol/wisc/lib/externalcourses/external_course.php');
require_once($CFG->dirroot . '/enrol/wisc/lib/courseinfoproviders/usercourses_client.php');

/**
 * Base course info provider
 *
 * @package
 * @author John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin System - Board of Regents
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class courseinfo_provider{

    /** @var usercourses_soapclient $client*/
    protected $client;

    /**
     * Creates a soapclient instance for the child provider
     *
     * @param $wsdl
     * @param $username
     * @param $password
     */
    function __construct($wsdl, $username, $password){

        $this->client = new usercourses_soapclient($wsdl, $username, $password);

    }

    /**
     * Child classes must define a get_courses function in order
     * for the external_user_courses class to call to the provider to get courses
     *
     * @param $eppn
     * @return array Array of external_course definitions
     */
    abstract function get_courses($eppn);


}




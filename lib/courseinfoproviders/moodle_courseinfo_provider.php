<?php


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/enrol/wisc/lib/courseinfoproviders/courseinfo_provider.php');

/**
 * Course info provider for a Moodle webservice
 *
 * @package enrol_wisc
 * @author John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin System - Board of Regents
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class moodle_courseinfo_provider extends courseinfo_provider{


    /**
     *
     *
     * @param string $username The moodle username of the user (which is the eppn created by the shib/wiscservices plugin)
     *
     * @return array $courses An array of external courses
     */
    public function get_courses($username){

        if (preg_match('/^(.*)@wisc\.edu$/', $username, $matches)) {
            // wisc domain
            $netid = $matches[1];
        } else {
            // not wisc, so ignore this username
            return array();
        }

        $response = $this->client->__soapCall('enrol_wisc_user_courses', array($netid, '', ''));

        $courses = array();
        if(is_array($response)){
            foreach($response as $course){


                $courses[] = new external_course($course['fullname'], $course['url'], $course['term'], $course['visible'], "Moodle");
            }
        }


        return $courses;

    }

}
<?php
/**
 * CHUB datastore compatibility class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * CHUB datastore compatibility class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @deprecated since 4.0 use \enrol_wisc\local\chub\chub_datasource class instead
 */
class wisc_timetable_datastore {

    /**
     * @deprecated since 4.0
     */
    public static function get_term_name($termCode) {
        return enrol_wisc\local\chub\timetable_util::get_term_name($termCode);
    }

    /**
     * @deprecated since 4.0
     */
    public static function get_timetable_datastore() {
        return new enrol_wisc\local\chub\chub_datasource();
    }

    /**
     * @deprecated since 4.0
     */
    public function cur_UW_Term() {
        return enrol_wisc\local\chub\timetable_util::get_current_termcode();
    }
}
<?php
/**
 * Special class for cohort permission administration.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class enrol_wisc_admin_setting_cohort extends admin_setting {

    /**
     * Calls parent::__construct with specific arguments
     */
    public function __construct() {
        $this->nosave = true;
        parent::__construct('enrol_wisc_tokenui', get_string('managecohorts', 'enrol_wisc'), '', '');
    }

    /**
     * Always returns true, does nothing
     *
     * @return true
     */
    public function get_setting() {
        return true;
    }

    /**
     * Always returns true, does nothing
     *
     * @return true
     */
    public function get_defaultsetting() {
        return true;
    }

    /**
     * Always returns '', does not write anything
     *
     * @return string Always returns ''
     */
    public function write_setting($data) {
        // do not write any setting
        return '';
    }

    /**
     * Builds the XHTML to display the control
     *
     * @param string $data Unused
     * @param string $query
     * @return string
     */
    public function output_html($data, $query='') {
        global $CFG, $OUTPUT, $DB, $USER;

        require_once($CFG->dirroot.'/cohort/lib.php');

        // display strings
        $strcohort = get_string('cohort', 'cohort');
        $strsubjects = get_string('createsubjects', 'enrol_wisc');
        $stredit = get_string('edit');
        $strdelete = get_string('clear');
        $strlimited = get_string('withapproval', 'enrol_wisc');

        $return = $OUTPUT->box_start('generalbox');
        $return .= '<br />'. get_string('cohortoverview', 'enrol_wisc') . '<br /><br />';

        $table = new html_table();
        $table->head = array($strcohort, $strsubjects, $stredit, $strdelete);
        $table->align = array('left', 'left');
        //$table->width = '100%';
        $table->data = array();

        $editcohorturl = new moodle_url('/enrol/wisc/cohort.php');
        $cohorturl = new moodle_url('/cohort/index.php');

        // Select all cohorts
        $cohorts = cohort_get_cohorts(context_system::instance()->id, 0, 0);
        $cohorts = $cohorts['cohorts'];

        // Add special cohort for all course creators
        $allusers = new stdClass();
        $allusers->id = 0;
        $allusers->name = get_string('allusers', 'enrol_wisc');
        $cohorts[0] = $allusers;

        $radatastore = new wisc_ra_datastore();
        list ($allsubjects, $allorgs) = $radatastore->getAllSubjectsAndOrgs();

        foreach ($cohorts as $cohort) {
                if ($cohort->id != 0) {
                    $allows = $DB->get_records('enrol_wisc_access', array('cohortid'=>$cohort->id));
                } else {
                    $allows = $DB->get_records_select('enrol_wisc_access', 'cohortid IS NULL');
                }
                if (!$allows) {
                    // Show the allusers option regardless
                    if ($cohort->id != 0) {
                        continue;
                    }
                }

                $subjects = array();
                $orgs = array();
                $candelete = false;
                foreach ($allows as $allow) {
                    switch ($allow->permission) {
                        case enrol_wisc_plugin::CREATECOURSE_FULL:
                            $permission = "";
                            break;
                        case enrol_wisc_plugin::CREATECOURSE_LIMITED:
                            $permission = " <span class='permission'>$strlimited</span>";
                            break;
                        default:
                            $permission = '';
                    }
                    if (!empty($allow->component)) {
                        $component = " <span class='component'>".get_string('pluginname', $allow->component)."</span>";
                    } else {
                        $component = '';
                        $candelete = true;
                    }
                    $codestr = " <span class='itemcode'>$allow->item</span>";
                    if ($allow->type == enrol_wisc_plugin::ACCESS_SUBJECT) {
                        $subjects[] = $codestr." ".$allsubjects[$allow->item]->description . $permission . $component;
                    } else if ($allow->type == enrol_wisc_plugin::ACCESS_ORG) {
                        $orgs[] = $codestr." ".$allorgs[$allow->item]->shortDescription . $permission . $component;
                    }
                }
                sort($subjects);
                sort($orgs);

                $items = implode('<br \>', array_merge($orgs, $subjects));

                $edit = html_writer::start_tag('a', array('href' => $editcohorturl->out(false, array('cohortid'=>$cohort->id, 'action'=>'edit'))));
                $edit .= $stredit;
                $edit .= html_writer::end_tag('a');
                if ($candelete) {
                    $delete = html_writer::start_tag('a', array('href' => $editcohorturl->out(false, array('cohortid'=>$cohort->id, 'action'=>'delete', 'sesskey'=>sesskey()))));
                    $delete .= $strdelete;
                    $delete .= html_writer::end_tag('a');
                } else {
                    $delete = "";
                }

                if ($cohort->id != 0) {
                    $cohortatag = html_writer::start_tag('a', array('href' => $cohorturl->out(false)));
                    $cohortatag .= $cohort->name;
                    $cohortatag .= html_writer::end_tag('a');
                } else {
                    $cohortatag = get_string('allusers', 'enrol_wisc');
                }

                $row = array($cohortatag, $items, $edit, $delete);
                $table->data[] = $row;
        }
        $return .= html_writer::table($table);

        $return .= $OUTPUT->box_end();
        // add a cohort to the table
        $return .= html_writer::start_tag('a', array('href' => $editcohorturl->out(false, array('action'=>'create'))));
        $return .= get_string('add');
        $return .= html_writer::end_tag('a');

        return highlight($query, $return);
    }
}
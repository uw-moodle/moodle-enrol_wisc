<?php

/**
 * Helper class for course creator controller class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/lib.php');
require_once($CFG->dirroot.'/enrol/wisc/accesslib.php');
require_once($CFG->dirroot.'/enrol/wisc/category_menu.class.php');
if (file_exists($CFG->dirroot.'/local/wiscservices/locallib.php')) {
    require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
}
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->libdir.'/coursecatlib.php');

abstract class enrol_wisc_controller_helper {

    public static function create_course_defaults(enrol_wisc_controller $cc) {
        global $DB;
        global $CFG;

        $term = $cc->get_term();
        $classes = $cc->get_classes();

        $mcourse = new stdClass;  // new moodle course object

        // Get the our datastore
        $radatastore = wisc_ra_datastore::get();

        // Compute course name
        if (!empty($classes)) {
            $termname = \enrol_wisc\local\chub\timetable_util::get_term_name($term);
            $courses = array();
            foreach ($classes as $class) {
                $courses['S'.$class->courseId]['S'.$class->sectionNumber][] = $class;
            }
            $names = array();
            $shortnames = array();
            $summaries = array();
            $singlecourse = count($courses) == 1;
            foreach ($courses as $scourseId => $sections) {
                $courseId = substr($scourseId, 1);
                $asection = reset($sections);
                $aclass = reset($asection);
                $course = $radatastore->getCourseByCourseId($term, $aclass->subjectCode, $courseId);

                $name = sprintf('%s %s', $course->subjectstr, $course->catalogNumber);
                if ($singlecourse) {
                    $name .= ' '.$course->title;
                    // add topic if there is only one section
                    if (count($sections) == 1 && !empty($sections[0]->topic)) {
                        $name .= ' ['.$sections[0]->topic.']';
                    }
                }

                // add course description
                if ($singlecourse) {
                    $summaries[] = $course->description;
                } else {
                    $summaries[] = "$name: $course->description";
                }
                $names[] = $name;
                $shortnames[] = sprintf('%s %s', $course->subjectstr, $course->catalogNumber);
            }
            $name = join('/', $names) . ' ' . $termname;
            $simple = join('/', $shortnames) . ' ' . $termname;
            $summary = implode("\n\n", $summaries);

            $instance_number = 1;
            $shortname = $simple . " (" . $instance_number .")";
            while( $DB->record_exists('course', array('shortname'=>$shortname)) ) {
                $shortname = $simple . " (" . ++$instance_number .")";
            }

            $mcourse->shortname = $shortname;
            $mcourse->fullname = preg_replace( '/\'/', "", $name ) . ' (' . $instance_number . ')';
            $mcourse->summary = format_text($summary, FORMAT_PLAIN);
        } else {
            // not a timetable class
            $mcourse->shortname = '';
            $mcourse->fullname = '';
            $mcourse->summary = '';
        }

        // Compute start and end dates
        if (!empty($term)) {
            if (!empty($classes)) {
                $startdates = array();
                $enddates = array();
                foreach ($classes as $class) {
                    $startdates[] = $class->startDate;
                    $enddates[] = $class->endDate;
                }
                $firststart = min($startdates);
                $lastend = max($enddates);
            } else {
                // no classes mapped, use term dates
                $datastore = new \enrol_wisc\local\chub\chub_datasource();
                $termobj = $datastore->getTerm($term);

                $firststart = $termobj->beginDate;
                $lastend = $termobj->endDate;
            }
            // Pretend that the course starts on a Sunday (even though it probably doesn't) so that our
            // weeks start on a Sunday.
            $dateInfo = getdate( $firststart );
            $courseStart = $firststart - $dateInfo['wday'] * 60 * 60 * 24;
            $timespan = $lastend - $courseStart;
            $hours = $timespan / 3600;
            $days = $hours / 24;
            $weeks = ceil( $days / 7 );

            $mcourse->startdate = $courseStart;
            $mcourse->numsections = $weeks;

        } else {
            // no term, so use moodle defaults
            $mcourse->startdate = time() + 3600 * 24;
            $mcourse->numsections = get_config('moodlecourse', 'numsections');
        }

        // The rest comes from moodle defaults for new courses

        $courseconfig = get_config('moodlecourse');

        $mcourse->summaryformat = FORMAT_HTML;

        $mcourse->format = $courseconfig->format;
        $mcourse->hiddensections = $courseconfig->hiddensections;
        $mcourse->newsitems = $courseconfig->newsitems;
        $mcourse->showgrades = $courseconfig->showgrades;
        $mcourse->showreports = $courseconfig->showreports;
        $mcourse->maxbytes = $courseconfig->maxbytes;
        $mcourse->groupmode = $courseconfig->groupmode;
        $mcourse->groupmodeforce = $courseconfig->groupmodeforce;
        $mcourse->visible = $courseconfig->visible;
        $mcourse->lang = $courseconfig->lang;
        $mcourse->enrol_guest_password_0 = '';

        require_once($CFG->libdir.'/completionlib.php');
        if(completion_info::is_enabled_for_site()) {
            $mcourse->enablecompletion = $courseconfig->enablecompletion;
        } else {
            $mcourse->enablecompletion = 0;
        }
        return $mcourse;
    }

/**
 * Generate the enrol_wisc_category_menu containing all potential valid course categories.
 *
 * For classes with a term configured we use categories with idnumber:
 *     {$deptcode}_{$term}    (e.g. 600_1132)
 *
 * For classes with no term, we use categories with idnumber:
 *     {$acadorgcode}_misc    (e.g. E_misc)
 *     {$deptcode}_misc       (e.g. 600_misc)
 *
 * When 'autocreatecategory' is enabled, we include categories that don't yet exist.
 *
 * @param enrol_wisc_controller $cc
 * @throws moodle_exception
 * @return enrol_wisc_category_menu
 */
    public static function get_category_menu(enrol_wisc_controller $cc) {
        $termcode = $cc->get_term();
        $classes = $cc->get_classes();

        $datastore = new \enrol_wisc\local\chub\chub_datasource();
        $radatastore = wisc_ra_datastore::get();

        $autocreatecategory = get_config("enrol_wisc", "autocreatecategory");
        $catmenu = new enrol_wisc_category_menu($autocreatecategory, $cc->get_userid());

        $misccategoryname = get_string('misccategoryname', 'enrol_wisc');

        if (empty($termcode)) {
            // No term for course.

            // Pick any term so that we can query subjects.
            $terms = $datastore->getAvailableTerms();
            $aterm = null;
            foreach ($terms as $term) {
                $aterm = $term;
                if ($term->termCode % 2 == 0) {
                    break; // even-numbered terms are preferred (e.g. not Winter term)
                }
            }
            if (empty($aterm)) {
                throw new moodle_exception('No suitable term found from CHUB');
            }

            // Query all subjects
            $subjects = $datastore->getSubjectsInTerm($aterm->termCode);

            // Add all available subject_misc and org_misc nodes
            foreach ($subjects as $subject) {
                $org = $subject->schoolCollege;
                $catmenu->add_node("{$org->academicOrgCode}", $org->shortDescription, '', false);
                $catmenu->add_node("{$org->academicOrgCode}_misc", $misccategoryname, "{$org->academicOrgCode}", true);
                $catmenu->add_node("{$subject->subjectCode}", $subject->description, "{$org->academicOrgCode}", false);
                $catmenu->add_node("{$subject->subjectCode}_misc", $misccategoryname, "{$subject->subjectCode}", true);
            }
            foreach ($radatastore->getOtherOrgs() as $org) {
                $catmenu->add_node("{$org->academicOrgCode}", $org->shortDescription, '', true);
                $catmenu->add_node("{$org->academicOrgCode}_misc", $misccategoryname, "{$org->academicOrgCode}", true);
            }
        } else {
            // We have a term for course.

            $term = $datastore->getTerm($termcode);
            if (!$term) {
                throw new moodle_exception('No term returned from CHUB');
            }

            if (empty($classes)) {
                // No classes, so query all subjects
                $subjects = $datastore->getSubjectsInTerm($term->termCode);
                // Add all available subject_term nodes
                foreach ($subjects as $subject) {
                    $org = $subject->schoolCollege;
                    $catmenu->add_node("{$org->academicOrgCode}", $org->shortDescription, '', false);
                    $catmenu->add_node("{$subject->subjectCode}", $subject->description, "{$org->academicOrgCode}", false);
                    $catmenu->add_node("{$subject->subjectCode}_{$term->termCode}", $term->longDescription, "{$subject->subjectCode}", true);
                }
                foreach ($radatastore->getOtherOrgs() as $org) {
                    $catmenu->add_node("{$org->academicOrgCode}", $org->shortDescription, '', false);
                    $catmenu->add_node("{$org->academicOrgCode}_{$term->termCode}", $misccategoryname, "{$org->academicOrgCode}", true);
                }
            } else {
                // Otherwise get subjects from the classes
                foreach ($classes as $class) {
                    // Add all available subject_term nodes
                    $catmenu->add_node("{$class->academicOrgCode}", $class->academicOrgShortDescription, '', false);
                    $catmenu->add_node("{$class->subjectCode}", $class->subjectDescription, "{$class->academicOrgCode}", false);
                    $catmenu->add_node("{$class->subjectCode}_{$term->termCode}", $term->longDescription, "{$class->subjectCode}", true);
                }
            }
        }

        // Add the root level misc category if appropriate
        if (empty($classes) || $catmenu->is_empty()) {
            $catmenu->add_node("_misc", $misccategoryname, '', true);
        }

        return $catmenu;
    }

    /**
     * Create a new category by idnumber.  This is a utility function which can be used from outside the
     * course creator to create wisc category trees.  Parent categories are created automatically.
     *
     * idnumber can be any of:
     *    subjectcode_termcode    e.g. 600_1142
     *    subjectcode_"misc"           600_misc
     *    subjectcode                  600
     *    orgcode_"misc"               E_misc
     *    orgcode                      E
     *
     *
     * @return int|false new category id on success
     */
    public static function create_category_by_idnumber($idnumber) {
        global $DB;
        if (empty($idnumber)) {
            return false;
        }
        // See if it already exists
        if ($rec = $DB->get_record('course_categories', array('idnumber'=>$idnumber), 'id', IGNORE_MISSING)) {
            return $rec->id;
        }

        // Make new category
        $newcategory = new stdClass();
        $newcategory->idnumber = $idnumber;

        $datastore = new \enrol_wisc\local\chub\chub_datasource();
        $radatastore = wisc_ra_datastore::get();

        // Parse idnumber  (using array_pad so as to supress a warning message)
        list ($itemcode, $suffix) = array_pad(explode('_', $idnumber, 2), 2, null);
        if (!empty($suffix)) {
            // We have a suffix, so this is a term or misc category

            // Get parent category first
            $parentid = self::create_category_by_idnumber($itemcode);
            if (!$parentid) {
                return false;
            }
            $newcategory->parent = $parentid;

            // Determine the name
            if ($suffix == 'misc') {
                $newcategory->name = get_string('misccategoryname', 'enrol_wisc');
            } else {
                $term = $datastore->getTerm($suffix);
                if (!$term) {
                    return false;
                }
                $newcategory->name = $term->longDescription;
            }
        } else {
            // No suffix, so this is a subject or org category
            if (is_number($itemcode)) {
                // Subject category

                // Pick any term so that we can query subjects.
                $term = $datastore->getCurrentTerm();
                $subject = $datastore->getSubject($term->termCode, $itemcode);
                if (!$subject) {
                    return false;
                }
                // Parent category is an academicOrg, so fetch category id
                $parentid = self::create_category_by_idnumber($subject->schoolCollege->academicOrgCode);
                if (!$parentid) {
                    return false;
                }
                $newcategory->parent = $parentid;
                $newcategory->name = $subject->description;

            } else {
                // Academic org category
                $schools = $datastore->getAllSchoolColleges();
                $foundorgname = null;
                foreach ($schools as $org) {
                    if ($org->academicOrgCode == $itemcode) {
                        $foundorgname = $org->shortDescription;
                        break;
                    }
                }
                if (!$foundorgname) {
                    foreach ($radatastore->getOtherOrgs() as $org) {
                        if ($org->academicOrgCode == $itemcode) {
                            $foundorgname = $org->shortDescription;
                            break;
                        }
                    }
                }
                if (!$foundorgname) {
                   return false;
                }
                // No parent category
                $newcategory->parent = 0;
                $newcategory->name = $foundorgname;
            }
        }
        // Actually make the category
        $newcategory = coursecat::create($newcategory);
        return $newcategory->id;
    }

    public static function decode_term( $termIn ) {
        $term = (string)$termIn;
        $answer = new stdClass();
        $year = (int) ($term[1] . $term[2] );
        $t = "(unknown)";
        if( $term[3] == '2' ) {
            $t = "Fall";
            $year -= 1;
        } else if( $term[3] == '4') {
            $t = "Spring";
        } else if( $term[3] == '6') {
            $t = "Summer";
        }
        $answer->year = $year;
        $answer->semester = $t;
        return $answer;
    }

    public static function create_course(enrol_wisc_controller $cc, &$errorstr, $options,
                                         \core\progress\base $progress,
                                         base_logger $logger) {
        global $DB, $CFG;

        $user = $DB->get_record('user', array('id'=>$cc->get_userid()), '*', MUST_EXIST);
        $errors = array();

        $progress->start_progress('Create course', 5);

        // Configure the course
        $course = enrol_wisc_controller_helper::create_course_defaults($cc);
        $settings = (array) $cc->get_settings();
        foreach ($settings as $key => $value) {
            $course->{$key} = $value;
        }
        $course->visible = 0; // course invisible to begin with
        $course->fullname .= get_string('coursecreationsuffix', 'enrol_wisc');

        $tempcategory = coursecat::get(get_config("enrol_wisc", "tempcategory"), MUST_EXIST, true);

        // Create the course category if necessary
        if (!is_number($course->category)) {
            // We need to create the course category, and potentially parents
            $catmenu = self::get_category_menu($cc);
            $savedcategoryid = $catmenu->create_selected_category($course->category);
        } else {
            $savedcategoryid = $course->category;
        }

        // Create into temp category
        $course->category = $tempcategory->id;

        // Fix the shortname to make it unique, if necessary.
        $shortname = $course->shortname;
        $instance_number = 0;
        while( $DB->record_exists('course', array('shortname'=>$shortname)) ) {
            $shortname = $course->shortname . " " . ++$instance_number;
        }
        $course->shortname = $shortname;

        // For later, in case restore changes them.

        $savedshortname = $course->shortname;
        $savedsummary = $course->summary;
        $savedsummaryformat = $course->summaryformat;

        // Create the course
        $course = create_course($course);

        $progress->progress(1);

        $cc->set_courseid($course->id);

        // create all course sections.  moodle will do this later, but we do it here so that archive imports work right
        for ($section = 1; $section <= $settings['numsections']; ++$section) {
            $thissection = new stdClass;
            $thissection->course  = $course->id;   // Create a new section structure
            $thissection->section = $section;
            $thissection->name    = null;
            $thissection->summary  = '';
            $thissection->summaryformat = FORMAT_HTML;
            $thissection->visible  = 1;
            $thissection->id = $DB->insert_record('course_sections', $thissection);
        }

        // Get the context of the newly created course
        $context = context_course::instance($course->id);

        // assign Course Originator role to person who is creating this course
        $courseoriginatorid = $DB->get_field('role','id',array('shortname'=>'courseoriginator'));
        if ($courseoriginatorid !== false) {
            enrol_try_internal_enrol($course->id, $user->id, $courseoriginatorid);
        }

        // ensure the the course creator has an instructor role in the course
        enrol_try_internal_enrol($course->id, $user->id, $CFG->creatornewroleid);

        $progress->progress(2);

        if ($cc->get_term()) {
            // add wisc/enrol instance
            $plugin = enrol_get_plugin('wisc');
            $fields = array(enrol_wisc_plugin::termfield => $cc->get_term(),
                    enrol_wisc_plugin::tarolefield => $settings['tarole'],
                    enrol_wisc_plugin::auditorrolefield => $settings['auditorrole'],
            );
            $instanceid = $plugin->add_instance($course, $fields);
            $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'wisc', 'id'=>$instanceid), '*', MUST_EXIST);

            $classes = $cc->get_classes();

            try {
                self::add_coursemaps($classes, $course, $context, $instance, $user);
            } catch (Exception $e) {
                $errors[] = "Error configuring timetable mapping: ".$e->getMessage();
            }
            self::sync_instance_enrolments($instance, $course, $progress);
        }

        // Make sure guest access is set properly. This is necessary because the
        // user may may not have had the access to do this before they were assigned
        // the creatorrole in the course

        $enrol_guest_status = $settings['enrol_guest_status_0'];
        if (has_capability('enrol/guest:config', $context, $user->id)) {
            $guestinstance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'guest'));
            if ($guestinstance) {
                $reset = ($guestinstance->status != $enrol_guest_status);

                $guestinstance->status       = $enrol_guest_status;
                $guestinstance->timemodified = time();
                $DB->update_record('enrol', $guestinstance);

                if ($reset) {
                    $context->mark_dirty();
                }
            }
        }

        $progress->progress(3);

        // Import from arhive
        $archiveid = $cc->get_archiveid();
        // Make sure the user can import from that archive
        if ($archiveid && !local_archive_user_can_import($user, $archiveid)) {
            $errors[] = "Error importing course archive: No permission to restore archive.";
            $archiveid = false;
        }

        if ($archiveid) {
            if (empty($options['customimport'])) {
                // Not a custom import, so do the import now while the course is hidden.
                try {
                    local_archive_execute_restore($course->id, $archiveid, backup::TARGET_NEW_COURSE, $user->id, false, $progress, $logger);
                } catch (Exception $e) {
                    $errors[] = "Error importing course archive: ".$e->getMessage();
                }
            } else {
                // Custom import happens after the user is redirected in the next stage.
            }
        }

        // Add the news forum if needed
        $forum = forum_get_course_forum($course->id, 'news');

        if (empty($errors)) {
            // Open the course for access
            $update = new stdClass();
            $update->id = $course->id;
            $update->fullname = $settings['fullname'];
            $update->shortname = $savedshortname;
            $update->summary = $savedsummary;
            $update->summaryformat = $savedsummaryformat;
            $update->visible = $settings['visible'];
            $DB->update_record('course', $update);

            // move to the correct category
            move_courses(array($course->id), $savedcategoryid);
        } else {
            // There were errors, so leave the course in the temp category.
            // The user will get an error email.  Admin will get an error summary.
        }

        // Reload the course context since it has changed
        $context = context_course::instance($course->id);

        // Remove manual teacher role if the creator has access at the category level
        if (is_viewing($context, $user->id, 'moodle/role:assign')) {
            self::helper_unenrol_internal_user($course->id, $user->id);
        }

        // purge appropriate caches in case fix_course_sortorder() did not change anything
        cache_helper::purge_by_event('changesincoursecat');

        $progress->end_progress();
        $cc->save_controller();

        $errorstr = implode('\n', $errors);

        $result = empty($errors);
        return $result;
    }

    public static function create_instance(enrol_wisc_controller $cc, &$errorstr, $options,
                                         \core\progress\base $progress) {
        global $DB, $CFG;
        $errors = array();

        $user = $DB->get_record('user', array('id'=>$cc->get_userid()), '*', MUST_EXIST);
        $course = $DB->get_record('course', array('id'=>$cc->get_courseid()), '*', MUST_EXIST);
        $context = context_course::instance($course->id);

        $settings = (array) $cc->get_settings();

        // add wisc/enrol instance
        $plugin = enrol_get_plugin('wisc');
        $fields = array(enrol_wisc_plugin::termfield => $cc->get_term(),
                enrol_wisc_plugin::tarolefield => $settings['tarole'],
                enrol_wisc_plugin::auditorrolefield => $settings['auditorrole'],
        );
        $instanceid = $plugin->add_instance($course, $fields);
        $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'wisc', 'id'=>$instanceid), '*', MUST_EXIST);

        $classes = $cc->get_classes();

        try {
            self::add_coursemaps($classes, $course, $context, $instance, $user);
        } catch (Exception $e) {
            $errors[] = "Error configuring timetable mapping: ".$e->getMessage();
        }
        self::sync_instance_enrolments($instance, $course, $progress);

        $errorstr = implode('\n', $errors);

        $result = empty($errors);
        return $result;
    }

    public static function update_instance(enrol_wisc_controller $cc, &$errorstr, $options,
                                         \core\progress\base $progress) {
        global $DB, $CFG;
        $errors = array();

        $progress->start_progress('Update enrollments', 2);

        $user = $DB->get_record('user', array('id'=>$cc->get_userid()), '*', MUST_EXIST);
        $course = $DB->get_record('course', array('id'=>$cc->get_courseid()), '*', MUST_EXIST);
        $context = context_course::instance($course->id);
        $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'wisc', 'id'=>$cc->get_instanceid()), '*', MUST_EXIST);

        $settings = (array) $cc->get_settings();

        // update tarole
        if ($instance->{enrol_wisc_plugin::tarolefield} != $settings['tarole']) {
            $DB->set_field('enrol', enrol_wisc_plugin::tarolefield, $settings['tarole'], array('courseid'=>$course->id, 'enrol'=>'wisc', 'id'=>$cc->get_instanceid()));
            $instance->{enrol_wisc_plugin::tarolefield} = $settings['tarole'];
        }

        // update auditorrole
        if ($instance->{enrol_wisc_plugin::auditorrolefield} != $settings['auditorrole']) {
            $DB->set_field('enrol', enrol_wisc_plugin::auditorrolefield, $settings['auditorrole'], array('courseid'=>$course->id, 'enrol'=>'wisc', 'id'=>$cc->get_instanceid()));
            $instance->{enrol_wisc_plugin::auditorrolefield} = $settings['auditorrole'];
        }

        // update maps
        $classes = $cc->get_classes();
        $radatastore = wisc_ra_datastore::get();
        $dbclasses = array();
        $maps = $DB->get_records('enrol_wisc_coursemap', array('enrolid'=>$instance->id, 'deleted'=>0));
        foreach ($maps as $map) {
            $dbclasses[] = $radatastore->getClassFromCoursemap($map);
        }
        $progress->progress(1);
        $adds = wisc_ra_datastore::helper_diff_classes($classes, $dbclasses);
        $removes = wisc_ra_datastore::helper_diff_classes($dbclasses, $classes);
        $approves = wisc_ra_datastore::helper_get_approved_classes($classes, $dbclasses);

        try {
            if (!empty($adds)) {
                self::add_coursemaps($adds, $course, $context, $instance, $user);
            }
            if (!empty($removes)) {
                self::remove_coursemaps($removes, $instance);
            }
            if (!empty($approves)) {
                self::approve_coursemaps($approves, $instance);
            }
        } catch (Exception $e) {
            $errors[] = "Error configuring timetable mapping: ".$e->getMessage();
        }

        self::sync_instance_enrolments($instance, $course, $progress);
        //events_trigger('enrol_wisc_coursemap_updated', $course->id);

        $progress->end_progress();
        $errorstr = implode('\n', $errors);

        $result = empty($errors);
        return $result;
    }

    public static function get_teachers(enrol_wisc_controller $cc) {

        $users = array();
        $settings = $cc->get_settings();
        // add timetable teachers
        try {
            if (!class_exists('local_wiscservices_plugin')) {
                throw new Exception('No local/wiscservices plugin');
            }
            $wiscservices = new local_wiscservices_plugin();
            // Get our datastore
            $radatastore = wisc_ra_datastore::get();
            $classes = $cc->get_classes();
            foreach ($classes as $class) {
                $instructors = $radatastore->getInstructors($class->termCode, $class->classNumber);
                foreach ($instructors as $instructor) {
                    $userid = $wiscservices->verify_person($instructor->person, false);
                    if (!$userid || isset($users[$userid])) {
                        continue;
                    }
                    if ($instructor->roleCode == 7 && empty($settings->tarole)) {
                        continue; // ta's with no role in course
                    }
                    $users[$userid] = $instructor->person->firstName.' '.$instructor->person->lastName;
                }
            }
        } catch (Exception $e) {
            error_log('Unable to query moodle instructors: '.$e->getMessage());
        }
        // and any other teachers in the course, if this is an instance config
        $courseid = $cc->get_courseid();
        if ($courseid) {
            $context = context_course::instance($courseid);
            $wisc = get_config("enrol_wisc");
            $managerroles = array_unique(array($wisc->teacherrole,$wisc->tarole,$wisc->otherrole));
            $courseusers = get_role_users($managerroles, $context, false, 'ra.id,u.id,u.firstname,u.lastname');
            foreach ($courseusers as $courseuser) {
                $users[$courseuser->id] = $courseuser->firstname.' '.$courseuser->lastname;
            }
        }

        return $users;
    }

    public static function notify_create_done(enrol_wisc_controller $cc, $offline) {
        global $DB, $CFG;

        $settings = $cc->get_settings();
        $admin = get_admin();

        $params = new stdClass();

        if ($cc->get_controllermode() == enrol_wisc_controller::TYPE_COURSE) {
            $params->fullname = $settings->fullname;
            $type = 'course';
        } else {
            $params->fullname = $DB->get_field('course', 'fullname', array('id'=>$cc->get_courseid()));
            $type = 'instance';
        }

        $params->supportemail = get_config('enrol_wisc', 'approvalemail');
        if (empty($params->supportemail)) {
            $params->supportemail = $admin->email;
        }

        if ($cc->get_courseid()) {
            $courseurl = new moodle_url('/course/view.php', array('id'=>$cc->get_courseid()));
            $params->url = $courseurl->out();
        } else {
            $params->url = '';
        }

        $templateemail = $cc->get_message();
        if (empty($templateemail)) {
            $templateemail = get_string('templateemail'.$type, 'enrol_wisc', $params);
        }
        $message = str_replace('{URL}', $params->url, $templateemail);
        $smallmessage = get_string('createsuccesssmall'.$type, 'enrol_wisc', $params);
        $subject = get_site()->fullname .': '. get_string('createemailsubject'.$type, 'enrol_wisc');

        $notifyotherusers = true;
        if ($cc->get_status() != enrol_wisc_controller::STATUS_FINISHED_OK) {
            // an error occured
            $subject = get_site()->fullname .': '. get_string('createerroremailsubject'.$type, 'enrol_wisc');
            $params->errors = $cc->get_errors();
            $message = get_string('createfailedemail'.$type, 'enrol_wisc', $params);
            $smallmessage = get_string('createfailedsmall'.$type, 'enrol_wisc', $params);
            $notifyotherusers = false;
        }

        $userids = array();
        if ($offline) {
            // send to user if this is an offline create
            $userids[] = $cc->get_userid();
        }
        if ($notifyotherusers) {
            $notify = $cc->get_notify();
            $userids = array_merge($userids, explode(',', $notify));
        }

        $recipients = array();
        if (!empty($userids)) {
            $userids = array_unique($userids);
            $recipients = $DB->get_records_list('user', 'id', $userids);
        }

        $result = true;
        foreach ($recipients as $recipient) {
            $eventdata = new stdClass();
            $eventdata->component        = 'enrol_wisc';
            $eventdata->name             = 'courseready';
            $eventdata->notification     = 1;

            $eventdata->userfrom         = $admin;
            $eventdata->userto           = $recipient;
            $eventdata->subject          = $subject;
            $eventdata->fullmessage      = $message;
            $eventdata->fullmessageformat= FORMAT_PLAIN;
            $eventdata->fullmessagehtml  = '';

            $eventdata->smallmessage     = $smallmessage;

            $mailresult = message_send($eventdata);
            $result = $result && $mailresult;
        }
        // Record that the notification was sent.
        $cc->set_messagesent(true);
        $cc->save_controller();
        return $result;
    }

    public static function add_coursemaps($classestoadd, $course, $context, $instance, $user = null) {
        global $DB;
        global $USER;

        if (is_null($user)) {
            $user = $USER;
        }

        $term = $instance->{enrol_wisc_plugin::termfield};
        $allowdups = enrol_get_plugin('wisc')->get_config('allowdups');
        foreach ($classestoadd as $class) {
            // See if it's a duplicate
            $conds = array('term'=>$term, 'class_number' =>$class->classNumber, 'deleted'=>0);
            if ($allowdups) {
                $conds['courseid'] = $course->id; // only search in this course if dups are okay
            }
            if ($DB->record_exists('enrol_wisc_coursemap', $conds)) {
                throw new moodle_exception(get_string('errorduplicatemap', 'enrol_wisc'));
            }
        }
        $now = time();
        foreach ($classestoadd as $class) {
            if ($class->termCode != $term) {
                throw new moodle_exception(get_string('errorwrongterm', 'enrol_wisc'));
            }
            if (!empty($class->official)) {
                $needsapproval = false;
            } else {
                // not officially teaching this class
                $needsapproval = true;
            }
            // make the new coursemap
            $classmap = new stdClass();
            $classmap->courseid = $course->id;
            $classmap->enrolid = $instance->id;
            $classmap->term = $term;
            $classmap->subject_code = $class->subjectCode;
            $classmap->subject = $class->subjectShortDescription;
            $classmap->catalog_number = $class->catalogNumber;
            $classmap->section_number = $class->sectionNumber;
            $classmap->session_code = $class->sessionCode;
            $classmap->class_number = $class->classNumber;
            $classmap->isis_course_id = $class->courseId;
            $classmap->type = $class->type;
            $classmap->timemodified = $now;
            $classmap->modifierid = $user->id;
            if ($needsapproval) {
                $classmap->approved = enrol_wisc_plugin::ASSOCIATION_PENDING;
            } else {
                $classmap->approved = enrol_wisc_plugin::ASSOCIATION_APPROVED;
            }
            $classmap->id = $DB->insert_record('enrol_wisc_coursemap', $classmap);
            // Log this action.
            $params = array(
                    'objectid' => $classmap->id,
                    'courseid' => $course->id,
                    'context' => context_course::instance($course->id),
                    'other' => array(
                            'instanceid' => $instance->id,
                            'term' => $classmap->term,
                            'subject' => $classmap->subject,
                            'catalog_number' => $classmap->catalog_number,
                            'section_number' => $classmap->section_number,
                            'class_number' => $classmap->class_number,
                    ),
            );
            $event = \enrol_wisc\event\coursemap_added::create($params);
            $event->trigger();
        }
    }

    public static function remove_coursemaps($classestoremove, $instance) {
        global $DB;

        foreach ($classestoremove as $class) {
            $params = array('enrolid'     =>$instance->id,
                            'class_number'=>$class->classNumber,
                            'term'        =>$class->termCode);
            $classmap = $DB->get_record('enrol_wisc_coursemap', $params);
            $DB->set_field('enrol_wisc_coursemap', 'deleted', 1, $params);
            // Log this action.
            $params = array(
                    'objectid' => $classmap->id,
                    'courseid' => $classmap->courseid,
                    'context' => context_course::instance($classmap->courseid),
                    'other' => array(
                            'instanceid' => $classmap->enrolid,
                            'term' => $classmap->term,
                            'subject' => $classmap->subject,
                            'catalog_number' => $classmap->catalog_number,
                            'section_number' => $classmap->section_number,
                            'class_number' => $classmap->class_number,
                    ),
            );
            $event = \enrol_wisc\event\coursemap_deleted::create($params);
            // No record snapshot added since we're not actually deleteing the record here.
            $event->trigger();
        }
    }

    public static function approve_coursemaps($classestoapprove, $instance) {
        global $DB;

        foreach ($classestoapprove as $class) {
            $params = array('enrolid'     =>$instance->id,
                    'class_number'=>$class->classNumber,
                    'term'        =>$class->termCode);
            $classmap = $DB->get_record('enrol_wisc_coursemap', $params);
            $DB->set_field('enrol_wisc_coursemap', 'approved', enrol_wisc_plugin::ASSOCIATION_APPROVED, $params);

            // Log this action.
            $params = array(
                    'objectid' => $classmap->id,
                    'courseid' => $classmap->courseid,
                    'context' => context_course::instance($classmap->courseid),
                    'other' => array(
                            'instanceid' => $classmap->enrolid,
                            'term' => $classmap->term,
                            'subject' => $classmap->subject,
                            'catalog_number' => $classmap->catalog_number,
                            'section_number' => $classmap->section_number,
                            'class_number' => $classmap->class_number,
                    ),
            );
            $event = \enrol_wisc\event\coursemap_approved::create($params);
            $event->trigger();
        }
    }

    /*
    public static function approve_coursemaps(enrol_wisc_controller $cc) {
        global $DB;
        $course = $DB->get_record('course', array('id'=>$cc->get_courseid()), '*', MUST_EXIST);
        $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'wisc', 'id'=>$cc->get_instanceid()), '*', MUST_EXIST);
        $params = array('enrolid' => $cc->get_instanceid());
        $DB->set_field('enrol_wisc_coursemap', 'approved', enrol_wisc_plugin::ASSOCIATION_APPROVED, $params);
    }
    */

    public static function sync_instance_enrolments($instance, $course, \core\progress\base $progress = null) {
        if (!enrol_is_enabled('wisc')) {
            return;
        }
        try {
            $wisc = enrol_get_plugin('wisc');
            $wisc->sync_instance($instance, $course, $progress);
        } catch (Exception $e) {
            error_log("Error syncing course enrollments: ".$e->getMessage());
        }
    }

    public static function helper_unenrol_internal_user($courseid, $userid) {
        global $DB;
        if (!$enrol = enrol_get_plugin('manual')) {
            return false;
        }
        if (!$instances = $DB->get_records('enrol', array('enrol'=>'manual', 'courseid'=>$courseid, 'status'=>ENROL_INSTANCE_ENABLED), 'sortorder,id ASC')) {
            return false;
        }
        $instance = reset($instances);

        $enrol->unenrol_user($instance, $userid);

    }

    /**
     * Send emails for pending roster associations.
     */
    public static function send_approval_emails() {
        global $DB;

        $approvalemail = get_config('enrol_wisc', 'approvalemail');
        if (!enrol_is_enabled('wisc') || empty($approvalemail)) {
            return;
        }
        // Send out approval notifications

        $site = get_site();

        // generate fake users so we can use the moodle mail functions
        $supportuser = core_user::get_support_user();

        // approval email doesn't correspond to a real moodle account, so base
        // if off the guest user
        $approvaluser = clone(guest_user());
        $approvaluser->email = $approvalemail;
        $approvaluser->firstname = "Course approval";
        $approvaluser->lastname = '';
        $approvaluser->maildisplay = true;
        $approvaluser->mailformat = 1;

        // This should really be SELECT DISTINCT modifierid,enrolid , but moodle won't let us do that without a unique key
        $records = $DB->get_records('enrol_wisc_coursemap', array('approved'=>enrol_wisc_plugin::ASSOCIATION_PENDING, 'deleted'=>0));

        $done = array();
        foreach ($records as $record) {
            $enrolid = $record->enrolid;
            $userid = $record->modifierid;
            $courseid = $record->courseid;

            if (!empty($done[$enrolid][$userid])) {
                continue; // only send one email per user/enrolinstance
            }
            $done[$enrolid][$userid] = true;

            $coursename = $DB->get_field('course', 'fullname', array('id'=>$courseid));

            $user = $DB->get_record('user', array('id'=>$userid));
            if ($user) {
                $userfullname = fullname($user);
                $useremail = $user->email;
                profile_load_custom_fields($user);
                if (isset($user->profile['uwRoles'])) {
                    $useruwroles = $user->profile['uwRoles'];
                } else {
                    $useruwroles = '';
                }
            } else {
                $userfullname = "UNKNOWN";
                $useremail = "UNKNOWN";
                $useruwroles = "UNKNOWN";
                $user = $supportuser;  // send the email from the moodle admin account
            }

            // Assemble the list of all timetable instructors
            $context = context_course::instance($courseid);
            $teacherroleid = get_config('enrol_wisc', 'teacherrole');
            $params = array('contextid'=>$context->id, 'roleid'=>$teacherroleid, 'component'=>'enrol_wisc');
            $teacherids = $DB->get_records_menu('role_assignments', $params, '', 'id,userid');
            $teacherids = array_unique($teacherids);
            $teachers = $DB->get_records_list('user', 'id', $teacherids, 'lastname,firstname', 'id,lastname,firstname,email');

            $teacherlist = array();
            foreach ($teachers as $teacher) {
                if ($teacher->id != $userid) {
                    $teacherlist[] = "  $teacher->firstname $teacher->lastname  ( $teacher->email )";
                }
            }
            $teacherlist = join("\n", $teacherlist);
            if (empty($teacherlist)) {
                $teacherlist = "None";
            }

            $url = new moodle_url('/enrol/wisc/instance.php', array('courseid'=>$courseid, 'id'=>$enrolid));

            $data = new stdClass();
            $data->site = $site->shortname;
            $data->course = $coursename;
            $data->user = $userfullname;
            $data->email = $useremail;
            $data->roles = $useruwroles;
            $data->teachers = $teacherlist;
            $data->url = $url->out(false);

            $message = get_string('approvalemail_message', 'enrol_wisc', $data);
            $subject = get_string('approvalemail_subject', 'enrol_wisc', $data);
            $messagehtml = text_to_html($message, false, false, true);

            $user->maildisplay = 1; // make sure the sending email address gets used
            if (email_to_user($approvaluser, $user, $subject, $message, $messagehtml)) {
                $DB->set_field('enrol_wisc_coursemap', 'approved', enrol_wisc_plugin::ASSOCIATION_MAILED, array('enrolid'=>$enrolid, 'modifierid'=>$userid));
            } else {
                error_log("error sending approval email");
            }

        }
    }

    public static function post_custom_import_tasks(enrol_wisc_controller $cc) {
        // Do any post import tasks.
        if (function_exists('local_archive_post_import_tasks')) {
            local_archive_post_import_tasks($cc->get_courseid(), false);
        }
    }

}

<?php
/**
 * Show course picker for adding a course to the roster associations page.
 *
 * This interface is only used when javascript is disabled.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/enrol/wisc/edit_forms.php');
require_once($CFG->dirroot.'/enrol/wisc/accesslib.php');

const WISC_COURSEPICKER_STAGE_SCHOOL  = 1;
const WISC_COURSEPICKER_STAGE_SUBJECT = 2;
const WISC_COURSEPICKER_STAGE_COURSE  = 3;

$stage        = optional_param('stage', WISC_COURSEPICKER_STAGE_SCHOOL, PARAM_INT);
$return       = required_param('return', PARAM_URL);
$term         = required_param('term', PARAM_INT);
$school       = optional_param('school','', PARAM_ALPHA);
$subject      = optional_param('subject',0, PARAM_INT);

require_login();
// not checking perms since this page is accessed from multiple contexts and
// it doesn't do anything beyond showing a list of classes

$thisurl = new moodle_url('/enrol/wisc/coursepicker.php', array('term'=>$term, 'return'=>$return));
$PAGE->set_url($thisurl);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());

$params = array();
$params['return']   = $return;
$params['term']     = $term;

if ($stage == WISC_COURSEPICKER_STAGE_SCHOOL) {
    $params['stage'] = $stage;
    $mform = new enrol_wisc_coursepicker_school_form(null, $params);
    if ($mform->is_cancelled()) {
        redirect($return);
    } else if ($data = $mform->get_data()) {
        $stage = WISC_COURSEPICKER_STAGE_SUBJECT;
    }
}

if ($stage == WISC_COURSEPICKER_STAGE_SUBJECT) {
    $params['stage']    = $stage;
    $params['school']   = $school;
    $mform = new enrol_wisc_coursepicker_subject_form(null, $params);
    if ($mform->is_cancelled()) {
        redirect($return);
    } else if ($data = $mform->get_data()) {
        $stage = WISC_COURSEPICKER_STAGE_COURSE;
    }
}

if ($stage == WISC_COURSEPICKER_STAGE_COURSE) {
    $params['stage']    = $stage;
    $params['school']   = $school;
    $params['subject']  = $subject;
    $mform = new enrol_wisc_coursepicker_course_form(null, $params);
    if ($mform->is_cancelled()) {
        redirect($return);
    } else if ($data = $mform->get_data()) {
        $returnurl = new moodle_url($return);
        $returnurl->param('newcourseid', $data->course);
        $returnurl->param('newsubject', $data->subject);
        $returnurl->param('addcourse', '1');
        redirect($returnurl);
    }
}

echo $OUTPUT->header();
$mform->display();
echo $OUTPUT->footer();
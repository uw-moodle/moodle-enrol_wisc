<?php
/******************************************************************************
* WISC Enrollments Web Services - External Library
*
* The web services functions for the WISC Enrollments plugin.
*
* Author: Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/externallib.php');

class enrol_wisc_external extends external_api {
    /**
     * Definition of course_pages parameters.
     * @param subject_code 	= UW department code
     * @param catalog_number	= course catalog number
     * @param term (optional)	= UW term code
     */
    public static function course_pages_parameters() {
        return new external_function_parameters(array(
            'subject_code'   => new external_value(PARAM_INT, 'subject code', VALUE_REQUIRED),
            'catalog_number' => new external_value(PARAM_INT, 'catalog_number', VALUE_REQUIRED),
            'term' 		 	 => new external_value(PARAM_INT, 'term', VALUE_REQUIRED),
        ));
    }

    public static function course_pages($subject_code, $catalog_number, $term) {
        global $DB;

        /* Validate parameters */
        self::validate_parameters(self::course_pages_parameters(),
            array('subject_code'=>$subject_code,'catalog_number'=>$catalog_number,'term'=>$term));

        /* Build SQL query for record retrieval */
        $columns = 'a.*, b.fullname, b.visible';
        $tables = '{enrol_wisc_coursemap} a, {course} b';

        $params = array('subject' => $subject_code, 'catalog' => $catalog_number);
        $w = array('a.courseid = b.id',
                   'a.subject_code = :subject',
                   'a.catalog_number = :catalog');
        if(isset($term)){
            $params['term'] = $term;
            $w[] = 'a.term = :term';
        }
        /* Only show visible courses without the capability */
        if(!has_capability('moodle/course:viewhiddencourses', context_system::instance())){
            $params['visible'] = 1;
            $w[] = 'b.visible = :visible';
        }
        $where = implode(" AND ", $w);

        $query = "SELECT $columns FROM $tables WHERE $where";

        /* Retrieve records */
        $records = $DB->get_records_sql($query, $params);

        /* Compile link objects */
        $links = array();
        foreach($records as	$record){
            if(!isset($links[$record->courseid])){
                $link = array('title' => $record->fullname,
                              'term' => $record->term,
                              'session' => $record->session_code,
                              'moodle_id' => $record->courseid,
                              'visible' => $record->visible);
                $links[$record->courseid] = $link;
                $links[$record->courseid]['sections'] = array();
            }
            $links[$record->courseid]['sections'][] = $record->section_number;
        }

        /* Process for web service format */
        $data = array();
        foreach($links as $courseid => $course){
            $data[] = $course;
        }

        return $data;
    }

    /**
     * Returns description of COURSE_PAGES response variables.
     * @return array[value] where value = title, term, session, moodle_id, visible, sections
     */
    public static function course_pages_returns() {
        return new external_multiple_structure(
            new external_single_structure(array(
                'title' 	=> new external_value(PARAM_TEXT, 'course title'),
                'term'		=> new external_value(PARAM_INT, 'UW term code'),
                'session'	=> new external_value(PARAM_ALPHANUM, 'UW session code'),
                'moodle_id'	=> new external_value(PARAM_INT, 'moodle course id'),
                'visible'	=> new external_value(PARAM_BOOL, 'course visibility'),
                'sections' 	=> new external_multiple_structure(new external_value(PARAM_TEXT, 'section number'))
        ))
    );
    }

       /**
     * Definition of user_course_pages parameters.
     * @param userid 		= UW student id
     * @param term (optional)	= UW term code
     */
    public static function user_courses_parameters(){
        return new external_function_parameters(array(
            'netid' 	=> new external_value(PARAM_ALPHANUMEXT, 'netid', VALUE_DEFAULT, ''),
            'eppn' 	    => new external_value(PARAM_RAW, 'netid', VALUE_DEFAULT, ''),
            'term'		=> new external_value(PARAM_INT, 'term', VALUE_DEFAULT, 0),
        ));
    }

    /**
     * Return all student roles
     *
     * TODO: This should use a local setting for enrol_wisc, once we decide this is what we want.
     *
     * @return array of role ids
     */
    protected static function get_student_roleids() {
        global $CFG;
        return explode(',', $CFG->gradebookroles);
    }

    /**
     * Returns courseids for which the user has a student role assigned directly on the course.
     *
     * Note this function does not filter by active enrollments or access.
     *
     * @param int $userid
     * @param array $courseids
     * @return array of course ids
     */
    protected static function get_users_courses_with_student_role($userid) {
        global $DB;

        $studentroles = self::get_student_roleids();
        if (empty($studentroles)) {
            return array();
        }

        // Only returns direct student role assignments on course
        list ($rolesql, $params) = $DB->get_in_or_equal($studentroles, SQL_PARAMS_NAMED);
        $params['userid'] = $userid;
        $params['contextcourse'] = CONTEXT_COURSE;
        $sql = "SELECT ra.id, c.instanceid as courseid FROM {role_assignments} ra
                         JOIN {context} c ON c.id = ra.contextid AND c.contextlevel = :contextcourse
                         WHERE ra.userid = :userid
                         AND ra.roleid $rolesql";
        return $DB->get_records_sql_menu($sql, $params);
    }

    /**
     * Get list of courses user is enrolled in (only active enrolments are returned).
     *
     * This function is partially copied from core_enrol_external->get_users_courses(), but modified to remove enrollment
     * counts and to relax the requirement that the current user can access each course.  The later change allows us
     * access this function using a less-privleged service account.
     *
     * @param int $userid
     * @return array of courses
     */
    public static function get_users_courses($userid) {
        global $USER, $DB;

        $courses = enrol_get_users_courses($userid, true, 'id, shortname, fullname, visible, startdate');
        $result = array();
        if (!$courses) {
            return array();
        }

        $studentcourses = self::get_users_courses_with_student_role($userid);

        foreach ($courses as $course) {
            $context = context_course::instance($course->id, IGNORE_MISSING);
            if ($userid != $USER->id and !has_capability('moodle/course:viewparticipants', $context)) {
                // we need capability to view participants
                continue;
            }
            if (in_array($course->id, $studentcourses)) {
                $role = "student";
            } else {
                $role = "non-student";
            }

            $result[] = array('id'=>$course->id,
                              'shortname'=>$course->shortname,
                              'fullname'=>$course->fullname,
                              'role'=>$role,
                              'visible'=>$course->visible,
                              'startdate'=>$course->startdate,
            );
        }

        return $result;
    }

    /**
     * An extension on moodle_enrol_get_users_courses that allows for non-db dependent user parameters.
     * @param string $netid 	= UW netid
     * @param int $term		    = UW term code
     */
    public static function user_courses($netid, $eppn, $term){
        global $CFG, $DB;
        // Validate parameters
        $eppn = trim($eppn);
        $netid = trim($netid);
        $params = self::validate_parameters(self::user_courses_parameters(), array('netid'=>$netid,'eppn'=>$eppn,'term'=>$term));

        if (!empty($eppn) && !empty($netid)) {
            throw new invalid_parameter_exception('Only one of Netid or eppn can be specified.');
        }

        if (!empty($eppn)) {
            $username = strtolower($eppn);
        } else if (!empty($netid)) {
            $username = strtolower($netid) . '@wisc.edu';
        } else {
            throw new invalid_parameter_exception('Netid or eppn required.');
        }

        if (empty($username)) {
            return array();
        }

        $user = $DB->get_record('user',array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id),'id');
        if (!$user) {
            return array();
        }

        $courses = self::get_users_courses($user->id);
        if (!$courses) {
            return array();
        }

        //Append/filter with UW enrollment information

        // Read terms from database
        $allcoursesids = array();
        foreach ($courses as $course) {
            $allcoursesids[] = $course['id'];
        }
        list($courseidsql, $params) = $DB->get_in_or_equal($allcoursesids, SQL_PARAMS_NAMED, 'cid');
        $select = "courseid $courseidsql AND status=:statusenabled AND enrol=:wiscenrol";
        $params['statusenabled'] = ENROL_INSTANCE_ENABLED;
        $params['wiscenrol'] = 'wisc';
        $enrols = $DB->get_records_select('enrol', $select, $params, '', 'id,courseid,'.enrol_wisc_plugin::termfield);

        // Organize terms by course
        $termsbycourse = array();
        foreach ($enrols as $enrol) {
            $termsbycourse[$enrol->courseid][] = (int) $enrol->{enrol_wisc_plugin::termfield};
        }

        // Process each course
        foreach($courses as $key => &$course){
            // Include course term
            if (isset($termsbycourse[$course['id']])) {
                $course['terms'] = $termsbycourse[$course['id']];
            } else {
                $course['terms'] = array();
            }

            // TODO: do we need to add session code?  Unfortunately we're not storing that in the enrol table.  It's also not necessarily constant for a course
            //$course['session'] = '';

            // Remove timetable courses without specified term (if set)
            $correctterm = empty($term) || in_array($term, $course['terms']);
            if (!$correctterm) {
                unset($courses[$key]);
                continue;
            }

            // Find one term for the class
            if (empty($course['terms'])) {
                unset($course['term']);
            } else if (count($course['terms']) == 1) {
                $course['term'] = reset($course['terms']);
            } else {
                // More than one, so fetch the highest term
                // TODO: If we start having multi-term classes, we should probably try to return the active term here.
                sort($course['terms'], SORT_NUMERIC);
                $course['term'] = array_pop($course['terms']);
            }

            // Course visibility:  get_users_courses has already filtered on access to non-visible courses,
            // so we can return the $course->visible flag unchanged.

            // Add course url
            $courseurl = new moodle_url('/course/view.php', array('id' => $course['id']));
            $url = new moodle_url('/auth/shibboleth/index.php', array('target'=> $courseurl->out()));
            $course['url'] = $url->out(false);
        }

        return $courses;
    }

    public static function user_courses_returns(){
        return new external_multiple_structure(
            new external_single_structure(array(
                'term'		=> new external_value(PARAM_INT, 'UW term code', false),
//	    		'session'	=> new external_value(PARAM_ALPHANUM, 'UW session code'),
                'id'		=> new external_value(PARAM_INT, 'moodle course id'),
                'url'       => new external_value(PARAM_RAW, 'moodle course url'),
                'shortname' => new external_value(PARAM_RAW, 'short name of course'),
                'fullname'  => new external_value(PARAM_RAW, 'long name of course'),
                'role'      => new external_value(PARAM_RAW, 'role in class (\'student\' or \'non-student\')'),
                'startdate' => new external_value(PARAM_INT, 'course startdate'),
                'visible'	=> new external_value(PARAM_INT, '1 means visible, 0 means hidden course'),
            ))
    );
    }

    /**
     * Definition of roster change event parameters.
     */
    public static function roster_change_event_parameters() {
        return new external_function_parameters(
            array(
                'events' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'JMS message id', 0),
                            'term' => new external_value(PARAM_INT, 'UW term code'),
                            'classNumber' => new external_value(PARAM_INT, 'class number'),
                            'previousStatus' => new external_value(PARAM_ALPHANUMEXT, 'previous status:  ENROLLED|NOT-ENROLLED|WAITLIST'),
                            'newStatus' => new external_value(PARAM_ALPHANUMEXT, 'new status:  ENROLLED|NOT-ENROLLED|WAITLIST'),
                            'pvi' => new external_value(PARAM_ALPHANUMEXT, 'PVI, required.  This only marked optional to match the course roster service schema.', false),
                            'emplid' => new external_value(PARAM_ALPHANUMEXT, 'emplid'),
                            'actionTime' => new external_value(PARAM_RAW, 'ISO 8601 datetime'),
                        )
                    )
                , 'list of events')
            )
        );
    }

    /**
     * Definition of roster change event returns.
     */
    public static function roster_change_event_returns() {
        return  new external_value(PARAM_RAW, 'result');
    }

    /**
     * Roster change event handler.
     *
     * This call produces no return status, but will throw an exception (SOAP error, etc) on a fatal error. Datasource
     * problems such as not being able to create moodle user from a PVI are logged but do not generate a fatal error.
     */
    public static function roster_change_event(array $events) {
        global $DB, $CFG;
        require_once($CFG->libdir.'/gradelib.php');
        $params = self::validate_parameters(self::roster_change_event_parameters(), array('events'=>$events));

        // Access control is implemented for each event at the course level.

        if (!enrol_is_enabled('wisc')) {
            throw new moodle_exception('enrolnotenabled', 'enrol_wisc');
        }

        $wiscenrol = enrol_get_plugin('wisc');
        $wiscservices = new local_wiscservices_plugin();

        foreach ($params['events'] as $event) {

            //error_log("roster_change_event: Event ".$event['id']." ".$event['previousStatus']." -> ".$event['newStatus']." (".$event['actionTime'].")");

            // Lookup moodle course.
            $conditions = array('term'         => $event['term'],
                                'class_number' => $event['classNumber'],
                                'deleted'      => 0,
                                'approved'     => enrol_wisc_plugin::ASSOCIATION_APPROVED);
            $coursemap = $DB->get_record('enrol_wisc_coursemap', $conditions, '*', IGNORE_MISSING);
            if (!$coursemap) {
                // Not an active course on site.
                //error_log("roster_change_event: No such course.");
                continue;
            }

            $instance = $DB->get_record('enrol', array('id' => $coursemap->enrolid), '*', MUST_EXIST);

            // Skip if no PVI.  We could try to search by emplid, but if CHUB doesn't have the PVI
            // then we likely can't deal with the user anyhow.
            if (empty($event['pvi'])) {
                error_log("roster_change_event: No PVI, ignoring.");
                continue;
            }

            // Get the section group, if it exists.
            if (!empty($coursemap->groupid)) {
                $group = groups_get_group($coursemap->groupid);
            } else {
                $group = false;
            }


            // Check access at the course level.
            require_capability('enrol/wisc:pushrosterchange', context_course::instance($instance->courseid));

            $user = $wiscservices->get_user_by_pvi($event['pvi']);
            if (!$user) {
                error_log("roster_change_event: Unable to create moodle user for ".$event['pvi'].".");
                continue;
            }

            switch ($event['newStatus']) {
                case 'ENROLLED':

                    // Check ferpa flags.
                    profile_load_custom_fields($user);

                    if (!empty($user->profile->ferpaName)) {
                        $roleid = $wiscenrol->get_config('ferparole');
                    } else {
                        $roleid = $wiscenrol->get_config('studentrole');
                    }
                    if (!$roleid) {
                        throw new coding_exception('No student role in configuration.');
                    }
                    //error_log("roster_change_event: Enrolling user in course $instance->courseid with role $roleid.");

                    // enrol_user is idempotent -- no need to check for existing enrollment.
                    $wiscenrol->enrol_user($instance, $user->id, $roleid, 0, 0, null, false);

                    // Make sure we set the enrolment status to active. If the user wasn't
                    // previously enrolled to the course, enrol_user() sets it. But if we
                    // configured the plugin to suspend the user enrolments _AND_ remove
                    // the role assignments on external unenrol, then enrol_user() doesn't
                    // set it back to active on external re-enrolment. So set it
                    // unconditionnally to cover both cases.
                    $DB->set_field('user_enrolments', 'status', ENROL_USER_ACTIVE, array('enrolid'=>$instance->id, 'userid'=>$user->id));
                    // Try to restore grade history, if any. This will fail if the user was still enrolled via another mechanism
                    // so we check first that the user has no grades
                    $sql = "SELECT gg.id
                            FROM {grade_grades} gg
                            JOIN {grade_items} gi ON gi.id = gg.itemid
                            WHERE gi.courseid = :courseid AND gg.userid = :userid";
                    $params = array('userid' => $user->id, 'courseid' => $instance->courseid);
                    if (!$DB->record_exists_sql($sql, $params)) {
                        grade_recover_history_grades($user->id, $instance->courseid);
                    }
                    // Add to section group.
                    if ($group) {
                        // groups_add_member is idempotent -- no need to check for existing membership.
                        groups_add_member($group, $user, 'enrol_wisc');
                    }
                    break;

                case 'NOT_ENROLLED':
                case 'WAITLIST':
                    if ($event['previousStatus'] == 'ENROLLED') {
                        // Unenroll the user if needed.

                        // Remove from section group.
                        if ($group) {
                            groups_remove_member($group, $user);
                        }

                        $keepenrollments = $wiscenrol->get_config('unenrolaction') == ENROL_EXT_REMOVED_KEEP;
                        if ($keepenrollments) {
                            // Admin setting is to keep enrollments, so we do nothing;
                            //error_log("roster_change_event: Keeping enrollment in course $instance->courseid.");
                            continue;
                        }

                        // We need to query chub here since the user may still be enrolled
                        // in another section of the same moodle course.

                        $enrolled = $wiscenrol->is_student_enrolled_in_instance($instance, $event['pvi']);
                        if (!$enrolled) {
                            // Here we only suspend the enrollment.  The wisc course sync cron job will later
                            // decide whether to remove roles, or completely remove the enrollment, clean up the group
                            // membership, etc.

                            $DB->set_field('user_enrolments', 'status', ENROL_USER_SUSPENDED,
                                            array('enrolid'=>$instance->id, 'userid'=>$user->id));
                            //error_log("roster_change_event: Suspending user enrollment in course $instance->courseid.");
                        } else {
                            //error_log("roster_change_event: Preserving user enrollment in course $instance->courseid.");
                        }
                    }
                    break;
                default:
                    throw new moodle_exception('unknowneventstatus', 'enrol_wisc');
            }

        }
        return "OK";
    }
}

?>

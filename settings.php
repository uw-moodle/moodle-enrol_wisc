<?php

/**
 * WISC enrolment plugin settings and presets.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// WISC settings
$ADMIN->add('enrolments', new admin_category('enrolwisc', new lang_string('admincategoryname', 'enrol_wisc')));

if ($ADMIN->fulltree) {

    require_once($CFG->dirroot.'/course/lib.php');
    require_once($CFG->libdir.'/coursecatlib.php');
    require_once($CFG->dirroot.'/enrol/wisc/lib.php');
    require_once($CFG->dirroot.'/enrol/wisc/updatelib.php');
    require_once($CFG->dirroot.'/enrol/wisc/adminlib.php');

    //--- heading ---
    $settings->add(new admin_setting_heading('enrol_wisc_settings', '', get_string('pluginname_desc', 'enrol_wisc')));

    if (!class_exists('SoapClient')) {
        $settings->add(new admin_setting_heading('enrol_phpsoap_noextension', '', get_string('phpsoap_noextension', 'enrol_wisc')));
    } else if (!file_exists($CFG->dirroot.'/local/wiscservices/locallib.php')) {
        $settings->add(new admin_setting_heading('enrol_nowiscservices', '', get_string('nowiscservices', 'enrol_wisc')));
    } else {
        //require_once($CFG->dirroot.'/enrol/wisc/settingslib.php');

        $yesno = array(get_string('no'), get_string('yes'));

        //--- connection settings ---
        $settings->add(new admin_setting_heading('enrol_wisc_server_settings', get_string('server_settings', 'enrol_wisc'), ''));
        $settings->add(new admin_setting_configtext('enrol_wisc/soapurl', get_string('soapurl_key', 'enrol_wisc'), '', 'http://esb.services.wisc.edu/esbv2/CAOS/WebService/caos-ws-1.7/caos.wsdl', PARAM_URL,60));
        $settings->add(new admin_setting_configtext('enrol_wisc/soapuser', get_string('soapuser_key', 'enrol_wisc'), '', '', PARAM_RAW));
        $settings->add(new admin_setting_configpasswordunmask('enrol_wisc/soappass', get_string('soappass_key', 'enrol_wisc'), '', '', PARAM_RAW));

        //--- role mapping settings ---
        $settings->add(new admin_setting_heading('enrol_wisc_roles', get_string('roles', 'enrol_wisc'), ''));
        if (!during_initial_install()) {
            $options = get_default_enrol_roles(context_system::instance());
            $options[0] = get_string('dontsync', 'enrol_wisc');
            $student = get_archetype_roles('student');
            $student = reset($student);
            $settings->add(new admin_setting_configselect('enrol_wisc/studentrole',
                get_string('studentrole_key', 'enrol_wisc'), get_string('studentrole', 'enrol_wisc'), $student->id, $options));
            $settings->add(new admin_setting_configselect('enrol_wisc/ferparole',
                get_string('ferparole_key', 'enrol_wisc'), get_string('ferparole', 'enrol_wisc'), $student->id, $options));
            $settings->add(new admin_setting_configselect('enrol_wisc/auditorrole',
                get_string('auditorrole_key', 'enrol_wisc'), get_string('auditorrole', 'enrol_wisc'), $student->id, $options));

            $editingteacher = get_archetype_roles('editingteacher');
            $editingteacher = reset($editingteacher);
            $settings->add(new admin_setting_configselect('enrol_wisc/teacherrole',
                get_string('teacherrole_key', 'enrol_wisc'), get_string('teacherrole', 'enrol_wisc'), $editingteacher->id, $options));
            $settings->add(new admin_setting_configselect('enrol_wisc/tarole',
                get_string('tarole_key', 'enrol_wisc'), get_string('tarole', 'enrol_wisc'), $editingteacher->id, $options));
            $settings->add(new admin_setting_configselect('enrol_wisc/otherrole',
                get_string('otherrole_key', 'enrol_wisc'), get_string('otherrole', 'enrol_wisc'), $editingteacher->id, $options));
        }

        //--- course enrolment settings ---
        $settings->add(new admin_setting_heading('enrol_wisc_enrolment_settings', get_string('enrolment_settings', 'enrol_wisc'), ''));
        //$options = $yesno;
        //$settings->add(new admin_setting_configselect('enrol_wisc/studentsoncreate', get_string('studentsoncreate_key', 'enrol_wisc'), get_string('studentsoncreate', 'enrol_wisc'), 0, $options));
        $options = $yesno;
        $settings->add(new admin_setting_configselect('enrol_wisc/allowdups', get_string('allowdups_key', 'enrol_wisc'), get_string('allowdups', 'enrol_wisc'), 0, $options));
        $options = $yesno;
        $settings->add(new admin_setting_configselect('enrol_wisc/ignorehiddencourses', get_string('ignorehiddencourses_key', 'enrol_wisc'), get_string('ignorehiddencourses', 'enrol_wisc'), 0, $options));

        $options = array(ENROL_EXT_REMOVED_UNENROL        => get_string('extremovedunenrol', 'enrol'),
                         ENROL_EXT_REMOVED_KEEP           => get_string('extremovedkeep', 'enrol'),
                         ENROL_EXT_REMOVED_SUSPEND        => get_string('extremovedsuspend', 'enrol'),
                         ENROL_EXT_REMOVED_SUSPENDNOROLES => get_string('extremovedsuspendnoroles', 'enrol'));
        $settings->add(new admin_setting_configselect('enrol_wisc/unenrolaction', get_string('unenrolaction_key', 'enrol_wisc'), get_string('unenrolaction', 'enrol_wisc'), ENROL_EXT_REMOVED_UNENROL, $options));

        //--- course creator roles ---
        $settings->add(new admin_setting_heading('enrol_wisc_coursecreatorroles_settings', get_string('coursecreatorroles_settings', 'enrol_wisc'), get_string('coursecreatorroles_help', 'enrol_wisc')));
        $options = array(enrol_wisc_plugin::CREATECOURSE_NONE        => get_string('createcourse_none', 'enrol_wisc'),
                         enrol_wisc_plugin::CREATECOURSE_LIMITED     => get_string('createcourse_limited', 'enrol_wisc'),
                         enrol_wisc_plugin::CREATECOURSE_FULL        => get_string('createcourse_full', 'enrol_wisc'));
        $uwroles = enrol_wisc_plugin::get_coursecreator_uwroles();
        foreach ($uwroles as $uwrole=>$display) {
            $settingname = 'enrol_wisc/uwrole_'.$uwrole;
            $settings->add(new admin_setting_configselect($settingname, $display, '', enrol_wisc_plugin::CREATECOURSE_NONE, $options));
        }
        $settings->add(new admin_setting_configtext('enrol_wisc/approvalemail', get_string('approvalemail_key', 'enrol_wisc'), get_string('approvalemail', 'enrol_wisc'), '', PARAM_EMAIL));

        //--- course creator settings ---
        $settings->add(new admin_setting_heading('enrol_wisc_coursecreator_settings', get_string('coursecreator_settings', 'enrol_wisc'), ''));

        $settings->add(new admin_setting_confightmleditor('enrol_wisc/creatorintro', get_string('creatorintro', 'enrol_wisc'), get_string('creatorintro_desc', 'enrol_wisc'), ''));

        $allterms = enrol_wisc_plugin::get_all_terms_menu();
        $settings->add(new admin_setting_configmultiselect('enrol_wisc/creatorterms', get_string('creatorterms', 'enrol_wisc'), get_string('creatorterms_desc', 'enrol_wisc'), array_keys($allterms), $allterms));

        $settings->add(new admin_setting_configtextarea('enrol_wisc/otherorgs', get_string('otherorgs', 'enrol_wisc'), get_string('otherorgs_desc', 'enrol_wisc'), ''));

        $tempcategory = wisc_get_temp_category();
        $displaylist = coursecat::make_categories_list();

        if (isset($CFG->defaultrequestcategory)) {
            $defaultcategory = $CFG->defaultrequestcategory;
        } else {
            $defaultcategory = 1;
        }
        $settings->add(new admin_setting_configselect('enrol_wisc/category', get_string('category_key', 'enrol_wisc'), get_string('category', 'enrol_wisc'),
                $defaultcategory, $displaylist));

        $settings->add(new admin_setting_configselect('enrol_wisc/tempcategory', get_string('tempcategory_key', 'enrol_wisc'), get_string('tempcategory', 'enrol_wisc'),
                $tempcategory, $displaylist));

        $settings->add(new admin_setting_configcheckbox('enrol_wisc/autocategory', get_string('autocategory_key', 'enrol_wisc'), get_string('autocategory', 'enrol_wisc'), 1));
        $settings->add(new admin_setting_configcheckbox('enrol_wisc/autocreatecategory', get_string('autocreatecategory_key', 'enrol_wisc'), get_string('autocreatecategory', 'enrol_wisc'), 1));

        $settings->add(new admin_setting_configcheckbox('enrol_wisc/offlinecreate', get_string('offlinecreate_key', 'enrol_wisc'), get_string('offlinecreate', 'enrol_wisc'), 0));
        $settings->add(new admin_setting_configtext('enrol_wisc/phppath', get_string('phppath_key', 'enrol_wisc'), get_string('phppath', 'enrol_wisc'), '/usr/bin/php', PARAM_TEXT,20));
        $settings->add(new admin_setting_configtext('enrol_wisc/maxconcurrency', get_string('maxconcurrency_key', 'enrol_wisc'), get_string('maxconcurrency', 'enrol_wisc'), 0, PARAM_INT, 3));

        $settings->add(new admin_setting_heading('external_course_settings', '', get_string('usercoursedsn_heading', 'enrol_wisc')));
        $settings->add(new admin_setting_configtextarea('enrol_wisc/usercoursesdsn', get_string('usercoursesdsn_setting', 'enrol_wisc'), get_string('usercoursesdsn_setting_desc', 'enrol_wisc'), ''));

    }
}

// Add settings
$ADMIN->add('enrolwisc', $settings);

// Clear $settings so that it doesn't get added twice
$settings = null;

// Cohort permissions
$temp = new admin_settingpage('enrolwisccohorts', new lang_string('managecohortsmenu', 'enrol_wisc'));
if ($ADMIN->fulltree) {
    $temp->add(new enrol_wisc_admin_setting_cohort());
}
$ADMIN->add('enrolwisc', $temp);

$temp = null;


// UWMOODLE-287: the build categories menu item isn't very useful anymore, so removing from menu.
// Build categories page
//$temp = new admin_externalpage('enrolwiscbuildcats', new lang_string('buildcategoriesmenu', 'enrol_wisc'), "$CFG->wwwroot/enrol/wisc/buildcategories.php", array('moodle/site:config'));
//$ADMIN->add('enrolwisc', $temp);

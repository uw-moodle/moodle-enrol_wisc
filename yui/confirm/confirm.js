YUI.add('moodle-enrol_wisc-confirm', function(Y) {

M.enrol_wisc = M.enrol_wisc || {};
/**
 * Adds confirmation dialogues to buttons on the page.
 *
 * @param {object} config
 */
M.enrol_wisc.watch_buttons = function(config) {
    Y.all(config.selector).each(function(){
        this._confirmationListener = this._confirmationListener || this.on('click', function(e){
            // Prevent the default event (sumbit) from firing
            e.preventDefault();
            // Create the confirm box
            var confirm = new M.core.confirm(config);
            // If the user clicks yes
            confirm.on('complete-yes', function(e){
                // Detach the listener for the confirm box so it doesn't fire again.
                this._confirmationListener.detach();
                // Simulate the original cancel button click
                this.simulate('click');
            }, this);
            // Show the confirm box
            confirm.show();
        }, this);
    });
}

}, '@VERSION@', {'requires':['base','node','node-event-simulate','moodle-core-notification-confirm']});

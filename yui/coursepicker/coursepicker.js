YUI.add('moodle-enrol_wisc-coursepicker', function(Y) {
 
    var PICKERNAME = 'wisc_coursepicker';
 
    var PICKER = function() {
    	PICKER.superclass.constructor.apply(this, arguments);
    }
 
    Y.extend(PICKER, Y.Base, {
 
        _loadingNode : null,
 
        initializer : function(params) {
	        this.set('base', Y.Node.create('<div class="coursepicker-panel hidden"></div>')
	        		.append(Y.Node.create('<div class="cp-wrap"></div>')
	                    .append(Y.Node.create('<div class="cp-header header"></div>')
	                        .append(Y.Node.create('<div class="close"></div>'))
	                        .append(Y.Node.create('<h2>Select a UW Course</h2>')))
	                    .append(Y.Node.create('<div class="cp-content"></div>')
                    		.append(Y.Node.create('<div class="cp-loading-lightbox hidden"></div>')
	                            .append(Y.Node.create('<img alt="loading" class="loading-icon" />')
	                                .setAttribute('src', M.util.image_url('i/loading', 'moodle')))
	                        .setStyle('opacity', 0.5))
	                        .append(Y.Node.create('<div class="cp-org-option cp-item"></div>')
	                                .append(Y.Node.create('<div class="cp-item-title"><label>School or college</label></div>'))
                                    .append(Y.Node.create('<div class="cp-item-element"><select><option value="">Select...</option></select></div>'))
                            )
	                        .append(Y.Node.create('<div class="cp-subject-option cp-item"></div>')
	                                .append(Y.Node.create('<div class="cp-item-title"><label>Department</label></div>'))
	                                .append(Y.Node.create('<div class="cp-item-element"><select disabled="disabled"><option value="">Select...</option></select></div>'))
	                        )
	                        .append(Y.Node.create('<div class="cp-course-option cp-item"></div>')
	                                .append(Y.Node.create('<div class="cp-item-title"><label>Course</label></div>'))
	                                .append(Y.Node.create('<div class="cp-item-element"><select disabled="disabled"><option value="">Select...</option></select></div>'))
	                        )
	                        .append(Y.Node.create('<div class="cp-submit"></div>')
	                        		.append(Y.Node.create('<input type="submit" disabled="disabled" value="Add course to list" />'))
	                        )
	                    )
	        		)
	            );
	        
	        this.get('base').one('.cp-header .close').on('click', this.hide, this);
	        this._loadingNode = this.get('base').one('.cp-content .cp-loading-lightbox');
	        Y.one('#addcourse').on('click', this.show, this);
	        this.get('base').one('.cp-org-option select').on('change', this.getSubjects, this);
	        this.get('base').one('.cp-subject-option select').on('change', this.getCourses, this);
	        this.get('base').one('.cp-course-option select').on('change', this.updateSubmit, this);
	        this.get('base').one('.cp-submit input').on('click', this.doSubmit, this);
	        
	        Y.one(document.body).append(this.get('base'));
        },
        populateOrgs : function() {
            this.on('orgsloaded', function(){
                var orgs = this.get('orgs');
                if (!orgs) {
                    alert('No courses available');
                    this.hide();
                    return;
                }
                var s = this.get('base').one('.cp-org-option select');
                for (var i in orgs) {
                    var option = Y.Node.create('<option value="'+i+'">'+orgs[i]+'</option>');
                    s.append(option);
                }
            }, this);
            this.getOrgs();
        },
        getOrgs : function(){
            Y.io(M.cfg.wwwroot+'/enrol/wisc/ajax.php', {
                method:'POST',
                data:'term='+this.get('term')+'&action=getorgs&sesskey='+M.cfg.sesskey,
                on: {
                    start : this.displayLoading,
                    complete: function(tid, outcome, args) {
                        try {
                            var orgs = Y.JSON.parse(outcome.responseText);
                            this.set('orgs', orgs.response);
                        } catch (e) {
                            new M.core.exception(e);
                        }
                        this.getOrgs = function() {};
                        this.fire('orgsloaded');
                    },
                    end : this.removeLoading
                },
                context:this
            });
        },
        getSubjects : function(){
            var org = this.get('base').one('.cp-org-option select').get('value');
            this.get('base').one('.cp-subject-option select').set('disabled', true);
            this.get('base').one('.cp-course-option select').set('disabled', true);
            this.get('base').one('.cp-submit input').set('disabled', true);
            
            if (org == 0) {
                return;
            }
            Y.io(M.cfg.wwwroot+'/enrol/wisc/ajax.php', {
                method:'POST',
                data:'term='+this.get('term')+'&action=getsubjects&sesskey='+M.cfg.sesskey+'&org='+org,
                on: {
                    start : this.displayLoading,
                    complete: this.processSubjectResults,
                    end : this.removeLoading
                },
                context:this
            });
        },
        processSubjectResults : function(tid, outcome, args) {
            try {
                var result = Y.JSON.parse(outcome.responseText);
                var subjects = result.response;
            } catch (e) {
                new M.core.exception(e);
            }
            var s = this.get('base').one('.cp-subject-option select');
            var option = Y.Node.create('<option value="">Select...</option>');
            s.setContent(option);
            for (var i in subjects) {
                var option = Y.Node.create('<option value="'+i+'">'+subjects[i]+'</option>');
                s.append(option);
            }
            if (subjects.length == 0) {
                alert("No subjects found for selected school or college");
            } else {
                s.set('disabled', false);
            }
        },
        getCourses : function(){
        	var subject = this.get('base').one('.cp-subject-option select').get('value');
        	this.get('base').one('.cp-course-option select').set('disabled', true);
        	this.get('base').one('.cp-submit input').set('disabled', true);
        	
        	if (subject == 0) {
        		return;
        	}
            Y.io(M.cfg.wwwroot+'/enrol/wisc/ajax.php', {
                method:'POST',
                data:'term='+this.get('term')+'&action=getcourses&sesskey='+M.cfg.sesskey+'&subject='+subject,
                on: {
            		start : this.displayLoading,
                    complete: this.processCoursesResults,
                    end : this.removeLoading
                },
                context:this
            });
        },
        processCoursesResults : function(tid, outcome, args) {
            try {
                var result = Y.JSON.parse(outcome.responseText);
                var courses = result.response;
            } catch (e) {
                new M.core.exception(e);
            }
            var s = this.get('base').one('.cp-course-option select');
            var option = Y.Node.create('<option value="">Select...</option>');
            s.setContent(option);
            for (var i in courses) {
                var option = Y.Node.create('<option value="'+i+'">'+courses[i]+'</option>');
                s.append(option);
            }
            if (courses.length == 0) {
            	alert("No courses found for selected department and term");
            } else {
            	s.set('disabled', false);
            }
        	var submit = this.get('base').one('.cp-submit');
        	submit.removeClass('hidden');
        	this.updateSubmit();
        },
		updateSubmit: function() {
        	var submit = this.get('base').one('.cp-submit input');
        	if (this.get('base').one('.cp-course-option select').get('value') != 0) {
    			submit.set('disabled', false);
    		} else {
    			submit.set('disabled', true);
    		}
    	},
        displayLoading : function() {
            this._loadingNode.removeClass('hidden');
        },
        removeLoading : function() {
            this._loadingNode.addClass('hidden');
        },
        show : function (e) {
            e.preventDefault();
            e.halt();
            
        	this.populateOrgs();
        	
            var base = this.get('base');
            base.removeClass('hidden');
            var x = (base.get('winWidth') - 400)/2;
            var y = (parseInt(base.get('winHeight'))-base.get('offsetHeight'))/2 + parseInt(base.get('docScrollY'));
            if (y < parseInt(base.get('winHeight'))*0.1) {
                y = parseInt(base.get('winHeight'))*0.1;
            }
            base.setXY([x,y]);

            this._escCloseEvent = Y.on('key', this.hide, document.body, 'down:27', this);
        },
        doSubmit : function(e) {
        	var courseid = this.get('base').one('.cp-course-option select').get('value');
        	var subject = this.get('base').one('.cp-subject-option select').get('value');
            window.location = this.get('submiturl')+'&newcourseid='+courseid+'&newsubject='+subject+'&addcourse=1';
        },
        hide : function(e) {
            if (this._escCloseEvent) {
                this._escCloseEvent.detach();
                this._escCloseEvent = null;
            }
            this.get('base').addClass('hidden');
        },
 
    }, {
        NAME : PICKERNAME,
        ATTRS : {
            term: {
    		},
            base : {
            },
            submiturl : {
            },
        }
    });
 
    M.enrol_wisc = M.enrol_wisc || {};
    M.enrol_wisc.init_coursepicker = function(params) {
        return new PICKER(params);
    }
 
}, '@VERSION@', {
    requires:['base','overlay', 'moodle-enrol-notification']
    //Note: 'moodle-enrol-notification' contains Moodle YUI exception
});
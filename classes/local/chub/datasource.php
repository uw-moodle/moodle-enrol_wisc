<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Datastore interface for CHUB data
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_wisc\local\chub;

use enrol_wisc\local\chub\schema\wisc_timetable_class_uniqueid;
use enrol_wisc\local\chub\schema\wisc_timetable_class;
use enrol_wisc\local\chub\schema\wisc_timetable_course;
use enrol_wisc\local\chub\schema\wisc_timetable_instructor;
use enrol_wisc\local\chub\schema\wisc_timetable_instructor_affiliation;
use enrol_wisc\local\chub\schema\wisc_timetable_instructor_affiliation_class;
use enrol_wisc\local\chub\schema\wisc_timetable_person;
use enrol_wisc\local\chub\schema\wisc_timetable_roster;
use enrol_wisc\local\chub\schema\wisc_timetable_school_or_college;
use enrol_wisc\local\chub\schema\wisc_timetable_student;
use enrol_wisc\local\chub\schema\wisc_timetable_subject;
use enrol_wisc\local\chub\schema\wisc_timetable_term;

defined('MOODLE_INTERNAL') || die;

/**
 * Datastore interface for CHUB data
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
interface datasource extends \local_wiscservices\local\soap\datasource {

    /**
     * Set developer mode (i.e. email addresses are obscured)
     *
     * @param bool $dev
     */
    public function set_devmode($dev);

    /**
     * Get developer mode (i.e. email addresses are obscured)
     *
     * @return bool
     */
    public function get_devmode();

    /**
     * Get the unix timestamp for when the timetable data was last updated
     * @param $termCode
     * @return timestamp
     */
    public function getTermLastUpdated($termCode);

    /**
     * Get the list of all available and future terms
     *
     * @exception soapFault on error
     * @return array of wisc_timetable_term
     */
    public function getAvailableAndFutureTerms();

    /**
     * Get the list of currently available terms
     *
     * @exception soapFault on error
     * @return array of wisc_timetable_term
     */
    public function getAvailableTerms();

    /**
     * Get the specified term
     *
     * @exception soapFault on error
     * @return wisc_timetable_term
     */
    public function getTerm($termCode);

    /**
     * Get current term
     *
     * @exception soapFault on error
     * @param wisc_timetable_term|null
     */
    public function getCurrentTerm();

    /**
     * Get all schools and colleges
     *
     * @exception soapFault on error
     * @return array of wisc_timetable_school_or_college
     */
    public function getAllSchoolColleges();


    /**
     * Get all subjects in term. Returns an empty array if no subjects in term.
     *
     * @exception soapFault on error
     * @param string $termCode
     * @return array of wisc_timetable_subject
     */
    public function getSubjectsInTerm($termCode);

    /**
     * Get one subjects in term.
     *
     * @exception soapFault on error
     * @param string $termCode
     * @param string $subjectCode
     * @return array of wisc_timetable_subject
     */
    public function getSubject($termCode, $subjectCode);
    /**
     * Get a course by catalog information.
     *
     * If $crosslists is true, then all crosslisted subjects are included in the course.
     * Otherwise only the given subject is returned.
     *
     * @exception soapFault on error or if course not found
     * @param string $termCode
     * @param string $subjectCode
     * @param string $catalogNumber
     * @param bool $crosslists
     * @return wisc_timetable_course
     */
    public function getCourseByCatalogNumber($termCode, $subjectCode, $catalogNumber, $crosslists);

    /**
     * Get a course by couseId
     *
     * If $crosslists is true, then all crosslisted subjects are included in the course.
     * Otherwise only the given subject is returned.
     *
     * @exception soapFault on error or if course not found
     * @param string $termCode
     * @param string $subjectCode
     * @param string $courseId
     * @param bool $crosslists
     * @return wisc_timetable_course
     */
    public function getCourseByCourseId($termCode, $subjectCode, $courseId, $crosslists);

    /**
     * Get all courses in a given subject. Returns an empty array if no courses found.
     *
     * @exception soapFault on error
     * @param string $termCode
     * @param string $subjectCode
     * @return array of wisc_timetable_course
     */
    public function getCoursesBySubject($termCode, $subjectCode);

    /**
     * Get a single class by classNumber
     *
     * @exception soapFault on error or if class not found
     * @param string $termCode
     * @param int $classNumber
     * @return wisc_timetable_class
     */
    public function getClassByClassNumber($termCode, $classNumber);

    /**
     * Get all crosslisted classes.
     *
     * @exception soapFault on error or if class not found
     * @param string $termCode
     * @param int $classNumber
     * @return wisc_timetable_class
     */
    public function getCrossListedClasses($termCode, $classNumber);


    /**
     * Get all classes belonging to a catalog number. Returns an empty array if no classes found
     *
     * @exception soapFault on error
     * @param string $termCode
     * @param string $subjectCode
     * @param string $catalogNumber
     * @return array of wisc_timetable_class
     */
    public function getClassesByCatalogNumber($termCode, $subjectCode, $catalogNumber);

    /**
     * Get all classes associated with a given instructor. Returns an empty array if no classes found
     *
     * @exception soapFault on error
     * @param string $termCode
     * @param string $pvi
     * @return array of wisc_timetable_instructor_affiliation
     */
    public function getClassesByInstructor($termCode, $pvi);

    /**
     * Get all instructors affiliations in a course.
     *
     * @exception soapFault on error
     * @param string $termCode
     * @param string $subjectCode
     * @param string $catalogNumber
     * @return array of wisc_timetable_instructor_affiliation
     */
    public function getInstructorAffiliationsForCourse($termCode, $subjectCode, $catalogNumber);

    /**
     * Get all instructors assocated to a class.
     *
     * @exception soapFault on error or if class not found
     * @param string $termCode
     * @param int $classNumber
     * @return array of wisc_timetable_instructor
     */
    public function getInstructors($termCode, $classNumber);

    /**
     * Get the class roster
     *
     * @exception soapFault on error or if class not found
     * @param string $termCode
     * @param int $classNumber
     * @return wisc_timetable_roster
     */
    public function getClassRoster($termCode, $classNumber);

    /**
     * Get students that have dropped
     *
     * Apparently this call only returns students that drop after the first day of classes, which, unfortunately, isn't a date we can query.
     * We assume that the course start date is after the first day of classes, so that students that drop after the class begins will show up here.
     *
     * @exception soapFault on error or if class not found
     * @param string $termCode
     * @param int $classNumber
     * @return array of wisc_timetable_student
     */
    public function getStudentsThatHaveDropped($termCode, $classNumber);

    /**
     * Get the classes a student is enrolled in
     *
     * @exception soapFault on error
     * @param string $termCode
     * @param string $pvi
     * @return array of wisc_timetable_class
     */
    public function getEnrolledClasses($termCode, $pvi);

    /**
     * Get the classids a student is enrolled in
     *
     * @exception soapFault on error
     * @param string $termCode
     * @param string $pvi
     * @return array of wisc_timetable_class_uniqueid
     */
    public function getEnrolledClassUniqueIds($termCode, $pvi);

}
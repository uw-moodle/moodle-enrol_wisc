<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * CHUB class
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_wisc\local\chub\schema;

defined('MOODLE_INTERNAL') || die;

/**
 * CHUB class
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class wisc_timetable_class {
    /** @var int */
    public $termCode;

    /** @var int */
    public $classNumber;

    /** @var int */
    public $sectionNumber;

    /** @var wisc_timetable_subject */
    public $subject;

    /** @var int */
    public $catalogNumber;

    /** @var string */
    public $type;

    /** @var string */
    public $sessionCode;

    /** @var string */
    public $courseId;

    /** @var string */
    public $topic;

    /** @var int */
    public $startDate;

    /** @var int */
    public $endDate;

    /** @var bool */
    public $published;

    /** @var bool */
    public $hasMeetsWith;
}
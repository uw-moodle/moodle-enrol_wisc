<?php
/**
 * CHUB datastore implementation class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_wisc\local\chub;

use enrol_wisc\local\chub\schema\wisc_timetable_class_uniqueid;
use enrol_wisc\local\chub\schema\wisc_timetable_class;
use enrol_wisc\local\chub\schema\wisc_timetable_course;
use enrol_wisc\local\chub\schema\wisc_timetable_instructor;
use enrol_wisc\local\chub\schema\wisc_timetable_instructor_affiliation;
use enrol_wisc\local\chub\schema\wisc_timetable_instructor_affiliation_class;
use enrol_wisc\local\chub\schema\wisc_timetable_person;
use enrol_wisc\local\chub\schema\wisc_timetable_roster;
use enrol_wisc\local\chub\schema\wisc_timetable_school_or_college;
use enrol_wisc\local\chub\schema\wisc_timetable_student;
use enrol_wisc\local\chub\schema\wisc_timetable_subject;
use enrol_wisc\local\chub\schema\wisc_timetable_term;

defined('MOODLE_INTERNAL') || die();

/**
 * This class implements the U.W. Timetable data store using Course Hub
 * Information System (CHUB) data.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class chub_datasource  implements datasource {

    /** @var int */
    protected $timeout;

    /** @var bool */
    protected $devmode;

    public function __construct() {
        $this->chubClient = chub_soapclient::get();
        $this->devmode = get_config('local_wiscservices', 'development');
    }

    /**
     * Set developer mode (i.e. email addresses are obscured)
     *
     * @param bool $dev
     */
    public function set_devmode($dev) {
        $this->devmode = $dev;
    }

    /**
     * Get developer mode (i.e. email addresses are obscured)
     *
     * @return bool
     */
    public function get_devmode() {
        return $this->devmode;
    }

    /**
     * Set SOAP timeout.
     *
     * @param int $timeout
     */
    public function set_timeout($timeout) {
       $this->timeout = $timeout;
    }

    /**
     * Get SOAP timeout.
     *
     * @return int
     */
    public function get_timeout() {
        return $this->timeout;
    }

    protected function initTimeout() {
       if (!is_null($this->timeout)) {
           $this->chubClient->__setTimeout($this->timeout);
       }
    }

    public function getTermLastUpdated($termCode) {
        $this->initTimeout();
        $params = array('termCode'=>$termCode);
        $sr = $this->chubClient->GetTermLastUpdated($params);
        return strtotime($sr->lastUpdated);
    }

    public function getAvailableAndFutureTerms() {
        $this->initTimeout();
        $sr = $this->chubClient->GetAvailableAndFutureTerms();
        $result = array();
        foreach( $sr->term as $term ) {
            $newterm = $this->createTerm($term);
            $result[] = $newterm;
        }
        return $result;
    }

    public function getAvailableTerms() {
        $this->initTimeout();
        $sr = $this->chubClient->GetAvailableTerms();
        $result = array();
        foreach( $sr->term as $term ) {
            $newterm = $this->createTerm($term);
            $result[] = $newterm;
        }
        return $result;
    }

    public function getTerm($termCode) {
        $this->initTimeout();
        $params = array('termCode'=>$termCode);
        $sr = $this->chubClient->GetTerm($params);
        $term = $this->createTerm($sr->term);
        return $term;
    }

    public function getCurrentTerm() {
        // Cache result
        static $currentTerm;

        if (!$currentTerm) {
            $now = time();
            $terms = $this->getAvailableTerms();
            $futureterms = array();
            foreach ($terms as $term) {
                if ($term->endDate > $now) {
                    $futureterms[$term->termCode] = $term;
                }
            }
            // use lowest termcode
            ksort($futureterms, SORT_NUMERIC);
            $currentTerm = reset($futureterms);
        }
        return $currentTerm;
    }

    public function getAllSchoolColleges() {
        $this->initTimeout();
        $sr = $this->chubClient->GetAllSchoolColleges();
        $result = array();
        if (!empty($sr->schoolCollege)) {
            foreach( $sr->schoolCollege as $org ) {
                $neworg = $this->createSchoolOrCollege($org);
                $result[] = $neworg;
            }
        }
        return $result;
    }

    public function getSubjectsInTerm($termCode) {
        $this->initTimeout();
        $params = array('termCode'=>$termCode);
        $sr = $this->chubClient->GetSubjectsInTerm($params);
        $result = array();
        if (!empty($sr->subject)) {
            foreach( $sr->subject as $subject ) {
                $newsubject = $this->createSubject($subject);
                $result[] = $newsubject;
            }
        }
        return $result;
    }

    public function getSubject($termCode, $subjectCode) {
        $this->initTimeout();
        $params = array('termCode'=>$termCode, 'subjectCode'=>$subjectCode);
        $sr = $this->chubClient->GetSubject($params);
        $subject = $this->createSubject($sr->subject);
        return $subject;
    }

    public function getCourseByCatalogNumber($termCode, $subjectCode, $catalogNumber, $crossLists) {
        $this->initTimeout();
        $params = array('termCode'     =>$termCode,
                        'subjectCode'  =>$subjectCode,
                        'catalogNumber'=>$catalogNumber);
        $sr = $this->chubClient->GetCourseByCatalogNumber($params);
        if (empty($sr->course)) {
            // Should never happen because GetCourseByCatalogNumber() throws exceptions on course not found.
            throw new Exception("Course not found");
        }
        if ($crossLists && $sr->course->crossListed) {
            $params2 = array('termCode'     =>$termCode,
                            'courseId'      =>$sr->course->courseId);
            $sr2 = $this->chubClient->GetCrossListedSubjects($params2);
            $subjects = $sr2->subject;
        } else {
            $subjects = array($sr->course->subject);
        }
        return $this->createCourse($sr->course, $subjects);
    }

    public function getCourseByCourseId($termCode, $subjectCode, $courseId, $crossLists) {
        $this->initTimeout();
        $params = array('termCode'     =>$termCode,
                        'subjectCode'  =>$subjectCode,
                        'courseId'     =>$courseId);

        if ($crossLists) {
            $sr = $this->chubClient->GetCourseWithCrossListedSubjects($params);
            if (!empty($sr->subject)) {
                $subjects = $sr->subject;
            } else {
                // If there is no crosslist then CHUB returns no subjects outside the course object.
                $subjects = array($sr->course->subject);
            }
        } else {
            $sr = $this->chubClient->GetCourseByCourseId($params);
            $subjects = array($sr->course->subject);
        }

/*  //  Alternate method: only call GetCourseWithCrossListedSubjects for XL classes
    //  At one point CHUB was broken and we had to do it this way.

        $sr = $this->chubClient->GetCourseByCourseId($params);
        $subjects = array($sr->course->subject);

        if ($sr->course->crossListed && $crossLists) {
            $sr = $this->chubClient->GetCourseWithCrossListedSubjects($params);
            if (!empty($sr->subject)) {
                $subjects = $sr->subject;
            } else {
                // If there is no crosslist then CHUB returns no subjects outside the course object.
                $subjects = array($sr->course->subject);
            }
        }
*/

        return $this->createCourse($sr->course, $subjects);
    }

    public function getCoursesBySubject($termCode, $subjectCode) {
        $this->initTimeout();
        $params = array('termCode'     =>$termCode,
                        'subjectCode'  =>$subjectCode);
        $sr = $this->chubClient->GetCoursesBySubject($params);
        $result = array();
        if (!empty($sr->course)) {
            foreach ($sr->course as $course) {
                $result[] = $this->createCourse($course, array($course->subject));
            }
        }
        return $result;
    }

    public function getClassByClassNumber($termCode, $classNumber) {
        $this->initTimeout();
        $params = array('classUniqueId'=>array('termCode'     =>$termCode,
                                               'classNumber'  =>$classNumber));
        $sr = $this->chubClient->GetClass($params);
        return $this->createClass($sr->class);
    }

    public function getCrossListedClasses($termCode, $classNumber) {
        $this->initTimeout();
        $params = array('classUniqueId'=>array('termCode'     =>$termCode,
                'classNumber'  =>$classNumber));
        $sr = $this->chubClient->GetCrossListedClasses($params);
        $result = array();
        foreach ($sr->class as $class) {
            $result[] = $this->createClass($class);
        }
        return $result;
    }

    public function getClassesByCatalogNumber($termCode, $subjectCode, $catalogNumber) {
        $this->initTimeout();
        $params = array('termCode'     =>$termCode,
                        'subjectCode'  =>$subjectCode,
                        'catalogNumber'=>$catalogNumber);
        $sr = $this->chubClient->GetClasses($params);
        if (empty($sr) || empty($sr->class)) {
            return array();
        }
        $result = array();
        foreach ($sr->class as $class) {
            $result[] = $this->createClass($class);
        }
        return $result;
    }

    public function getClassesByInstructor($termCode, $pvi) {
        $this->initTimeout();
        $params = array('termCode'   =>$termCode,
                        'personQuery'=>array('pvi'=>$pvi));
        $sr = $this->chubClient->GetInstructorAffiliationClassesForTeacher($params);
        if (empty($sr->instructorAffiliationClass)) {
            return array();
        }
        $result = array();
        foreach ($sr->instructorAffiliationClass as $a) {
            $result[] = $this->createInstructorAffiliationClass($a);
        }
        return $result;
    }

    public function getInstructorAffiliationsForCourse($termCode, $subjectCode, $catalogNumber) {
        $this->initTimeout();
        $params = array('termCode'     =>$termCode,
                        'subjectCode'  =>$subjectCode,
                        'catalogNumber'=>$catalogNumber);
        $sr = $this->chubClient->GetInstructorAffiliationsForCourse($params);
        if (empty($sr->instructorAffiliation)) {
            return array();
        }
        $result = array();
        foreach ($sr->instructorAffiliation as $affiliation) {
            $result[] = $this->createInstructorAffiliation($affiliation);
        }
        return $result;
    }

    public function getInstructors($termCode, $classNumber) {
        $this->initTimeout();
        $params = array('classUniqueId'=>array('termCode'   =>$termCode,
                                               'classNumber'=>$classNumber));
        $sr = $this->chubClient->GetInstructorAffiliations($params);
        $result = array();
        if (empty($sr->instructorAffiliation)) {
            return array();
        }
        foreach ($sr->instructorAffiliation as $affiliation) {
            $instructor = $this->createInstructor($affiliation);
            $result[] = $instructor;
        }
        return $result;
    }

    public function getClassRoster($termCode, $classNumber) {
        $this->initTimeout();
        $params = array('classUniqueId'=>array('termCode'   =>$termCode,
                                               'classNumber'=>$classNumber));
        $sr = $this->chubClient->GetClassRoster($params);
        $result = $this->createRoster($sr->classRoster);
        return $result;
    }

    public function getStudentsThatHaveDropped($termCode, $classNumber) {
        $this->initTimeout();
        $params = array('classUniqueId'=>array('termCode'   =>$termCode,
                                               'classNumber'=>$classNumber));
        $sr = $this->chubClient->GetStudentsThatHaveDropped($params);
        if (empty($sr->droppedStudent)) {
            return array();
        }
        foreach ($sr->droppedStudent as $dropped) {
            $student = $this->createStudent($dropped->student);
            $result[] = $student;
        }
        return $result;
    }

    public function getEnrolledClasses($termCode, $pvi) {
        $this->initTimeout();
        $params = array('termCode'   =>$termCode,
                        'personQuery'=>array('pvi'=>$pvi));
        $sr = $this->chubClient->GetEnrolledClasses($params);
        if (empty($sr->class)) {
            return array();
        }
        $result = array();
        foreach ($sr->class as $class) {
            $result[] = $this->createClass($class);
        }
        return $result;
    }

    public function getEnrolledClassUniqueIds($termCode, $pvi) {
        $this->initTimeout();
        $params = array('termCode'   =>$termCode,
                'personQuery'=>array('pvi'=>$pvi));
        $sr = $this->chubClient->getEnrolledClassUniqueIds($params);
        if (empty($sr->classUniqueId)) {
            return array();
        }
        $result = array();
        foreach ($sr->classUniqueId as $classUniqieId) {
            $result[] = $this->createClassUniqueId($classUniqieId);
        }
        return $result;
    }

    protected function createTerm($term) {
        $o = new wisc_timetable_term();
        $o->termCode = $term->termCode;
        $o->shortDescription = $term->shortDescription;
        $o->longDescription = $term->longDescription;
        // strtotime() seems to handle ISO 8601 dates
        $o->beginDate = strtotime($term->beginDate);
        $o->endDate = strtotime($term->endDate);
        return $o;
    }

    protected function createSchoolOrCollege($org) {
        $o = new wisc_timetable_school_or_college();
        $o->academicOrgCode = $org->academicOrgCode;
        $o->shortDescription = $org->shortDescription;
        return $o;
    }

    protected function createSubject($subject) {
        $o = new wisc_timetable_subject();
        $o->subjectCode = $subject->subjectCode;
        $o->description = $subject->description;
        $o->shortDescription = $subject->shortDescription;
        $o->schoolCollege = $this->createSchoolOrCollege($subject->schoolCollege);
        return $o;
    }

    protected function createCourse($course, $subjects) {
        $o = new wisc_timetable_course();
        $o->termCode = $course->termCode;
        $o->courseId = $course->courseId;
        $o->catalogNumber = $course->catalogNumber;
        $o->title = $course->title;
        $o->description = !empty($course->description)? $course->description : '';
        $o->crossListed = $course->crossListed;
        $o->catalogPrintFlag = $course->catalogPrintFlag;

        $o->subjects = array();
        foreach ($subjects as $subject) {
            $o->subjects[] = $this->createSubject($subject);
        }
        return $o;
    }

    protected function createClass($class) {
        $o = new wisc_timetable_class();
        $o->subject = $this->createSubject($class->subject);
        $o->catalogNumber = $class->catalogNumber;
        $o->sectionNumber = $class->sectionNumber;
        $o->termCode = $class->classUniqueId->termCode;
        $o->classNumber = $class->classUniqueId->classNumber;
        $o->type = $class->type;
        $o->sessionCode = $class->sessionCode;
        $o->courseId = $class->courseId;
        $o->topic = isset($class->topic)? $class->topic->shortDescription : '';
        // strtotime() seems to handle ISO 8601 dates
        $o->startDate = strtotime($class->startDate);
        $o->endDate = strtotime($class->endDate);
        $o->published = $class->published;
        $o->hasMeetsWith = $this->classHasMeetsWith($class);
        return $o;
    }

    protected function classHasMeetsWith($class) {
        if (empty($class->crossListing->crossListedType)) {
            return false;
        }
        // See https://wams.doit.wisc.edu/chub/curricular-data-model-1.5/apidocs/edu/wisc/services/ebo/curricular/v1_5/CrossListedType.html
        switch ((string)$class->crossListing->crossListedType) {
            case "CrossListedAndMeetsWith":
            case "MeetsWith":
            case "SectionLevelMeetsWith":
                return true;
        }
        return false;
    }

    protected function createClassUniqueId($classUniqieId) {
        $o = new wisc_timetable_class_uniqueid();
        $o->termCode = $classUniqieId->termCode;
        $o->classNumber = $classUniqieId->classNumber;
        return $o;
    }

    // person can be either a CHUB 'student' or an 'instructor'
    protected function createPerson($person) {
        global $CFG;
        $o = new wisc_timetable_person();
        $personAttr = $person->personAttributes;

        if (isset($personAttr->name->preferredFirst)) {
            $o->firstName = $personAttr->name->preferredFirst;
        } else if (isset($personAttr->name->first)) {
            $o->firstName = $personAttr->name->first;
        } else {
            $o->firstName = '';
        }

        if (isset($personAttr->name->preferredMiddle)) {
            $o->middleName = $personAttr->name->preferredMiddle;
        } else if (isset($personAttr->name->middle)) {
            $o->middleName = $personAttr->name->middle;
        } else {
            $o->middleName = '';
        }

        $o->lastName  = $personAttr->name->last;
        $o->pvi = $personAttr->pvi;
        $o->emplid = $personAttr->emplid;
        $o->netid = isset($personAttr->netid)? $personAttr->netid : '';
        $o->email = isset($personAttr->email)? $personAttr->email : '';

        if (isset($person->ferpaAttributes)) {
            $ferpaAttr = $person->ferpaAttributes;
            $o->ferpaName = $ferpaAttr->name;
            $o->ferpaEmail = $ferpaAttr->email;
        }

        // Check for development mode
        if ($this->devmode) {
            $o->email = $CFG->noreplyaddress;
        }

        return $o;
    }
    protected function createStudent($student) {
        $o = new wisc_timetable_student();
        $o->person = $this->createPerson($student);
        return $o;
    }
    protected function createInstructor($affiliation) {
        $o = new wisc_timetable_instructor();
        $o->person = $this->createPerson($affiliation->instructor);
        $o->roleCode = $affiliation->roleCode;
        $o->roleDescription = $affiliation->roleDescription;
        $o->displayInstructor = $affiliation->displayInstructor;
        return $o;
    }
    protected function createInstructorAffiliationClass($affiliation) {
        $o = new wisc_timetable_instructor_affiliation_class();
        $o->instructor = $this->createInstructor($affiliation);
        $o->class = $this->createClass($affiliation->class);
        return $o;
    }
    protected function createInstructorAffiliation($affiliation) {
        $o = new wisc_timetable_instructor_affiliation();
        $o->instructor = $this->createInstructor($affiliation);
        $o->classUniqieId = $this->createClassUniqueId($affiliation->classUniqueId);
        return $o;
    }
    protected function createRoster($classRoster) {
        $o = new wisc_timetable_roster();
        $o->instructors = array();
        if (!empty($classRoster->instructorAffiliation)) {
            foreach ($classRoster->instructorAffiliation as $affiliation) {
                $instructor = $this->createInstructor($affiliation);
                $o->instructors[] = $instructor;
            }
        }
        $o->students = array();
        if (!empty($classRoster->student)) {
            // CHUB 1.0
            foreach ($classRoster->student as $affiliation) {
                $student = $this->createStudent($affiliation);
                $o->students[] = $student;
            }
        } else if (!empty($classRoster->studentClassMembership)) {
            // CHUB 1.1
            foreach ($classRoster->studentClassMembership as $affiliation) {
                if (!empty($affiliation->dropTimestamp)) {
                    continue;  // TODO: save dropped students to avoid a call to getStudentsThatHaveDropped() later
                }
                $student = $this->createStudent($affiliation->student);
                if (isset($affiliation->isAuditing)) {
                    $student->isAuditing = $affiliation->isAuditing;
                }
                $o->students[] = $student;
            }
        }
        return $o;
    }
}


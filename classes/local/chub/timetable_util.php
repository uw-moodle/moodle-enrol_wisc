<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * CHUB utility functions.
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace enrol_wisc\local\chub;

defined('MOODLE_INTERNAL') || die;

/**
 * CHUB utility functions.
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 abstract class timetable_util {

    /**
     * Return a string representing the term (e.g. "Fall 2010")
     * This function doesn't make any remote calls.
     *
     * @param string $termCode
     * @return string $termName
     */
    public static function get_term_name($termCode) {
        $termCode = (string)$termCode;

        $c = substr($termCode,0,1);
        $yy = substr($termCode,1,2);
        $year = 1900+100*$c+$yy;
        $semester = substr($termCode,3,1);
        switch($semester) {
            case 2:
                $name = sprintf("Fall %d", $year-1);
                break;
            case 3:
                $name = sprintf("Winter %d", $year);
                break;
            case 4:
                $name = sprintf("Spring %d", $year);
                break;
            case 6:
                $name = sprintf("Summer %d", $year);
                break;
            default:
                $name = "Unknown";
        }
        return $name;
    }

    /**
     * Return the termcode of the current term.
     *
     * This function does not make any remote calls.
     *
     *  Timetable DB encodes term as 4 digit code in form 'CYYT'
     *  'C' is  '0' for the 20'th century and '1' for the 21'st.
     *  'YY' is the last two digits of the year.
     *  'T' is '2' for Fall, '4' for Spring, and '6' for Summer
     *
     * @return string
     */
    public static function get_current_termcode() {
        $now = getdate();
        if( $now['year'] >= 2000 ) {
            $century = 1;
        } else {
            $century = 0;
        }
        if( $now['mon'] >= 9 ) {
            $year = ($now['year'] + 1) % 100;
        } else {
            $year = $now['year'] % 100;
        }
        if( $year < 10 ) {
            $year = "0" . $year;
        }
        if( $now['mon'] < 6 ) {
            $term = 4;	// Jan - May = Spring
        } else if( $now['mon'] < 9 ) {
            $term = 6;	// Jun - Aug = Summer
        } else {
            $term = 2;	// Sep - Dec = Fall
        }
        return $century . $year . $term;
    }
}
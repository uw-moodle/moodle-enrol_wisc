<?php
/**
 * Cleanup tasks.
 *
 * @copyright 2015 University of Wisconsin
 * @author Matt Petro
 */

namespace enrol_wisc\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/controller.class.php');
require_once($CFG->dirroot.'/enrol/wisc/controller_helper.class.php');
require_once($CFG->dirroot.'/backup/util/includes/backup_includes.php');

if (file_exists($CFG->dirroot.'/local/archive/locallib.php')) {
    require_once($CFG->dirroot.'/local/archive/locallib.php');
}

class cleanup extends \core\task\scheduled_task {

    /* Seconds to wait before notifying instructors when doing a custom import. */
    const NOTIFY_WAIT = 1800;

    public function get_name() {
        // Shown in admin screens
        return get_string('cleanuptask', 'enrol_wisc');
    }

    /**
     * Executes the task.
     *
     */
    public function execute() {
        global $DB;

        $now = time();


        $lastruntime = get_config('enrol_wisc', 'cleanup_last_run');
        if (!$lastruntime) {
            $lastruntime = 0;
        }

        // Send pending notifications after grace period.  This is used for interactive
        // imports where we don't have control when the import completes.
        // We notify teachers after a fixed time period.
        $active = $DB->get_records_select('enrol_wisc_controllers', 'status = ? AND timemodified < ?',
                array(\enrol_wisc_controller::STATUS_NOTIFYWAIT, $now - self::NOTIFY_WAIT));
        foreach ($active as $controller) {
            $cc = \enrol_wisc_controller::load_controller($controller->controllerid);
            mtrace('Sending delayed notification for controller: '.$cc->get_controllerid());
            $cc->set_status(\enrol_wisc_controller::STATUS_FINISHED_OK);
            $cc->save_controller();
            \enrol_wisc_controller_helper::post_custom_import_tasks($cc);
            \enrol_wisc_controller_helper::notify_create_done($cc, false);
        }

        // Scan actively running course creations.
        $active = $DB->get_records_select('enrol_wisc_controllers', "status = ? OR status = ?", array(\enrol_wisc_controller::STATUS_EXECUTING, \enrol_wisc_controller::STATUS_LAUNCHED));
        foreach ($active as $controller) {
            $failed = false;
            if (!$controller->pid && ($now - $controller->timemodified) > 60*60) {
                // Fail non-background jobs after one hour.
                $failed = true;
            }
            if (!$controller->pid && $controller->status == \enrol_wisc_controller::STATUS_LAUNCHED && ($now - $controller->timemodified) > 60) {
                // Fail launched jobs without a pid after one minute.
                $failed = true;
            }
            if ($controller->pid && !posix_kill($controller->pid, 0) && ($now - $controller->timemodified) > 60) {
                // Fail background jobs after one minute if process isn't running.
                $failed = true;
            }

            if ($failed) {
                $cc = \enrol_wisc_controller::load_controller($controller->controllerid);
                mtrace('No process for course creation controller: '.$cc->get_controllerid());

                $errors = "Execution failed for unknown reason.  No such process.";
                if ($controller->errors) {
                    $errors = $controller->errors."\n".$errors;
                }
                $cc->set_errors($errors);
                $cc->set_status(\enrol_wisc_controller::STATUS_FINISHED_ERR);
                $cc->save_controller();
            }
        }

        // Notify admins of any failures since last run.
        $errored = $DB->get_records_select('enrol_wisc_controllers',
                'status = ? AND timemodified >= ?', array(\enrol_wisc_controller::STATUS_FINISHED_ERR, $lastruntime));
        foreach ($errored as $controller) {
            $cc = \enrol_wisc_controller::load_controller($controller->controllerid);
            $site = get_site();
            $subject = "Course creation failed on ". format_string($site->fullname);

            $body = "Controller status\n\n" . (string)$cc;

            // For each destination, send mail.
            mtrace('Emailing admins about course creation failure: '.$cc->get_controllerid());
            email_to_user(\core_user::get_support_user(), \core_user::get_support_user(), $subject, $body);
        }


        // Delete old enrol_wisc_controllers
        $loglifetime = 3600*24*7;
        $params = array( 'threshold'        => time()-$loglifetime,
                'status_executing' => \enrol_wisc_controller::STATUS_EXECUTING,
                'status_error'     => \enrol_wisc_controller::STATUS_FINISHED_ERR);
        $DB->execute("DELETE FROM {enrol_wisc_controllers}
                WHERE timecreated < :threshold
                AND status != :status_executing
                AND status != :status_error", $params);

        set_config('cleanup_last_run', $now, 'enrol_wisc');
        return true;
    }
}
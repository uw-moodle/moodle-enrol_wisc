<?php
/**
 * Send approval emails.
 *
 * @copyright 2015 University of Wisconsin
 * @author Matt Petro
 */

namespace enrol_wisc\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/controller_helper.class.php');

class send_approval_emails extends \core\task\scheduled_task {

    public function get_name() {
        // Shown in admin screens
        return get_string('sendapprovalemailstask', 'enrol_wisc');
    }

    public function execute() {
        \enrol_wisc_controller_helper::send_approval_emails();
        return true;
    }
}
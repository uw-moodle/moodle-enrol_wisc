<?php
/**
 * Create course task.
 *
 * @copyright 2015 University of Wisconsin
 * @author Matt Petro
 */

namespace enrol_wisc\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/controller.class.php');

class create_course extends \core\task\adhoc_task {

    /**
     * Executes the course create.
     *
     * This forks the course creation into a separate process.  It may be possible to run the course creation
     * inside cron, but the course creation can take a very long time (20 mins) and runs as the user who
     * requested the course.  It is simpler and safer to just fork the creation into an independent process.
     *
     */
    public function execute() {
        global $CFG, $DB;

        $data = $this->get_custom_data();

        if (!$data->controllerid) {
            error_log('No controllerid passed to enrol_wisc_coursecreation_task');
            return true;
        }

        // See how many course creations are executing.
        $count = $DB->count_records('enrol_wisc_controllers', array('status' => \enrol_wisc_controller::STATUS_EXECUTING));
        $maxconcurrency = get_config('enrol_wisc', 'maxconcurrency');
        if ($maxconcurrency > 0 && $count >= $maxconcurrency) {
            // Throw a soft error.  The task API will retry later.
            throw new \moodle_exception('Too many course creation jobs running.  Will retry later.');
        }

        // Prevent simultaneous creations by same user.   We've frequently had managers create
        // multiple version of the same course at the same time.  The archive restore jobs tend to synchronize in this case, and things
        // often fail.  It's better to prevent this by limiting concurrent creations by the same user.
        $count = $DB->count_records('enrol_wisc_controllers', array('userid' => $data->userid, 'status' => \enrol_wisc_controller::STATUS_EXECUTING));
        if ($count >= 1) {
            // Throw a soft error.  The task API will retry later.
            throw new \moodle_exception('Concurrent course creation jobs running for same user.  Will retry later.');
        }

        $DB->set_field('enrol_wisc_controllers', 'status', \enrol_wisc_controller::STATUS_LAUNCHED, array('controllerid' => $data->controllerid));

        $phppath = get_config('enrol_wisc', 'phppath');
        $script = escapeshellarg($CFG->dirroot."/enrol/wisc/cli/createcourse.php");
        $args = "-c ".escapeshellarg($data->controllerid);

        // The following will work on the linux servers, at least.
        if (file_exists('/usr/bin/logger')) {
            $logger = ' 2>&1 | /usr/bin/logger -p local3.info -t create_course -i';
        } else {
            $logger = '';
        }

        $debug = '';
        // For ZendStudio debugging
        //$debug = 'QUERY_STRING="start_debug=1&use_remote=1&send_sess_end=1&debug_start_session=1&debug_port=10137&debug_host=127.0.0.1&debug_stop=1"';
        exec("$debug $phppath $script $args $logger >/dev/null 2>&1 &");

        return true;
    }
}
<?php

/**
 * Page to build department course categories
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/enrol/wisc/lib.php');
require_once($CFG->dirroot.'/enrol/wisc/updatelib.php');
require_once($CFG->dirroot.'/enrol/wisc/edit_forms.php');

require_login();
$PAGE->set_course($SITE);

require_capability('moodle/site:config', context_system::instance());

$PAGE->set_pagelayout('admin');
$PAGE->set_heading('Create Categories');
$PAGE->set_title('Create Categories');
$PAGE->set_url('/enrol/wisc/buildcategories.php');


$mform = new enrol_wisc_buildcategories_form();

if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot);

} else if ($data = $mform->get_data()) { // no magic quotes

    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('buildcategorydone', 'enrol_wisc'));

    $terms = array();
    foreach ((array)$data as $fieldname=>$value) {
        if (strpos($fieldname, 'term_') !== 0) {
            continue;
        }
        list($ignored, $term) = explode('_', $fieldname);
        $terms[] = $term;
    }

    wisc_get_temp_category();
    wisc_create_categories($terms, !empty($data->update), !empty($data->sort));

    echo $OUTPUT->continue_button('/');
    echo $OUTPUT->footer();
    exit;
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('buildcategory', 'enrol_wisc'));

echo $OUTPUT->box(get_string('categoryinfo', 'enrol_wisc'));

$mform->display();
echo $OUTPUT->footer();

<?php
/**
 * Datastore for the roster associations page.
 *
 * This class provides a custom interface to the wisc_timetable_datastore class.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_wisc\local\chub\schema\wisc_timetable_course;
use enrol_wisc\local\chub\schema\wisc_timetable_class;
use enrol_wisc\local\chub\schema\wisc_timetable_person;


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/accesslib.php');

class wisc_ra_datastore {
    protected $datastore;

    public static function get() {
        static $instance = null; // global instance of this class
        if ($instance === null) {
            $instance = new wisc_ra_datastore();
        }
        return $instance;
    }

    /**
     * Load the real datastore on demand
     */
    protected function get_datastore() {
        global $CFG;
        require_once($CFG->dirroot.'/enrol/wisc/lib/datastore.php');
        if (!isset($this->datastore)) {
            $this->datastore = new \enrol_wisc\local\chub\chub_datasource();
        }
        return $this->datastore;
    }

    public function getCourseByCourseId($termCode, $subjectCode, $courseId) {
        $datastore = $this->get_datastore();
        $ttcourse = $datastore->getCourseByCourseId($termCode, $subjectCode, $courseId, true);
        return $this->parseCourse($ttcourse);
    }

    public function getClassByClassNumber($termCode, $classNumber) {
        $datastore = $this->get_datastore();
        $ttclass = $datastore->getClassByClassNumber($termCode, $classNumber);
        $ttinstructors = $datastore->getInstructors($termCode, $classNumber);
        return $this->parseClass($ttclass, $ttinstructors);

    }

    public function getClassesByCatalogNumber($termCode, $subjectCode, $catalogNumber) {
        $datastore = $this->get_datastore();
        $ttclasses = $datastore->getClassesByCatalogNumber($termCode, $subjectCode, $catalogNumber);
        $ttaffiliations = $datastore->getInstructorAffiliationsForCourse($termCode, $subjectCode, $catalogNumber);
        $ttaffiliationsbyclass = array();
        foreach ($ttaffiliations as $ttaffiliation) {
            $ttaffiliationsbyclass[$ttaffiliation->classUniqieId->classNumber][] = $ttaffiliation->instructor;
        }
        $classes = array();
        foreach ($ttclasses as $ttclass) {
            if (isset($ttaffiliationsbyclass[$ttclass->classNumber])) {
                $ttinstructors = $ttaffiliationsbyclass[$ttclass->classNumber];
            } else {
                $ttinstructors = array();
            }
            $classes[] = $this->parseClass($ttclass, $ttinstructors);
        }
        return $classes;
    }

    public function getCrossListedClasses($termCode, $classNumber) {
        $datastore = $this->get_datastore();
        $ttclasses = $datastore->getCrossListedClasses($termCode, $classNumber);
        $classes = array();
        foreach ($ttclasses as $ttclass) {
            $classes[] = $this->parseClass($ttclass);
        }
        return $classes;
    }

    public function getClassesByInstructor($termCode, $pvi) {
        $datastore = $this->get_datastore();
        $ttaffils = $datastore->getClassesByInstructor($termCode, $pvi);
        $subjects = $this->getSubjectsInTermUser($termCode);
        $subjectcodes = array();
        foreach ($subjects as $subject) {
            $subjectcodes[$subject->subjectCode] = true;
        }
        // A course is valid if any crosslist has an allowed subject
        $allowedcourseids = array();
        foreach ($ttaffils as $ttaffil) {
            if (!empty($subjectcodes[$ttaffil->class->subject->subjectCode])) {
                $allowedcourseids[$ttaffil->class->courseId] = true;
            }
        }
        $classes = array();
        foreach ($ttaffils as $ttaffil) {
            if (!empty($allowedcourseids[$ttaffil->class->courseId])) {
                $classes[] = $this->parseClass($ttaffil->class, array($ttaffil->instructor));
            }
        }
        return $classes;
    }

    public function getSchoolCollegesMenu($termCode) {
        // We gather the list of schools/colleges from the list from all subjects, so that
        // we filter by CHUB access rights
        $subjects = $this->getSubjectsInTermUser($termCode);
        foreach ($subjects as $subject) {
            $org = $subject->schoolCollege;
            $menu[$org->academicOrgCode] = $org->shortDescription;
        }
        return $menu;
    }

    public function getSubjectsMenu($termCode, $orgCode) {
        $subjects = $this->getSubjectsInTermUser($termCode);
        foreach ($subjects as $subject) {
            if ($subject->schoolCollege->academicOrgCode === $orgCode) {
                $menu[$subject->subjectCode] = $subject->description;
            }
        }

        return $menu;
    }

    /**
     * Get subjects in term that $USER is allowed to see.  This is determined by the WISC cohort configuration
     *
     * @param unknown_type $termCode
     */
    public function getSubjectsInTermUser($termCode) {
        global $USER;
        global $DB;

        $datastore = $this->get_datastore();
        $allsubjects = $datastore->getSubjectsInTerm($termCode);

        if (has_capability('enrol/wisc:anysubject', context_system::instance())) {
            // no restrictions
            return $allsubjects;
        }

        $createbyuwroles = has_capability('enrol/wisc:createcoursebyuwrole', context_system::instance()) && (wisc_uwroles_to_capability($USER) != enrol_wisc_plugin::CREATECOURSE_NONE);
        if ($createbyuwroles) {
            $uwrolescond = "a.cohortid IS NULL OR ";
        } else {
            $uwrolescond = ''; // This effectively changes the LEFT JOINs to INNER JOINS
        }

        // check subjects against cohort access table
        $sql = "SELECT a.id, a.item, a.type, a.cohortid
                  FROM {enrol_wisc_access} a
             LEFT JOIN {cohort} c ON c.id = a.cohortid AND c.contextid = :sitecontext
             LEFT JOIN {cohort_members} cm ON cm.cohortid = c.id
                 WHERE a.permission > 0
                   AND ($uwrolescond cm.userid = :userid)";
        $params = array();
        $params['sitecontext'] = context_system::instance()->id;
        $params['userid'] = $USER->id;
        $allows = $DB->get_records_sql($sql, $params);

        $allowedorgs = array();
        $allowedsubjects = array();
        foreach ($allows as $allow) {
            if ($allow->type == enrol_wisc_plugin::ACCESS_ORG) {
                $allowedorgs[$allow->item] = true;
            } else if ($allow->type == enrol_wisc_plugin::ACCESS_SUBJECT) {
                $allowedsubjects[$allow->item] = true;
            }
        }

        $subjects = array();
        foreach ($allsubjects as $subject) {
            if (!empty($allowedorgs[$subject->schoolCollege->academicOrgCode])) {
                $subjects[] = $subject;
            } else if (!empty($allowedsubjects[$subject->subjectCode])) {
                $subjects[] = $subject;
            }
        }
        return $subjects;
    }

    /**
     * Get all subjects in all active terms.  This is not implemented efficiently in CHUB, so we cache the result in the SESSION.
     * Do not use this function except on admin configuration pages.
     *
     * returns an array of arrays:  [ array $subjects indexed by subjectCode, array $orgs indexed by academicOrgCode ]
     * each subject is a stdClass containing 'subjectCode', 'description', 'shortDescription', 'academicOrgCode'
     * each org is a stdClass containing 'academicOrgCode', 'shortDescription'
     */
    public function getAllSubjectsAndOrgs() {
        global $SESSION;
        if (!empty($SESSION->enrol_wisc_subjectorgs)) {
            return $SESSION->enrol_wisc_subjectorgs;
        }
        $datastore = $this->get_datastore();
        $terms = $datastore->getAvailableTerms();
        $subjects = array();
        $orgs = array();
        foreach ($terms as $term) {
            $termsubjects = $datastore->getSubjectsInTerm($term->termCode);
            foreach ($termsubjects as $subject) {
                $subjectobject = new stdClass();
                $subjectobject->subjectCode = $subject->subjectCode;
                $subjectobject->description = $subject->description;
                $subjectobject->shortDescription = $subject->shortDescription;
                $subjectobject->academicOrgCode = $subject->schoolCollege->academicOrgCode;
                $subjects[$subject->subjectCode] = $subjectobject;
                $academicOrgCode = $subject->schoolCollege->academicOrgCode;
                if (empty($orgs[$academicOrgCode])) {
                    $orgobject = new stdClass();
                    $orgobject->academicOrgCode = $academicOrgCode;
                    $orgobject->shortDescription = $subject->schoolCollege->shortDescription;
                    $orgs[$academicOrgCode] = $orgobject;
                }
            }
            unset ($termsubjects);
        }
        $SESSION->enrol_wisc_subjectorgs = array ($subjects, $orgs);
        return $SESSION->enrol_wisc_subjectorgs;
    }

    public function getSubjectCoursesMenu($termCode, $subjectCode) {
        $datastore = $this->get_datastore();
        $courses = $datastore->getCoursesBySubject($termCode, $subjectCode);
        $menu = array();
        if (!empty($courses)) {
            $acourse = reset($courses);
            $subjectname = preg_replace('/ /', '', reset($acourse->subjects)->shortDescription);
            foreach ($courses as $course) {
                $subject = reset($course->subjects);
                $menu[$course->courseId] = $subjectname . ' ' . $course->catalogNumber . ' ' . $course->title;
            }
        }
        return $menu;
    }

    /**
     * Get non-chub top-level orgs, manually configured in moodle.
     *
     * The configuration is of the form  code:name\ncode2:name1\n...
     *
     * @return array of stdClass with keys academicOrgCode and shortDescription
     */
    public function getOtherOrgs() {
        $otherorgs = get_config('enrol_wisc', 'otherorgs');
        $lines = explode("\n", $otherorgs);
        if (!$lines) {
            return array();
        }
        $result = array();
        foreach ($lines as $line) {
            list($code,$name) = array_pad(explode(":", $line), 2, null);
            if ($code && $name) {
                $neworg = new stdClass();
                $neworg->academicOrgCode = trim($code);
                $neworg->shortDescription = trim($name);
                $result[] = $neworg;
            }
        }
        return $result;
    }

    public function getInstructors($termCode, $classNumber) {
        $datastore = $this->get_datastore();
        $people = $datastore->getInstructors($termCode, $classNumber);
        return $people;
    }

    public function getClassFromCoursemap($map) {
        try {
            $class = $this->getClassByClassNumber($map->term, $map->class_number);
        } catch (Exception $e) {
            // do the best we can with the stored info.

            $class = new stdClass();
            $class->classNumber = $map->class_number;
            $class->catalogNumber = $map->catalog_number;
            $class->sectionNumber = $map->section_number;
            $class->sessionCode = $map->session_code;
            $class->subjectCode = $map->subject_code;
            $class->subjectShortDescription = $map->subject;
            $class->termCode = $map->term;
            $class->type = $map->type;
            $class->courseId = $map->isis_course_id;
            $class->instructor = get_string('classnotfound', 'enrol_wisc');
            $class->topic = '';
            // fixes for older coursemaps
            if (empty($class->subjectShortDescription)) {
                $class->subjectShortDescription = $map->subject_code;
            }
            if (empty($class->courseId)) {
                $class->courseId = $map->class_number;
            }
            if (empty($class->type)) {
                $class->type = 'SEC';
            }
        }
        $class->official = ($map->approved == enrol_wisc_plugin::ASSOCIATION_APPROVED);

        return $class;
    }

    protected function parseCourse(wisc_timetable_course $ttcourse) {
        $course = new stdClass();
        $course->courseId = $ttcourse->courseId;
        $course->termCode = $ttcourse->termCode;
        $course->catalogNumber = $ttcourse->catalogNumber;
        $course->title = $ttcourse->title;
        $course->description = $ttcourse->description;
        $subjectstrs = array();
        foreach ($ttcourse->subjects as $subject) {
            $course->subjects[] = $subject->subjectCode;
            $subjectstrs[] = $subject->shortDescription;
        }
        sort($subjectstrs);
        $course->subjectstr = join('/',$subjectstrs);
        $course->subjectstr = preg_replace('/ /', '', $course->subjectstr);
        return $course;
    }

    protected function parseClass(wisc_timetable_class $ttclass, array $instructors = array()) {
        $class = new stdClass();
        $class->termCode = $ttclass->termCode;
        $class->classNumber = $ttclass->classNumber;
        $class->courseId = $ttclass->courseId;
        $class->catalogNumber = $ttclass->catalogNumber;
        $class->sectionNumber = $ttclass->sectionNumber;
        $class->sessionCode = $ttclass->sessionCode;
        $class->subjectCode = $ttclass->subject->subjectCode;
        $class->subjectShortDescription = $ttclass->subject->shortDescription;
        $class->subjectDescription = $ttclass->subject->description;
        $class->academicOrgCode = $ttclass->subject->schoolCollege->academicOrgCode;
        $class->academicOrgShortDescription = $ttclass->subject->schoolCollege->shortDescription;
        $class->type = $ttclass->type;
        $class->topic = $ttclass->topic;
        $class->startDate = $ttclass->startDate;
        $class->endDate = $ttclass->endDate;
        $class->hasMeetsWith = $ttclass->hasMeetsWith;
        $instructornames = array();
        foreach ($instructors as $instructor) {
            $instructornames[] = $this->fullname($instructor->person);
        }
        $class->instructor = implode(', ', $instructornames);
        return $class;
    }

    protected function fullname(wisc_timetable_person $person) {
        return substr($person->firstName,0, 1).'. '.$person->lastName;
    }

    static public function helper_diff_classes($classes, $toremove) {
        // remove $toremove classes from $classes array
        $excludes = array();
        foreach ($toremove as $class) {
            $excludes[$class->classNumber] = true;
        }
        $out = array();
        foreach ($classes as $class) {
            if (!isset($excludes[$class->classNumber])) {
                $out[] = $class;
            }
        }
        return $out;
    }

    static public function helper_get_approved_classes($classes, $dbclasses) {
        // find which classes are marked official in $classes but not $dbclasses
        $approved = array();
        foreach ($dbclasses as $class) {
            $approved[$class->classNumber] = $class->official;
        }
        $out = array();
        foreach ($classes as $class) {
            if (!empty($class->official) && isset($approved[$class->classNumber]) && !$approved[$class->classNumber]) {
                $out[] = $class;
            }
        }
        return $out;
    }
}
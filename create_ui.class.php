<?php

/**
 * Course creator UI class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/create_ui_helper.class.php');
require_once($CFG->dirroot.'/enrol/wisc/controller.class.php');
require_once($CFG->dirroot.'/enrol/wisc/create_ui_stage.class.php');

if (file_exists($CFG->dirroot.'/local/archive/ui_component.php')) {
    require_once($CFG->dirroot.'/local/archive/ui_component.php');
}

/**
 * This is the create course/instance user interface class
 */
class enrol_wisc_create_ui {

    const STAGE_TERM = 1;
    const STAGE_MAPPING = 2;
    const STAGE_SETTINGS = 3;
    const STAGE_IMPORT = 4;
    const STAGE_REVIEW = 5;
    const STAGE_COMPLETE = 6;

    const PROGRESS_INTIAL = 0;
    const PROGRESS_PROCESSED = 1;
    const PROGRESS_SAVED = 2;
    const PROGRESS_EXECUTED = 3;

    protected $controller;
    protected $stage;
    protected $stagenum;
    protected $progress;
    protected $params;

    protected $stages;

    protected $backgroundcreate;
    protected $customimport;

    /**
     * @param enrol_wisc_controller $controller
     */
    public function __construct(enrol_wisc_controller $controller, array $params=null) {
        $this->controller = $controller;
        $this->params = $params;
        $this->progress = self::PROGRESS_INTIAL;
        $controllermode = $this->controller->get_controllermode();
        if ($controllermode == enrol_wisc_controller::TYPE_COURSE) {
            $this->stages = array(self::STAGE_TERM, self::STAGE_MAPPING, self::STAGE_SETTINGS, self::STAGE_IMPORT, self::STAGE_REVIEW, self::STAGE_COMPLETE);
        } else if ($controllermode == enrol_wisc_controller::TYPE_INSTANCE) {
            $this->stages = array(self::STAGE_TERM, self::STAGE_MAPPING, self::STAGE_REVIEW, self::STAGE_COMPLETE);
        } else if ($controllermode == enrol_wisc_controller::TYPE_EDIT){
            $this->stages = array(self::STAGE_MAPPING, self::STAGE_REVIEW, self::STAGE_COMPLETE);
        } else {
            throw new moodle_exception('invalidcontrollermode', 'enrol_wisc');
        }
        $this->initialise_stage(null);
    }

    public function get_createid() {
        return $this->controller->get_controllerid();
    }

    public function get_controller() {
        return $this->controller;
    }

    public function initialise_stage($stagenum) {
        if (is_null($stagenum)) {
            $stagenum = optional_param('stage', 0, PARAM_INT);
        }
        $params = $this->params;
        $params['stage'] = $stagenum;
        switch ($this->stages[$stagenum]) {
            case self::STAGE_TERM:
                $stage = new enrol_wisc_stage_term($this, $params);
                break;
            case self::STAGE_MAPPING:
                $stage = new enrol_wisc_stage_mapping($this, $params);
                break;
            case self::STAGE_IMPORT:
                $stage = new enrol_wisc_stage_import($this, $params);
                break;
            case self::STAGE_SETTINGS:
                $stage = new enrol_wisc_stage_course_settings($this, $params);
                break;
            case self::STAGE_REVIEW:
                if ($this->controller->get_controllermode() == enrol_wisc_controller::TYPE_COURSE) {
                    $stage = new enrol_wisc_stage_review_course($this, $params);
                } else {
                    $stage = new enrol_wisc_stage_review_instance($this, $params);
                }
                break;
            case self::STAGE_COMPLETE:
                $stage = new enrol_wisc_stage_complete($this, $params);
                break;
            default:
                throw new moodle_exception('unknownstage', 'enrol_wisc');
                break;
        }
        $this->stagenum = $stagenum;
        $this->stage = $stage;
    }

    /**
     * Loads the create controller if we are tracking one
     * @return enrol_wisc_controller|false
     */
    final public static function load_controller($createid=false) {
        // Get the create id optional param
        if ($createid) {
            try {
                // Try to load the controller with it.
                // If it fails at this point it is likely because this is the first load
                $controller = enrol_wisc_controller::load_controller($createid);
                return $controller;
            } catch (Exception $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * This processes the current stage
     * @return bool
     */
    public function process() {
        if ($this->progress >= self::PROGRESS_PROCESSED) {
            throw new moodle_exception('createuialreadyprocessed', 'enrol_wisc');
        }

        $previous = optional_param('previous', false, PARAM_BOOL);

        // Process the stage
        if (!$previous) {
            //next
            $processoutcome = $this->stage->process();
            while ($processoutcome !== false){
                $this->initialise_stage($this->get_next_stage(), $this->params);
                $processoutcome = $this->stage->process();
            }
        } else {
            // previous
            $processoutcome = true;
            while ($processoutcome !== false && $this->stagenum > 0){
                $this->initialise_stage($this->get_prev_stage(), $this->params);
                $processoutcome = $this->stage->process();
            }

        }

        // Process UI event after to check changes are valid
        $this->controller->process_ui_event();
        return $processoutcome;
    }

    /**
     * Saves the create controller.
     *
     * Once this has been called nothing else can be changed in the controller.
     *
     * @return bool
     */
    public function save_controller() {
        if ($this->progress >= self::PROGRESS_SAVED) {
            throw new moodle_exception('createuialreadysaved', 'enrol_wisc');
        }
        $this->progress = self::PROGRESS_SAVED;
        // Process UI event after to check any changes are valid
        $this->controller->process_ui_event();
        // Save the controller
        $this->controller->save_controller();
        return true;
    }

    /**
     * This displays the current stage
     * @return bool
     */
    public function display() {
        if ($this->progress < self::PROGRESS_SAVED) {
            throw new moodle_exception('createuisavebeforedisplay', 'enrol_wisc');
        }
        return $this->stage->display();
    }

    public function initialize_js($page) {
        $this->stage->initialize_js($page);
    }

    public function set_creation_options($backgroundcreate, $customimport) {
        if ($backgroundcreate && !$customimport) {
            $this->backgroundcreate = true;
        } else {
            $this->backgroundcreate = false;
        }
        $this->customimport = $customimport;
    }

    /**
     * Executes the course create
     * @return bool
     */
    public function execute() {
        global $CFG, $OUTPUT;

        // Close all buffers if possible.  This is so the progress bar works reliably with
        // php.ini output_buffering enabled.
        while(ob_get_level()) {
            if (!ob_end_flush()) {
                // prevent infinite loop when buffer can not be closed
                break;
            }
        }


        $laststage = end($this->stages);
        if ($this->progress >= self::PROGRESS_EXECUTED) {
            throw new moodle_exception('createuialreadyexecuted','enrol_wisc');
        }
        if ($this->get_stage() < $laststage) {
            throw new moodle_exception('finalisedbeforeexecute','enrol_wisc');
        }

        error_log('enrol_wisc: Created course, controllerid=' .  $this->controller->get_controllerid());

        $this->progress = self::PROGRESS_EXECUTED;
        $this->controller->finish_ui();
        $this->controller->save_controller();

        $offlinecreate = get_config('enrol_wisc', 'offlinecreate');
        $coursemode = $this->controller->get_controllermode() == enrol_wisc_controller::TYPE_COURSE;

        $offlinecreate = $offlinecreate && $coursemode;
        if (!is_null($this->backgroundcreate)) {
            $offlinecreate = $offlinecreate && $this->backgroundcreate;
        }
        if ($offlinecreate) {
            // Create an adhoc task.
            $task = new \enrol_wisc\task\create_course();
            $task->set_custom_data(array(
                    'controllerid' => $this->controller->get_controllerid(),
                    'userid' => $this->controller->get_userid(),
            ));
            // Queue it to run at next cron.
            \core\task\manager::queue_adhoc_task($task);
        } else {
            // Basic/initial prevention against time/memory limits
            set_time_limit(1 * 60 * 60); // 1 hour for 1 course initially granted
            raise_memory_limit(MEMORY_EXTRA);

            // Div used to hide the 'progress' step once the page gets onto 'finished'.
            echo html_writer::start_div('', array('id' => 'executionprogress'));

            // Start the progress display.
            $progress = new \core\progress\display();

            // Prepare logger for import.
            $logger = new core_backup_html_logger($CFG->debugdeveloper ? backup::LOG_DEBUG : backup::LOG_INFO);

            flush();

            $options = array('customimport' => $this->customimport);
            $this->controller->execute($options, $progress, $logger, false);

            // All progress complete. Hide progress area.
            echo html_writer::end_div();
            echo html_writer::script('document.getElementById("executionprogress").style.display = "none";');

            // Get and display log data if there was any.
            $loghtml = $logger->get_html();
            if ($loghtml != '') {
                echo $OUTPUT->box_start('backup_log');
                echo $OUTPUT->heading(get_string('backuplog', 'backup'));
                echo $OUTPUT->box($loghtml, 'backup_log_contents');
                echo $OUTPUT->box_end();
            }

            if ($this->controller->get_status() != enrol_wisc_controller::STATUS_FINISHED_ERR
                    && $this->customimport) {
                // Redirect for custom import.
                $message .= get_string('continuetoimport', 'enrol_wisc');
                $params = array(
                        'id'=>$this->controller->get_courseid(),
                        'importid'=>$this->controller->get_archiveid(),
                        'target'=>backup::TARGET_CURRENT_ADDING);
                $redirecturl = new moodle_url('/local/archive/import.php', $params);
                redirect($redirecturl);

            }


        }
        return true;
    }

    /**
     * Gets an array of progress bar items that can be displayed through the enrol_wisc renderer.
     * @return array Array of items for the progress bar
     */
    public function get_progress_bar() {
        global $PAGE;

        $items = array();
        $currentstage = $this->get_stage();
        $laststage = end($this->stages);
        foreach ($this->stages as $stagenum=>$stage) {
            $classes = array('create_stage');
            if ($stage == $currentstage) {
                $classes[] = 'create_stage_current';
            } else if ($stage < $currentstage) {
                $classes[] = 'create_stage_complete';
            }
            $item = array('text' => ($stagenum+1).'. '.get_string('stage'.$stage, 'enrol_wisc'),'class' => join(' ', $classes));
            if ($stage < $currentstage && $currentstage < $laststage) {
                $params = $this->stage->get_params();
                $params['stage'] = $stagenum;
                $item['link'] = new moodle_url($PAGE->url, $params);
            }
            // Disable the mapping stage in case term=other
            // TODO: move this logic somewhere else
            if ($stage == self::STAGE_MAPPING && $this->controller->get_term() == 0) {
                $item['class'] = 'create_stage';
                unset ($item['link']);
            }
            $items[] = $item;
            $stage = $stage - 1;
        }
        return $items;
    }

    /**
     * Gets the current stage
     * @return int
     */
    public function get_stage() {
        return $this->stages[$this->stagenum];
    }
    public function get_next_stage() {
        return $this->stagenum + 1;
    }
    public function get_prev_stage() {
        return $this->stagenum - 1;
    }
    public function get_stage_name() {
        return get_string('stage'.$this->get_stage(), 'enrol_wisc');
    }
    /**
     * Gets the id of the first stage this UI is reponsible for
     * @return int
     */
    public function get_first_stage_id() {
        return self::STAGE_INITIAL;
    }

    public function cancel_process() {
        global $PAGE;
        // Determine the appropriate URL to redirect the user to
        if ($PAGE->context->contextlevel == CONTEXT_MODULE && $PAGE->cm !== null) {
            $relevanturl = new moodle_url('/mod/'.$PAGE->cm->modname.'/view.php', array('id'=>$PAGE->cm->id));
        } else {
            $relevanturl = new moodle_url('/course/view.php', array('id'=>$PAGE->course->id));
        }
        redirect($relevanturl);
    }

}

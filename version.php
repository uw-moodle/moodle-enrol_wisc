<?php

/**
 * Version information
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2017011101;
$plugin->requires  = 2014051200;   // See http://docs.moodle.org/dev/Moodle_Versions
$plugin->release   = '4.0.2';
$plugin->component = 'enrol_wisc';
$plugin->maturity  = MATURITY_BETA;

$plugin->dependencies = array(
        'local_wiscservices' => 2015031700,
);

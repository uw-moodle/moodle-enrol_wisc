/**
 * Javascript helper functions for WISC enrol
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

M.enrol_wisc = {
    Y : null,

    // Create an associations form
    init : function(Y) {
        this.Y = Y;
        var add = Y.one('#add');
        var addselect = Y.one('#addselect');
        add.set('disabled', this.is_selection_empty(addselect));
        this.selection_on_change(addselect, function() { add.set('disabled', this.is_selection_empty(addselect)); });

        var remove = Y.one('#remove');
        var removeselect = Y.one('#removeselect');
        remove.set('disabled', this.is_selection_empty(removeselect));
        this.selection_on_change(removeselect, function() { remove.set('disabled', this.is_selection_empty(removeselect)); });
    },
    is_selection_empty : function(n) {
        var selection = false;
        n.all('option').each(function(){
            if (this.get('selected')) {
                selection = true;
            }
        });
        return !(selection);
    },
    selection_on_change : function(n, f) {
        n.on('keyup', f, this);
        n.on('click', f, this);
        n.on('change', f, this);
    }
}

<?php

/**
 * Course creator controller class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/ui_components.php');
require_once($CFG->dirroot.'/enrol/wisc/controller_helper.class.php');
require_once($CFG->dirroot.'/backup/util/includes/restore_includes.php');

/**
 * Class implementing the controller of a course create or enrol instance create process
 */
class enrol_wisc_controller {

    // Controller Type
    const TYPE_COURSE    = 0;  // new course
    const TYPE_INSTANCE  = 1;  // new instance inside course
    const TYPE_EDIT      = 2;  // existing instance inside course

    const STATUS_CREATED     = 1;
    const STATUS_CONFIGURED  = 2;
    const STATUS_AWAITING    = 3;
    const STATUS_LAUNCHED    = 4;
    const STATUS_EXECUTING   = 5;
    const STATUS_NOTIFYWAIT  = 6;
    const STATUS_FINISHED_ERR= 99;
    const STATUS_FINISHED_OK = 100;

    protected $controllerid; // Unique identificator

    protected $term;
    protected $classes;
    protected $uistate;
    protected $settings;
    protected $archiveid;

    protected $courseid;   // if existing course
    protected $instanceid; // if existing instance
    protected $userid;

    protected $controllermode; // course or instance
    protected $status;
    protected $notify;
    protected $message;
    protected $messagesent;
    protected $pid;  // for course create process
    protected $errors;  // from create

    public function __construct($userid, $controllermode = self::TYPE_COURSE, $courseid = null, $instanceid = null) {
        global $DB;

        $this->userid = $userid;
        $this->controllermode = $controllermode;
        $this->set_status(self::STATUS_CREATED);
        $this->calculate_controllerid();
        $this->classes = array();

        if ($controllermode == self::TYPE_INSTANCE) {
            if (!$courseid) {
                throw new moodle_exception("controllernocourse", "enrol_wisc");
            }
            $this->set_courseid($courseid);
        } else if ($controllermode == self::TYPE_EDIT) {
            if (!$courseid || !$instanceid) {
                throw new moodle_exception("controllernoinstance", "enrol_wisc");
            }
            $this->set_courseid($courseid);
            $this->set_instanceid($instanceid);

            // Load instance configuration
            $instance = $DB->get_record('enrol', array('id'=>$instanceid, 'courseid'=>$courseid, 'enrol'=>'wisc'), '*', MUST_EXIST);
            $this->set_term($instance->{enrol_wisc_plugin::termfield});

            $settings = new stdClass();
            $settings->tarole = $instance->{enrol_wisc_plugin::tarolefield};
            $settings->auditorrole = $instance->{enrol_wisc_plugin::auditorrolefield};
            if (is_null($settings->auditorrole)) {
                $settings->auditorrole = get_config('enrol_wisc', 'auditorrole');
            }
            $this->set_settings($settings);

            $radatastore = wisc_ra_datastore::get();
            $maps = $DB->get_records('enrol_wisc_coursemap', array('enrolid'=>$instanceid, 'deleted'=>0));
            $classes = array();
            foreach ($maps as $map) {
                $classes[] = $radatastore->getClassFromCoursemap($map);
            }
            $this->set_classes($classes);
        }
    }

    /**
     * @return the $controllerid
     */
    public function get_controllerid() {
        return $this->controllerid;
    }

    /**
     * @return the $controllermode
     */
    public function get_controllermode() {
        return $this->controllermode;
    }

    /**
     * @return the $term
     */
    public function get_term() {
        return $this->term;
    }

    /**
     * @return the $classes
     */
    public function get_classes() {
        return $this->classes;
    }

    /**
     * @return the $uistate
     */
    protected function get_uistate() {
        return $this->uistate;
    }

    /**
     * @return the $settings
     */
    public function get_settings() {
        return $this->settings;
    }

    /**
     * @return the $archiveid
     */
    public function get_archiveid() {
        return $this->archiveid;
    }

    /**
     * @return the $status
     */
    public function get_status() {
        return $this->status;
    }

    /**
     * @return the $notify
     */
    public function get_notify() {
        return $this->notify;
    }

    /**
     * @return the $message
     */
    public function get_message() {
        return $this->message;
    }

    /**
     * @return the $messagesent
     */
    public function get_messagesent() {
        return $this->messagesent;
    }

    /**
     * @return the $userid
     */
    public function get_userid() {
        return $this->userid;
    }

    /**
     * @return the $pid
     */
    public function get_pid() {
        return $this->pid;
    }

    /**
     * @return the $courseid
     */
    public function get_courseid() {
        return $this->courseid;
    }

    /**
     * @return the $instanceid
     */
    public function get_instanceid() {
        return $this->instanceid;
    }


    /**
     * @return the $errors
     */
    public function get_errors() {
        return $this->errors;
    }

    /**
     *  @param field_type $controllerid
     */
    public function set_controllerid($controllerid) {
        $this->controllerid = $controllerid;
    }

    /**
     * @param field_type $term
     */
    public function set_term($term) {
        $this->term = $term;
    }

    /**
     * @param field_type $classes
     */
    public function set_classes($classes) {
        $this->classes = $classes;
    }

    /**
     * @param field_type $uistate
     */
    protected function set_uistate($uistate) {
        if (empty($uistate)) {
            $uistate = new stdClass();
        }
        $this->uistate = $uistate;
    }

    /**
     * @param field_type $settings
     */
    public function set_settings($settings) {
        $this->settings = $settings;
    }

    /**
     * @param field_type $archiveid
     */
    public function set_archiveid($archiveid) {
        $this->archiveid = $archiveid;
    }

    /**
     * @param field_type $status
     */
    public function set_status($status) {
        $this->status = $status;
    }

    /**
     * @param field_type $notify
     */
    public function set_notify($notify) {
        $this->notify = $notify;
    }

    /**
     * @param field_type $message
     */
    public function set_message($message) {
        $this->message = $message;
    }

    /**
     * @param field_type $messagesent
     */
    public function set_messagesent($messagesent) {
        $this->messagesent = $messagesent;
    }

    /**
     * @param field_type $pid
     */
    public function set_pid($pid) {
        $this->pid = $pid;
    }

    /**
     * @param field_type $courseid
     */
    public function set_courseid($courseid) {
        $this->courseid = $courseid;
    }

    /**
     * @param field_type $instanceid
     */
    protected function set_instanceid($instanceid) {
        $this->instanceid = $instanceid;
    }

    /**
     * @param field_type $errors
     */
    public function set_errors($errors) {
        $this->errors = $errors;
    }
    // access classes for uistate

    // mapper state
    public function get_course_mapper_state() {
        $uistate = $this->get_uistate();
        if (!empty($uistate->mapper)) {
            return $uistate->mapper;
        } else {
            return null;
        }
    }

    public function set_course_mapper_state($mstate) {
        $uistate = $this->get_uistate();
        $uistate->mapper = $mstate;
        $this->set_uistate($uistate);
    }

    // archive search state
    public function get_import_state() {
        $uistate = $this->get_uistate();
        if (!empty($uistate->import)) {
            return $uistate->import;
        } else {
            return new stdClass();
        }
    }

    public function set_import_state($astate) {
        $uistate = $this->get_uistate();
        $uistate->import = $astate;
        $this->set_uistate($uistate);
    }

    public function set_configured($configured) {
        $status = $this->get_status();
        if ($status != self::STATUS_CREATED && $status != self::STATUS_CONFIGURED) {
            throw new moodle_exception('statealreadyexecuted', 'enrol_wisc');
        }
        if ($configured) {
            $this->set_status(self::STATUS_CONFIGURED);
        } else {
            $this->set_status(self::STATUS_CREATED);
        }
    }

    public function save_controller() {
        global $DB;
        // Save controller to DB
        $rec = new stdclass();
        $rec->controllerid   = $this->controllerid;
        $rec->controllermode = $this->controllermode;
        $rec->userid         = $this->userid;
        $rec->status         = $this->status;
        $rec->pid            = $this->pid;
        $rec->courseid       = $this->courseid;
        $rec->instanceid     = $this->instanceid;
        $rec->term           = $this->term;
        $rec->classes        = json_encode($this->classes);
        $rec->uistate        = json_encode($this->uistate);
        $rec->settings       = json_encode($this->settings);
        $rec->archiveid      = $this->archiveid;
        $rec->notify         = $this->notify;
        $rec->message        = $this->message;
        $rec->messagesent    = $this->messagesent;
        $rec->errors         = $this->errors;

        // Send it to DB
        if ($recexists = $DB->get_record('enrol_wisc_controllers', array('controllerid' => $rec->controllerid), 'id')) {
            $rec->id = $recexists->id;
            $rec->timemodified = time();
            $DB->update_record('enrol_wisc_controllers', $rec);
        } else {
            $rec->timecreated = time();
            $rec->timemodified = 0;
            $rec->id = $DB->insert_record('enrol_wisc_controllers', $rec);
        }
        return $rec->id;
    }

    public static function load_controller($controllerid) {
        global $DB;
        // Load controller from DB
        if (! $rec = $DB->get_record('enrol_wisc_controllers', array('controllerid' => $controllerid))) {
            throw new moodle_exception('controller_nonexisting', 'enrol_wisc');
        }
        $controller = new enrol_wisc_controller($rec->userid, $rec->controllermode, $rec->courseid, $rec->instanceid);

        $classes = json_decode($rec->classes);
        if ($classes === NULL) {
            //throw new moodle_exception('controller_cant_decode_classes', 'enrol_wisc');
        }
        $uistate = json_decode($rec->uistate);
        if ($uistate === NULL) {
            //throw new moodle_exception('controller_cant_decode_uistate', 'enrol_wisc');
        }
        $settings = json_decode($rec->settings);
        if ($settings === NULL) {
            //throw new moodle_exception('controller_cant_decode_settings', 'enrol_wisc');
        }
        $controller->set_controllerid($rec->controllerid);
        $controller->set_status($rec->status);
        $controller->set_pid($rec->pid);
        $controller->set_courseid($rec->courseid);
        $controller->set_term($rec->term);
        $controller->set_classes($classes);
        $controller->set_uistate($uistate);
        $controller->set_settings($settings);
        $controller->set_archiveid($rec->archiveid);
        $controller->set_notify($rec->notify);
        $controller->set_message($rec->message);
        $controller->set_messagesent($rec->messagesent);
        $controller->set_errors($rec->errors);

        return $controller;
    }

    public function process_ui_event() {

        // Perform security checks throwing exceptions if something is wrong
        $this->helper_check_security();
    }

    public function finish_ui() {
        if ($this->status == self::STATUS_CREATED) {
            throw new moodle_exception('uinotconfigured', 'enrol_wisc');
        }
        if ($this->status != self::STATUS_CONFIGURED) {
            throw new moodle_exception('uialreadyexecuted', 'enrol_wisc');
        }
        $this->set_status(self::STATUS_AWAITING);
    }

    public function execute($options = array(), \core\progress\base $progress, base_logger $logger = null, $offline = true) {
        global $DB;

        if ($this->status > self::STATUS_LAUNCHED) {
            throw new moodle_exception('controlleralreadyexecuted', 'enrol_wisc');
        }

        if ($this->status < self::STATUS_AWAITING) {
            throw new moodle_exception('controllernotready', 'enrol_wisc');
        }

        // Update controller status
        $this->set_status(self::STATUS_EXECUTING);
        if ($offline) {
            $this->set_pid(getmypid());
        }
        $this->save_controller();

        $errorstr = '';
        try {
            if ($this->get_controllermode() == self::TYPE_COURSE) {
                $success = enrol_wisc_controller_helper::create_course($this, $errorstr, $options, $progress, $logger);
            } else if ($this->get_controllermode() == self::TYPE_INSTANCE){
                $success = enrol_wisc_controller_helper::create_instance($this, $errorstr, $options, $progress);
            } else {
                $success = enrol_wisc_controller_helper::update_instance($this, $errorstr, $options, $progress);
            }
        } catch (Exception $e) {
            $success = false;
            $errorstr = $e->getMessage();
        }
        if ($success) {
            if (!empty($options['customimport'])) {
                $this->set_status(self::STATUS_NOTIFYWAIT);
            } else {
                $this->set_status(self::STATUS_FINISHED_OK);
            }
            $this->set_errors(null);
        } else {
            $this->set_status(self::STATUS_FINISHED_ERR);
            // log to error log
            error_log("enrol/wisc/create.php?createid={$this->controllerid}:$errorstr");
            $this->set_errors($errorstr);
        }

        if ($this->get_status() != self::STATUS_NOTIFYWAIT) {
            enrol_wisc_controller_helper::notify_create_done($this, $offline);
        }
        $this->set_pid(null);
        $this->save_controller();
    }

    public function __toString() {
        global $DB;
        $settings = $this->get_settings();
        if (isset($settings->fullname)) {
            $coursename = $settings->fullname;
        } else if ($this->get_courseid()) {
            $coursename = $DB->get_field('course', 'fullname', array('id'=>$this->get_courseid()), IGNORE_MISSING);
        } else {
            $coursename = "UNKNOWN";
        }
        switch ($this->get_controllermode()) {
            case self::TYPE_COURSE:
                $mode = "Course creation";
                break;
            case self::TYPE_INSTANCE:
                $mode = "New enrol instance creation";
                break;
            case self::TYPE_EDIT:
                $mode = "Roster editing";
                break;
            default:
                $mode = "UNKNOWN MODE";
        }
        $output = array();
        $output[] = "$mode execution summary";
        $output[] = "------------------------";
        $output[] = "Name: " . $coursename;
        $output[] = "Courseid: " . $this->get_courseid();
        $output[] = "Errors: " . $this->get_errors();
        $output[] = "Controllerid: " . $this->get_controllerid();
        $output[] = "Mode: " . $mode;
        $output[] = "Term: " . $this->get_term();
        $output[] = "Classes: " . json_encode($this->get_classes());
        $output[] = "Settings: " . json_encode($this->get_settings());
        if ($this->get_archiveid()) {
            $archive = $DB->get_record('archive_course', array('id' => $this->get_archiveid()));
            $output[] = "Archive: {$archive->id} - {$archive->coursename} archived on {$archive->archivedate}";
        }
        return implode("\n", $output);
    }

// Protected API starts here

    protected function calculate_controllerid() {
        $this->controllerid = md5(time() . '-' . $this->userid . '-' . random_string(20));
    }

    protected function helper_check_security() {
        global $USER;
        if ($this->userid !== $USER->id) {
            throw new moodle_exception('useriddoesntmatch', 'enrol_wisc');
        }
    }

}

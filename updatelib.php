<?php

/**
 * Upgrade functions
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/lib.php');

/**
 * Check for course originator role
 * if it doesn't exist, create it and grant our default
 * permissions to it.
 *
 */
function wisc_check_courseoriginator() {
    global $DB;

       $role = $DB->get_record ( 'role', array('shortname'=>'courseoriginator'));
       if (!$role) {
           // need to create the role
           $role = new stdClass ( );
           $role->name = "WISC Course Originator";
           $role->shortname = "courseoriginator";
           $role->description = "This role is automatically assigned to a person at the course level when they create a new WISC course";

           // need to find the sort order item, and add 1
           $sql = 'SELECT MAX(sortorder) AS maxsort FROM {role}';
           $record = $DB->get_records_sql ( $sql );
           foreach ($record as $max) {
               $role->sortorder = ($max->maxsort) + 1;
           }
           $id = $DB->insert_record ( 'role', $role );

           // Get a manager role

           $manager = get_archetype_roles('manager');
           $manager = reset($manager);

           // set default allow assignments
           $allowassign = new stdClass ();
           $allowassign->roleid = $id;
           $allowassign->allowassign = $id;
           $DB->insert_record('role_allow_assign',$allowassign);

           if ($manager) {
               // set default allow for manager role
               $allowassign2 = new stdClass ();
               $allowassign2->roleid = $manager->id;
               $allowassign2->allowassign = $id;
               $DB->insert_record('role_allow_assign',$allowassign2);

               // set the default override permissions
               $override = new stdClass();
               $override->roleid = $manager->id;
               $override->allowoverride = $id;
               $DB->insert_record('role_allow_override',$override);
           }

           // set the default role capabilities
           $rolecap1 = new stdClass();
           $rolecap1->contextid = 1; // system level
           $rolecap1->roleid=$id;
           $rolecap1->capability="moodle/course:reset";
           $rolecap1->permission = 1;
           $rolecap1->timemodified = time();
           $rolecap1->modifierid=0;
           $DB->insert_record('role_capabilities',$rolecap1);

           $rolecap2 = new stdClass();
           $rolecap2->contextid = 1;
           $rolecap2->roleid=$id;
           $rolecap2->capability="moodle/course:delete";
           $rolecap2->permission = 1;
           $rolecap2->timemodified = time();
           $rolecap2->modifierid=0;
           $DB->insert_record('role_capabilities',$rolecap2);

           // set the role context level
           $rolecontext = new stdClass();
           $rolecontext->roleid=$id;
           $rolecontext->contextlevel=CONTEXT_COURSE;
           $DB->insert_record('role_context_levels',$rolecontext);

       }
}

function wisc_get_temp_category() {
    global $DB, $CFG;

    require_once($CFG->libdir.'/coursecatlib.php');

    $category = $DB->get_record('course_categories', array('idnumber'=>'enrol_wisc_tempcategory'));
    if ($category) {
        return $category->id;
    }

    // Not found, so create the temp category
    // Code from course/editcategory.php
    $category = new stdClass();
    $category->name = get_string('tempcategoryname', 'enrol_wisc');
    $category->visible = 0;
    $category->idnumber = 'enrol_wisc_tempcategory';
    $category->parent = 0;
    $category->description = get_string('tempcategorydescription', 'enrol_wisc');
    $category = coursecat::create($category);
    fix_course_sortorder();

    return $category->id;
}

function wisc_create_categories($termcodes = array(), $updateexisting = false, $resort = true) {
    $miscstr = get_string('misccategoryname', 'enrol_wisc');

    $datastore = new \enrol_wisc\local\chub\chub_datasource();
    $allterms = $datastore->getAvailableTerms();
    $subjects = array();

    if (empty($termcodes)) {
        $termcodes[] = \enrol_wisc\local\chub\timetable_util::get_current_termcode();
    }

    $terms = array();
    $termcodes = array_flip($termcodes);
    foreach ($allterms as $term) {
        if (isset($termcodes[$term->termCode])) {
            $terms[] = $term;
        }
    }

    // get all subjects from all terms
    foreach ($terms as $term) {
        $termsubjects = $datastore->getSubjectsInTerm($term->termCode);
        $subjects = array_merge($subjects, $termsubjects);
    }

    $orgcats = array();
    $subjectcats = array();
    foreach ($subjects as $subject) {
        if (isset($subjectcats[$subject->subjectCode])) {
            continue;
        }
        // check org category
        $org = $subject->schoolCollege;
        if (!isset($orgcats[$org->academicOrgCode])) {
            $category = wisc_update_category($org->shortDescription, 0, $org->academicOrgCode, $updateexisting);
            $orgcats[$org->academicOrgCode] = $category->id;
            // check misc category
            $category = wisc_update_category($miscstr, $orgcats[$org->academicOrgCode], $org->academicOrgCode.'_misc', $updateexisting);
        }

        // check dept category
        $category = wisc_update_category($subject->description, $orgcats[$org->academicOrgCode], $subject->subjectCode, $updateexisting);
        $subjectcats[$subject->subjectCode] = $category->id;

        // check term categories
        foreach ($terms as $term) {
            $category = wisc_update_category($term->longDescription, $subjectcats[$subject->subjectCode], $subject->subjectCode.'_'.$term->termCode, $updateexisting);
        }

        // check misc category
        $category = wisc_update_category($miscstr, $subjectcats[$subject->subjectCode], $subject->subjectCode.'_misc', $updateexisting);
    }
    // top-level org category
    $category = wisc_update_category($miscstr, 0, '_misc', $updateexisting);

    // let moodle fix the category hierarchy
    fix_course_sortorder();

    if ($resort) {
        wisc_sort_categories();
        fix_course_sortorder();
    }
}

function wisc_update_category($name, $parent, $idnumber, $updateexisting) {
    global $DB;
    $category = $DB->get_record('course_categories', array('idnumber'=>$idnumber));
    if (!$category) {
        $category = new stdClass();
        $category->name = $name;
        $category->parent = $parent;
        $category->idnumber = $idnumber;
        $category = create_course_category($category);
    } else if ($updateexisting) {
        if ($category->name != $name) {
            $category->name = $name;
            $DB->update_record('course_categories', $category);
        }
        if ($category->parent != $parent) {
            move_category($category, $parent);
            $category = $DB->get_record('course_categories', array('idnumber'=>$idnumber));
        }
    }
    return $category;
}

function wisc_sort_subcategories($categoryid) {
    global $DB;
    $categories = $DB->get_records('course_categories', array('parent'=>$categoryid));
    // Resort the category
    if ($courses = get_courses($category->id, "fullname ASC", 'c.id,c.fullname,c.sortorder')) {
        $i = 1;
        foreach ($courses as $course) {
            $DB->set_field('course', 'sortorder', $category->sortorder+$i, array('id'=>$course->id));
            $i++;
        }
        fix_course_sortorder(); // should not be needed
    }
}

function wisc_sort_categories() {
    global $DB;
    // This is PHP 5.3 code!

    $allorgs  = $DB->get_records('course_categories', array('depth'=>1), '', 'id, sortorder, idnumber, parent, name');
    $allsubjs = $DB->get_records('course_categories', array('depth'=>2), '', 'id, sortorder, idnumber,parent, name');
    $allterms = $DB->get_records('course_categories', array('depth'=>3), '', 'id, sortorder, idnumber,parent, name');

    // sort function for orgs and subjects
    $namecmp = function ($a, $b) {
        // Always put misc categories at the end
        $amisc = strstr($a->idnumber, '_misc');
        $bmisc = strstr($b->idnumber, '_misc');
        if ($amisc && !$bmisc) {
            return 1;
        }
        if (!$amisc && $bmisc) {
            return -1;
        }
        $res = strnatcasecmp($a->name, $b->name);
        if ($res === 0) {
            $res = ($a->id < $b->id)? -1: 1;
        }
        return $res;
    };

    // sort function for terms
    $termcmp = function ($a, $b) {
        return (-1)*(strcmp($a->idnumber, $b->idnumber));
    };

    uasort($allorgs, $namecmp);
    uasort($allsubjs, $namecmp);
    uasort($allterms, $termcmp);

    foreach ($allterms as $cat) {
        $allsubjs[$cat->parent]->children[] = $cat;
    }
    unset($allterms);
    foreach ($allsubjs as $cat) {
        $allorgs[$cat->parent]->children[] = $cat;
    }
    unset($allsubjs);

    $sortorder = 0;
    _wisc_sort_categories($allorgs, $sortorder);
}

function _wisc_sort_categories($children, &$sortorder) {
    global $DB;

    foreach ($children as $cat) {
        $sortorder = $sortorder + MAX_COURSES_IN_CATEGORY;
        if ($cat->sortorder != $sortorder) {
            $cat->sortorder = $sortorder;
            $DB->update_record('course_categories', $cat, true);
        }
        if (isset($cat->children)) {
            _wisc_sort_categories($cat->children, $sortorder);
        }
    }
}
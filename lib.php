<?php
/**
 * University of Wisconsin - Madison CHUB enrollment plugin.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use enrol_wisc\local\chub\chub_datasource;
use enrol_wisc\local\chub\timetable_util;
use enrol_wisc\local\chub\schema\wisc_timetable_class;
use enrol_wisc\local\chub\schema\wisc_timetable_instructor;


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/group/lib.php');
require_once($CFG->dirroot.'/lib/authlib.php');

if (file_exists($CFG->dirroot.'/local/wiscservices/locallib.php')) {
    require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
}

class enrol_wisc_plugin extends enrol_plugin {

    // data sources
    /** @var enrol_wisc\local\chub\datasource */
    protected $datastore;
    /** @var local_wiscservices_plugin */
    protected $wiscservices;

    // cached data
    protected $activeterms;

    // error logging prefix
    protected $errorlogtag = '[ENROL WISC] ';

    // custom fields in enrol table
    const termfield = 'customchar1';        // stores term code
    const groupingidfield = 'customint1';   // stores grouping id
    const tarolefield = 'customint2';        // stores ta role
    const auditorrolefield = 'customint3';   // stores auditor role

    // status for enrol_wisc_coursemap approved field
    const ASSOCIATION_PENDING = 0;          // not approved
    const ASSOCIATION_APPROVED = 1;         // approved
    const ASSOCIATION_MAILED = 2;           // not approved and email sent to admin

    // uw role course creator capabilities, as stored in config table
    const CREATECOURSE_NONE = "0";
    const CREATECOURSE_LIMITED= "1";
    const CREATECOURSE_FULL = "2";

    // datastore object types for the access table
    const ACCESS_SUBJECT  = 1;  // allow by subject code
    const ACCESS_ORG      = 2;  // allow by academic org

    /**
     * Return uw roles which can be assigned coursecreator access
     *
     * @return array in the form isis name => displayname
     */
    public static function get_coursecreator_uwroles() {
        return array( 'Instructor' => 'Instructor',
                      'Faculty' => 'Faculty',
                        'LimitedEmployee' => 'Limited Employee',
                      'AcademicStaff' => 'Academic Staff',
                      'EmployeeInTraining' => 'Employee In Training (aka postdocs)',
                      'ClassifiedPermanent' => 'Classified Permanent',
                      'ClassifiedLTE' => 'Classified LTE',
                      'ClassifiedProject' => 'Classified Project',
                      'ClassifiedPermanent' => 'Classified Permanent');
    }

    /**
     * Get all UW terms in the foreseeable future, for configuration purposes
     *
     * We could get this information from CHUB, but then we'd be making SOAP calls just to construct the admin tree.  Using CHUB would
     * also mean that we'd risk losing settings if CHUB was down.
     *
     * @return array in the form termcode => displayname
     */
    public static function get_all_terms_menu() {
        $MINYEAR = 2013;  // Academic year starting in Fall
        $MAXYEAR = 2020;

        $terms = array();
        for ($year = $MINYEAR ; $year <= $MAXYEAR; ++$year) {
            $prefix = sprintf("%3d", ($year - 1900 + 1));
            $yearterms = array($prefix."2" => "Fall ".($year),
                               $prefix."3" => "Winter ".($year+1),
                               $prefix."4" => "Spring ".($year+1),
                               $prefix."6" => "Summer ".($year+1),
                              );
            $terms += $yearterms;
        }
        $terms[0] = "Other";
        return $terms;
    }

    /**
     * Determine if the instructor should have the faculty role
     *
     * @param wisc_timetable_instructor $instructor
     * @return bool
     */
    protected function is_faculty(wisc_timetable_instructor $instructor) {
        $roleCode = isset($instructor->roleCode)? $instructor->roleCode : 0;
        return $roleCode >= 1 && $roleCode <= 6;
    }

    /**
     * Determine if the instructor should have the ta role
     *
     * @param wisc_timetable_instructor $instructor
     * @return bool
     */
    protected function is_ta(wisc_timetable_instructor $instructor) {
        $roleCode = isset($instructor->roleCode)? $instructor->roleCode : 0;
        return $roleCode == 7;
    }

    /**
     * Returns localised name of enrol instance
     *
     * @param object $instance (null is accepted too)
     * @return string
     */
    public function get_instance_name($instance) {
        global $DB;

        if (empty($instance)) {
            $enrol = $this->get_name();
            return get_string('pluginname', 'enrol_'.$enrol);
        } else {
            $enrol = $this->get_name();
            $termname = timetable_util::get_term_name($instance->{enrol_wisc_plugin::termfield});
            return get_string('pluginname', 'enrol_'.$enrol) . ' ' . $termname;
        }
    }

    /**
     * Get the datastore, creating it if necessary
     *
     * @return wisc_timetable_datastore | null
     */
    protected function get_datastore() {
        if (!isset($this->datastore)) {
            try {
               $this->datastore = new chub_datasource();
            } catch (Exception $e) {
               error_log($this->errorlogtag.$e->getMessage());
               return null;
            }
        }
        return $this->datastore;
    }

    /**
     * Get the wiscservices plugin, creating it if necessary
     *
     * @return local_wiscservices_plugin | null
     */
    protected function get_wiscservices() {
        if (!isset($this->wiscservices)) {
            try {
               if (!class_exists('local_wiscservices_plugin')) {
                   throw new Exception('No local/wiscservices plugin');
               }
               $this->wiscservices = new local_wiscservices_plugin();
            } catch (Exception $e) {
               error_log($this->errorlogtag.$e->getMessage());
               return null;
            }
        }
        return $this->wiscservices;
    }

    /**
     * sync a single course
     *
     * @param int courseid
     */
    public function sync_course_enrolments($courseid, \core\progress\base $progress = null) {
        global $DB;
        @set_time_limit(0); // this might take a long time
        $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
        $instances = $DB->get_records('enrol', array('courseid'=>$courseid, 'enrol'=>'wisc'));
        foreach ($instances as $instance) {
            $this->sync_instance($instance, $course, $progress);
        }
    }

    /**
     * sync a given enrolment instance
     *
     * @param object enrol instance
     * @param object course
     */
    public function sync_instance($instance, $course, \core\progress\base $progress = null) {
        global $CFG, $DB;
        require_once($CFG->libdir.'/gradelib.php');

        $datastore = $this->get_datastore();
        $wiscservices = $this->get_wiscservices();

        // Some roster calls take a long time to return.
        $datastore->set_timeout(300);

        if (!$datastore || !$wiscservices) {
            return;
        }

        if (!$progress) {
            $progress = new \core\progress\none();
        }

        // Load our configured roleid's
        $student_roleid = $this->get_config('studentrole');
        $ferpa_roleid = $this->get_config('ferparole');
        $teacher_roleid = $this->get_config('teacherrole');

        $other_roleid = $this->get_config('otherrole');

        $ta_roleid = $instance->{enrol_wisc_plugin::tarolefield};
        if (is_null($ta_roleid)) {
            $ta_roleid = $this->get_config('tarole');
        }

        $auditor_roleid = $instance->{enrol_wisc_plugin::auditorrolefield};
        if (is_null($auditor_roleid)) {
            $auditor_roleid = $this->get_config('auditorrole');
        }

        $coursemaps = $DB->get_records('enrol_wisc_coursemap', array('enrolid'=>$instance->id));

        $progress->start_progress('Syncing enrollments', 3);

        $term = $instance->{enrol_wisc_plugin::termfield};
        $termname = timetable_util::get_term_name($term);
        CLI_SCRIPT && print_string('synccourse', 'enrol_wisc', array('shortname'=>$course->shortname,'id'=>$course->id,'instance'=>$termname));

        // enrol students if 'studentsoncreate' is set
        $enrolstudents = /* $this->get_config('studentsoncreate') &&*/ $student_roleid;

        $ignorehidden = $this->get_config('ignorehiddencourses');
        if ($ignorehidden && !$course->visible) {
            $enrolstudents = false;
        }

        // Return if the term is not active
        $activeterms = $this->get_terms();
        if (empty($activeterms[(integer)$term])) {
            error_log($this->errorlogtag."Skipping course {$course->shortname}, term $term.");
            $progress->end_progress();
            return;
        }

        // Create the section grouping
        $groupingname = $activeterms[(integer)$term]->longDescription . ' Sections';
        $groupingid = $this->create_grouping($instance, $course, $groupingname);

        // Set to true if CHUB is missing a section which is being deleted.
        $missingchubsection = false;

        // Fetch each class in the coursemap and organize by courseid/section
        $extcourses = array();
        $extcoursemaps = array();
        $deletedcourses = array();
        $groupinfo = array();
        $dropsbegin = 0;
        foreach ($coursemaps as $coursemap) {
            if ($coursemap->term !== $term) {
                error_log($this->errorlogtag."Skipping coursemap entry for {$course->shortname}, wrong term.");
                continue;
            }
            $classNumber = $coursemap->class_number;

            try {
                $extclass = $datastore->getClassByClassNumber($term, $classNumber);
                $uniqueid = $extclass->courseId.'_'.$extclass->sectionNumber; // unique across crosslists
                $extcourses[$uniqueid][] = $extclass;
                $extcoursemaps[$uniqueid][] = $coursemap;
                $dropsbegin = max($dropsbegin, $extclass->startDate);// find the latest start date
                                                                     // We use this to decide when dropped students will start
                                                                     //  officially showing up.
            } catch (Exception $e) {
                error_log($this->errorlogtag."({$course->shortname},$term,$classNumber) Error: ".$e->getMessage());
                if ($coursemap->deleted  && preg_match('/class does not exist/', $e->getMessage())) {
                    // Class doesn't exist, and we're deleting the section.  In this case, we'll update the
                    // enrollment using remaining sections, and ignore drops.  This happens occasionally when
                    // CHUB loses a class for some reason.
                    $missingchubsection = true;
                } else {
                    $progress->end_progress();
                    return; // abort
                }
            }
            if ($coursemap->deleted) {
                // keep track of deleted classes
                $deletedcourses[$classNumber] = true;
            }
        }

        // [UWMOODLE-184] Add 7 hours, since apparently drops don't start being recorded at 5AM on the first day of the semester.
        //                Hopefully they'll start by noon.
        $dropsbegin += 7*60*60;

        // check if dropped students will show up in the getStudentsThatHaveDropped() call.
        //
        //   We wait one full sync cycle after drops are supposed to start
        //   before using the call so that we don't miss any last minute drops.
        //   CHUB doesn't give us the date drops begin, so we use use course start date instead.
        //
        $lastsync = $this->get_config('lastsync', 0);
        $usedrops = $lastsync > $dropsbegin; // True if we are sure drops are being recorded

        //
        // Fetch rosters, create groups, and add/update people information
        //
        $enrols = array();

        // some of the roleid's may be 0, but we'll handle that later.

        $enrols[$student_roleid] = array();
        $enrols[$auditor_roleid] = array();
        $enrols[$ferpa_roleid] = array();
        $enrols[$teacher_roleid] = array();
        $enrols[$ta_roleid] = array();
        $enrols[$other_roleid] = array();

        $alldropped = array();

        $progress->progress(1);

        $progress->start_progress('Querying UDS', count($coursemaps));
        foreach ($extcourses as $uniqueid => &$extcourse) {
            $subjects = array();
            $groupmembers = array();
            $approved = true;
            foreach ($extcoursemaps[$uniqueid] as $coursemap) {
                $approved = $approved && $coursemap->approved;
            }
            if (!$approved && $enrolstudents) {
                error_log($this->errorlogtag."({$course->shortname},$term,$extclass->classNumber): Not approved for enrolment");
            }
            foreach ($extcourse as $extclass) {
                $progress->increment_progress();
                // query students if we are asked to and the mapping is approved, or anytime we're deleting a coursemap
                $deletedclass = !empty($deletedcourses[$extclass->classNumber]);
                $querystudents = $enrolstudents && $approved || $deletedclass;
                try {
                    $dropped = array();
                    $instructors = array();
                    $students = array();
                    if ($querystudents) {
                        $roster = $datastore->getClassRoster($term, $extclass->classNumber);
                        $dropped = $datastore->getStudentsThatHaveDropped($term, $extclass->classNumber);
                        $students = $roster->students;
                        $instructors = $roster->instructors;
                        unset($roster);
                    } else {
                        $instructors = $datastore->getInstructors($term, $extclass->classNumber);
                    }
                    if ($deletedclass) {
                        //pretend all the students dropped
                        $dropped = array_merge($dropped, $students);
                        //and clear the roster
                        $students = array();
                        $instructors = array();
                    }

                    foreach ($students as $student) {
                        $userid = $wiscservices->verify_person($student->person);
                        if ($userid === false) {
                            continue;
                        }
                        if ($student->isAuditing && $auditor_roleid != $student_roleid) {
                            // Use special auditor role.
                            // We won't have a "_hidden" role, so ignore ferpa status.  In most cases, other roles are already "hidden"
                            // in the same sense as hidden_student, anyhow.
                            if ($auditor_roleid) {
                                $enrols[$auditor_roleid][] = $userid;
                                $groupmembers[] = $userid;
                            }
                        } else {
                            // Use standard student role.
                            if (!$student->person->ferpaName) {
                                $enrols[$student_roleid][] = $userid;
                            } else {
                                $enrols[$ferpa_roleid][] = $userid;
                            }
                            $groupmembers[] = $userid;
                        }
                    }
                    foreach ($dropped as $student) {
                        $userid = $wiscservices->verify_person($student->person);
                        if ($userid === false) {
                            continue;
                        }
                        $alldropped[] = $userid;
                    }
                    foreach ($instructors as $instructor) {
                        $userid = $wiscservices->verify_person($instructor->person);
                        if ($userid === false) {
                            continue;
                        }
                        if ($this->is_faculty($instructor)) {
                            $enrols[$teacher_roleid][] = $userid;
                        } else if ($this->is_ta($instructor)) {
                            $enrols[$ta_roleid][] = $userid;
                        } else {
                            $enrols[$other_roleid][] = $userid;
                        }
                        $groupmembers[] = $userid;
                    }
                } catch (Exception $e) {
                    error_log($this->errorlogtag."({$course->shortname},$term,$extclass->classNumber) Error: ".$e->getMessage());
                    $progress->end_progress();
                    $progress->end_progress();
                    return; //abort
                }
                $subjects[] = $extclass->subject;
            }
            // UWMOODLE-738 Save the group info for later processing so that we can detect missing students first.
            $groupinfo[$uniqueid] = new stdClass();
            $groupinfo[$uniqueid]->groupname = $this->make_group_name(reset($extcourse), $subjects);
            $groupinfo[$uniqueid]->groupmembers = $groupmembers;
            $groupinfo[$uniqueid]->coursemaps = $extcoursemaps[$uniqueid];
        }
        $progress->end_progress();

        unset($enrols[0]); // if any role id's were zero, we ignore them
                           // In this case, we did already run verify_person on the corresponding people if they were
                           // instructors, but that's not a bad idea even if they are not automatically
                           // automatically enrolled in the class.

        // Keep track of all users enrolled in the course.  This is used in unenrolling users
        $allmembers = array();
        foreach ($enrols as $members) {
            $allmembers = array_merge($allmembers, $members);
        }

        // Update course enrolment
        $roles = get_all_roles();
        $preserveusers = array();  // User ids that should have their group enrollment preserved.  See UWMOODLE-738.

        $progress->start_progress('Updating enrollments', count($enrols));
        foreach ($enrols as $roleid => $members) {
            if (empty($roles[$roleid])) {
                error_log($this->errorlogtag."No role with id $roleid");
                continue;
            }
            $progress->start_progress('Updating role', count($members));
            $role = $roles[$roleid];
            $unenrolaction = $this->get_config('unenrolaction');
            if (!$usedrops) {
                $unenrolaction = ENROL_EXT_REMOVED_UNENROL; // course hasn't started, so just delete the enrolment
            }

            $isstudentrole = ($roleid == $student_roleid) || ($roleid == $ferpa_roleid);

            // Find role assignments that are in the db, but not external
            $exclude = $members;
            /*if ($resync && $isstudentrole && $usedrops) {
                // A couresemap entry was deleted in an active course, so
                // when computing delta exclude official drops as well
                $exclude = array_merge($exclude, $alldropped);
            }*/

            // Prune old enrolments
            // hopefully they'll fit in the max buffer size for the RDBMS
            $sql= "SELECT u.id as userid, u.username, u.idnumber, ue.status, ra.contextid
                     FROM {user} u
                     JOIN {role_assignments} ra ON (ra.userid = u.id AND ra.component = 'enrol_wisc' AND ra.roleid = :roleid)
                     JOIN {user_enrolments} ue ON (ue.userid = u.id AND ue.enrolid = ra.itemid)
                    WHERE u.deleted = 0 AND ue.enrolid = :enrolid ";
            $params = array('roleid'=>$roleid, 'enrolid'=>$instance->id);
            if (!empty($exclude)) {
                list($sql2, $params2) = $DB->get_in_or_equal($exclude, SQL_PARAMS_NAMED, 'm0', false); // not in clause
                $sql .= "AND u.id $sql2";
                $params = array_merge($params, $params2);
                unset($params2);
            }
            unset($exclude);
            $todelete = $DB->get_records_sql($sql, $params);

            $context = context_course::instance($course->id);;
            if (!empty($todelete)) {
                $transaction = $DB->start_delegated_transaction();
                foreach ($todelete as $row) {
                    if (array_search($row->userid, $allmembers) !== false) {
                        // user still exists in course, so just remove role
                        role_unassign($roleid, $row->userid, $row->contextid, 'enrol_wisc', $instance->id);
                        error_log($this->errorlogtag.get_string('extremovedroleunassign', 'enrol_wisc',
                                                                array('userid'=>$row->userid,
                                                                      'course'=>$course->shortname,
                                                                      'courseid'=>$course->id,
                                                                      'roleid'=>$roleid)));
                    } else if ($usedrops && !$missingchubsection && $isstudentrole && (array_search($row->userid, $alldropped) === false)) {
                        // student went away, but no drop record, so just log a message
                        $classnumbers = array();
                        foreach ($coursemaps as $coursemap) {
                            $classnumbers[] = $coursemap->class_number;
                        }
                        error_log($this->errorlogtag.get_string('extremovednodrop', 'enrol_wisc',
                                                                    array('userid'=>$row->userid,
                                                                          'useridnumber'=>$row->idnumber,
                                                                          'course'=>$course->shortname,
                                                                          'courseid'=>$course->id,
                                                                          'term'=>$term,
                                                                          'classnumbers'=>implode(',', $classnumbers))));
                        $preserveusers[] = $row->userid;
                        unset($classnumbers);
                    } else {
                        // user doesn't exist in course, so unenrol alltogether
                        // TODO: Refactor so that course unenrollment happens later, after all roles are processed
                        switch ($unenrolaction) {
                        case ENROL_EXT_REMOVED_UNENROL:
                            $this->unenrol_user($instance, $row->userid);
                            error_log($this->errorlogtag.get_string('extremovedunenrol', 'enrol_wisc',
                                                                    array('userid'=>$row->userid,
                                                                          'course'=>$course->shortname,
                                                                          'courseid'=>$course->id)));
                            break;
                        case ENROL_EXT_REMOVED_KEEP:
                            // Keep - only adding enrolments
                            break;
                        case ENROL_EXT_REMOVED_SUSPEND:
                            if ($row->status != ENROL_USER_SUSPENDED) {
                                $DB->set_field('user_enrolments', 'status', ENROL_USER_SUSPENDED, array('enrolid'=>$instance->id, 'userid'=>$row->userid));
                                error_log($this->errorlogtag.get_string('extremovedsuspend', 'enrol_wisc',
                                                                        array('userid'=>$row->userid,
                                                                              'useridnumber'=>$row->idnumber,
                                                                              'course'=>$course->shortname,
                                                                              'courseid'=>$course->id)));
                            }
                            break;
                        case ENROL_EXT_REMOVED_SUSPENDNOROLES:
                            if ($row->status != ENROL_USER_SUSPENDED) {
                                $DB->set_field('user_enrolments', 'status', ENROL_USER_SUSPENDED, array('enrolid'=>$instance->id, 'userid'=>$row->userid));
                            }
                            role_unassign_all(array('contextid'=>$row->contextid, 'userid'=>$row->userid, 'component'=>'enrol_wisc', 'itemid'=>$instance->id));
                            error_log($this->errorlogtag.get_string('extremovedsuspendnoroles', 'enrol_wisc',
                                                                    array('userid'=>$row->userid,
                                                                          'course'=>$course->shortname,
                                                                          'courseid'=>$course->id)));
                            break;
                        }
                    }
                }
                $transaction->allow_commit();
            }

            // Insert current enrolments
            // bad we can't do INSERT IGNORE with postgres...
            $transaction = $DB->start_delegated_transaction();
            foreach ($members as $member) {
                $sql= "SELECT ue.status
                         FROM {user_enrolments} ue
                         JOIN {role_assignments} ra ON (ra.itemid = ue.enrolid AND ra.userid = ue.userid)
                        WHERE ue.userid = :userid AND ue.enrolid = :enrolid AND ra.roleid = :roleid AND ra.component = 'enrol_wisc'";
                $params = array('userid'=>$member, 'enrolid'=>$instance->id, 'roleid'=>$roleid);
                $userenrolment = $DB->get_record_sql($sql, $params);

                if(empty($userenrolment)) {
                    // Don't recover grades. we'll do that only if necessary later, to avoid warnings
                    $this->enrol_user($instance, $member, $roleid, 0, 0, null, false);
                    // Make sure we set the enrolment status to active. If the user wasn't
                    // previously enrolled to the course, enrol_user() sets it. But if we
                    // configured the plugin to suspend the user enrolments _AND_ remove
                    // the role assignments on external unenrol, then enrol_user() doesn't
                    // set it back to active on external re-enrolment. So set it
                    // unconditionnally to cover both cases.
                    $DB->set_field('user_enrolments', 'status', ENROL_USER_ACTIVE, array('enrolid'=>$instance->id, 'userid'=>$member));
                    // Try to restore grade history, if any. This will fail if the user was still enrolled via another mechanism
                    // so we check first that the user has no grades
                    $sql = "SELECT gg.id
                              FROM {grade_grades} gg
                              JOIN {grade_items} gi ON gi.id = gg.itemid
                             WHERE gi.courseid = :courseid AND gg.userid = :userid";
                    $params = array('userid' => $member, 'courseid' => $course->id);
                    if (!$DB->record_exists_sql($sql, $params)) {
                        grade_recover_history_grades($member, $course->id);
                    }
                    error_log($this->errorlogtag.get_string('enroluser', 'enrol_wisc',
                                                            array('userid'=>$member,
                                                                  'course'=>$course->shortname,
                                                                  'courseid'=>$course->id)));

                } else {
                    if ($userenrolment->status == ENROL_USER_SUSPENDED) {
                        // Reenable enrolment that was previously disabled. Enrolment refreshed
                        $DB->set_field('user_enrolments', 'status', ENROL_USER_ACTIVE, array('enrolid'=>$instance->id, 'userid'=>$member));
                        error_log($this->errorlogtag.get_string('enroluserenable', 'enrol_wisc',
                                                                array('userid'=>$member,
                                                                      'course'=>$course->shortname,
                                                                      'courseid'=>$course->id)));
                    }
                }
                $progress->increment_progress();
            }
            $transaction->allow_commit();
            $progress->end_progress();
        }
        $progress->end_progress();

        // Update group membership.
        foreach ($groupinfo as $uniqueid=>$info) {
            CLI_SCRIPT && print_string('processgroup', 'enrol_wisc', array('name'=>$info->groupname,'count'=>count($info->groupmembers)));
            $this->update_group($info->groupname, $groupingid, $info->groupmembers, $info->coursemaps, $course, $preserveusers);
        }

        // delete any role assignments in roles we are not configured to handle
        // this could happen if the module configuration has changed.
        $sql= "SELECT u.id as userid, u.username, ue.status, ra.contextid, ra.roleid
                 FROM {user} u
                 JOIN {role_assignments} ra ON (ra.userid = u.id AND ra.component = 'enrol_wisc')
                 JOIN {user_enrolments} ue ON (ue.userid = u.id AND ue.enrolid = ra.itemid)
                WHERE u.deleted = 0 AND ue.enrolid = :enrolid ";
        $params = array('enrolid'=>$instance->id);
        if (!empty($enrols)) {
            list($sql2, $params2) = $DB->get_in_or_equal(array_keys($enrols), SQL_PARAMS_NAMED, 'm0', false);
            $sql .= "AND ra.roleid $sql2";
            $params = array_merge($params, $params2);
            unset($params2);
        }
        $todelete = $DB->get_records_sql($sql, $params);
        if (!empty($todelete)) {
            $transaction = $DB->start_delegated_transaction();
            foreach ($todelete as $row) {
                if (array_search($row->userid, $allmembers) === false) {
                    // user isn't in course, so unenrol
                    $this->unenrol_user($instance, $row->userid);
                    error_log($this->errorlogtag.get_string('extremovedunenrol', 'enrol_wisc',
                                                            array('userid'=> $row->userid,
                                                                  'course'=>$course->shortname,
                                                                  'courseid'=>$course->id)));
                } else {
                    // user still exists in course, so just remove role
                    role_unassign($row->roleid, $row->userid, $row->contextid, 'enrol_wisc', $instance->id);
                    error_log($this->errorlogtag.get_string('extremovedroleunassign', 'enrol_wisc',
                                                            array('userid'=>$row->userid,
                                                                  'course'=>$course->shortname,
                                                                  'courseid'=>$course->id,
                                                                  'roleid'=>$row->roleid)));
                }
            }
            $transaction->allow_commit();
        }

        // delete any coursemaps marked as deleted
        $DB->delete_records('enrol_wisc_coursemap', array('enrolid'=>$instance->id, 'deleted'=>1));

        $progress->end_progress();

        return true;
    }

    /**
     * Determine the group name for a timetable section
     *
     * @param wisc_timetable_class $class
     * @param array $subjects
     *
     * @return string
     */
    protected function make_group_name(wisc_timetable_class $class, Array $subjects) {
        foreach ($subjects as $s) {
            $shortnames[] = preg_replace('/ /', '', $s->shortDescription);
        }
        sort($shortnames);
        $name = sprintf("%s %s %s %s", implode('/',$shortnames),
                                       $class->catalogNumber,
                                       $class->type,
                                       $class->sectionNumber);
        return $name;
    }

   /**
     * Get the list of current and future active terms.  A term is current
     * if its enddate isn't in the past, and a term is active if the datastore
     * has data on it.
     *
     * @return array of wisc_timetable_term with keys the (numeric) termCode
     */
    protected function get_terms() {
        // cache the result
        if (!empty($this->activeterms)) {
            return $this->activeterms;
        }

        if (!($datastore = $this->get_datastore())) {
            return array();
        }
        try {
            $activeterms = $datastore->getAvailableTerms();
            $now = time();
            $terms = array();
            foreach ($activeterms as $key=>$term) {
                if ($term->endDate >= $now) {
                    $terms[(integer)$term->termCode] = $term;
                }
            }
        } catch (Exception $e) {
            error_log($this->errorlogtag.$e->getMessage());
            return array();
        }
        $this->activeterms = $terms;
        return $terms;
    }

    /**
     * Create a new grouping for the section groups
     *
     * @param object $instance our instance
     * @param object $course moodle course
     * @param string $groupingname name of new grouping
     *
     * @return groupingid | false
     */
    protected function create_grouping($instance, $course, $groupingname) {
        global $DB;
        if (!empty($instance->{enrol_wisc_plugin::groupingidfield})) {
            $groupingid = $instance->{enrol_wisc_plugin::groupingidfield};
            $grouping = groups_get_grouping($groupingid);
            if (empty($grouping) || $grouping->courseid != $course->id) {
                error_log($this->errorlogtag."Unexpectantly missing grouping '$groupingname' ($groupingid)");
                $groupingid = null;
            }
        }
        if (empty($groupingid)) {
            $groupinginfo = new object();
            $groupinginfo->name = $groupingname;
            $groupinginfo->courseid = $course->id;
            // Note: We use the following text to identify roter groupings on restore.
            $groupinginfo->description = "The groups in this grouping are kept in sync with the class roster. New persons can be manually added to any existing group and will merge with the university enrolments.";
            $groupinginfo->descriptionformat = FORMAT_PLAIN;
            $groupingid = groups_create_grouping($groupinginfo);
            if (!$groupingid) {
                error_log($this->errorlogtag."Failed to create Section grouping '$groupingname' for course {$course->id}\n");
                return false;
            }
            $instance->customint1 = $groupingid;
            $DB->set_field('enrol', enrol_wisc_plugin::groupingidfield, $groupingid, array('id' => $instance->id));
        }
        return $groupingid;
    }

    /**
     * Update section group.
     *
     * When examing existing group members, we use the modification time as a flag:
     * A modification time of 0 indicates that the member was automatically enrolled
     * in the group and is free to be removed if they are no longer in the section.
     * Any other modification time, and we assume that the assignment was done manually
     * and we leave it alone.
     *
     * @param string $name group name
     * @param int $secgroupingid grouping
     * @param $membersin array of moodle id's
     * @param $coursemaps array of coursemap entries
     * @param $course which course
     * @param array $preserveusers Users which should be left in groups regardless of $membersin
     */
    protected function update_group($name, $secgroupingid, $membersin, $coursemaps, $course, $preserveusers)
    {
        global $DB, $CFG;
        include_once($CFG->dirroot.'/group/lib.php');

        $members = array_unique($membersin);

        $groupid = null;
        foreach ($coursemaps as $coursemap) {
            if (!is_null($coursemap->groupid)) {
                $groupid = $coursemap->groupid;
                break;
            }
        }
        $group = false;
        if (!is_null($groupid)) {
            // load existing group
            if (!$group = groups_get_group($groupid)) {
                error_log($this->errorlogtag."Unexpectantly missing group '$name' ($groupid)");
                $groupid = null;
            }
            if ($group->courseid != $course->id) {
                // UWMOODLE-803 - This corrects a bug where stale groupids were restored from backups.
                error_log($this->errorlogtag."Stale groupid in coursemap (wrong course) '$name' ($groupid)");
                // Ensure that grouping is cleaned up.
                groups_unassign_grouping($secgroupingid, $groupid); //idempotent call
                $groupid = null;
                $group = null;
            }
        }

        if (!$group) {
            // create new group
            $groupsettings = new object();
            $groupsettings->name = $name;
            $groupsettings->courseid = $course->id;
            $groupsettings->description = "This group will always contain the students in the associated sections.  New persons can be added to the group if need be, but enrolled students cannot be removed.";
            $groupid = groups_create_group($groupsettings);
            if (!$groupid) {
                error_log($this->errorlogtag."Failed to create group $name for course {$course->id}");
               return false;
            }
            $group = groups_get_group($groupid);
            error_log($this->errorlogtag.get_string('creategrp','enrol_wisc',
                                                    array('name'=>$name,
                                                          'course'=>$course->shortname,
                                                          'courseid'=>$course->id)));
        } else if ($group->name !== $name) {
            // name isn't current
            $group->name = $name;
            $group->timemodified = time();
            $DB->update_record('groups', $group);
        }
        // make sure it's in the right grouping
        groups_assign_grouping($secgroupingid, $groupid); //idempotent call

        // Get group members.  We can't call groups_get_members() since we want
        // the modificaton time.
        $oldmembers = $DB->get_records_sql_menu("SELECT u.id, u.id
                                            FROM {groups_members} gm
                                            JOIN {user} u ON u.id = gm.userid
                                            WHERE gm.component = 'enrol_wisc'
                                              AND gm.groupid = ?",
                                           array($groupid));
        $oldmemberids = array_keys($oldmembers);
        $toadd = array_diff($members,$oldmemberids);
        $todel = array_diff($oldmemberids,$members);

        foreach ($toadd as $userid) {
            groups_add_member($groupid, $userid, 'enrol_wisc');
        }
        foreach ($todel as $userid) {
            // remove extra users from group
            if (!in_array($userid, $preserveusers)) {
                // automatic assignment
                groups_remove_member($groupid, $userid);
            }
        }

        // Update all the coursemap records with our groupid
        foreach ($coursemaps as $coursemap) {
            if ($coursemap->groupid != $groupid) {
                $coursemap->groupid = $groupid;
                $DB->update_record('enrol_wisc_coursemap', $coursemap);
            }
        }

        return true;
    }

    /**
     * Sync all enrolments in a term
     *
     * @param string $term  term code
     */
    public function sync_term_enrolments($term) {
        global $DB;
        $instances = $DB->get_records("enrol", array(enrol_wisc_plugin::termfield=>$term));
        foreach ($instances as $instance) {
            try {
                $course = $DB->get_record('course', array('id'=>$instance->courseid), '*', MUST_EXIST);
                $this->sync_instance($instance, $course);
            } catch (Exception $e) {
                error_log($this->errorlogtag.$e->getMessage());
            }
        }
    }

    /**
     * Sync all enrolments in all active terms
     *
     * @param bool $force force updates, ignoring the datastore timestamps
     */
    public function sync_enrolments($force = false) {
        if (!$datastore = $this->get_datastore()) {
            return;
        }
        $lastsync = $this->get_config('lastsync', 0);
        try {
            $terms = $this->get_terms();
            $maxlastupdated = 0;
            // Find the most recently updated term
            foreach ($terms as $term) {
                $lastupdated = $datastore->getTermLastUpdated($term->termCode);
                if ($lastupdated > $maxlastupdated) {
                    $maxlastupdated = $lastupdated;
                }
            }
            // Sync all terms.  We can't do the $lastupdated > $lastsync reliably on a term-by-term basis since
            // we're only storing a single lastsync time.  Instead we use max($terms->lastupdated) > $lastsync
            //
            // There are some (rare) circumstances where this code may result in updates for a term being delayed one
            // CHUB update cycle, but to fix that we'd have to keep track of the update time on each term individually.  In practice
            // the terms seem to update at the same time, so it hasn't been an issue.
            $dosync = $force || $maxlastupdated > $lastsync;
            foreach ($terms as $term) {
                if ($dosync) {
                    $this->sync_term_enrolments($term->termCode);
                } else {
                    print_string ('skippingterm', 'enrol_wisc', $term->termCode);
                }
            }
            $this->set_config('lastsync', $maxlastupdated);
        } catch (Exception $e) {
            error_log($this->errorlogtag.$e->getMessage());
        }
    }

    /**
     * Adds navigation links into course admin block.
     *
     * By defaults looks for manage links only.
     *
     * @param navigation_node $instancesnode
     * @param object $instance
     * @return void
     */
    public function add_course_navigation($instancesnode, stdClass $instance) {
        if ($instance->enrol !== 'wisc') {
            throw new coding_exception('invalid enrol instance!');
        }
        $context = context_course::instance($instance->courseid);
        if (has_capability('enrol/wisc:config', $context)) {
            $managelink = new moodle_url('/enrol/wisc/instance.php', array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $instancesnode->add($this->get_instance_name($instance), $managelink, navigation_node::TYPE_SETTING);
        }
    }

    /**
     * Returns edit icons for the page with list of instances
     * @param stdClass $instance
     * @return array
     */
    public function get_action_icons(stdClass $instance) {
        global $OUTPUT;

        if ($instance->enrol !== 'wisc') {
            throw new coding_exception('invalid enrol instance!');
        }
        $context = context_course::instance($instance->courseid);

        $icons = array();

        if (has_capability('enrol/wisc:config', $context)) {
            $editlink = new moodle_url("/enrol/wisc/instance.php", array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('i/edit', get_string('edit'), 'core', array('class'=>'icon')));
        }

        return $icons;
    }

    /**
     * Is it possible to delete enrol instance via standard UI?
     *
     * @param object $instance
     * @return bool
     */
    public function can_delete_instance($instance) {

        $context = context_course::instance($instance->courseid);
        if (has_capability('enrol/wisc:enrolconfig', $context)) {
            return true;
        }
        return false;
    }

    // For compatibility with moodle 2.6
    public function instance_deleteable($instance) {
        return $this->can_delete_instance($instance);
    }

    /**
     * Is it possible to hide/show enrol instance via standard UI?
     *
     * @param stdClass $instance
     * @return bool
     */
    public function can_hide_show_instance($instance) {
        return true;
    }

    /**
     * Returns link to page which may be used to add new instance of enrolment plugin in course.
     * @param int $courseid
     * @return moodle_url page url
     */
    public function get_newinstance_link($courseid) {
        $context = context_course::instance($courseid);

        if (!has_capability('enrol/wisc:enrolconfig', $context)) {
            return NULL;
        }

        // multiple instances supported - different term for different instances
        return new moodle_url('/enrol/wisc/instance.php', array('courseid'=>$courseid));
    }

    /**
     * Delete course enrol plugin instance, unenrol all users.
     * @param object $instance
     * @return void
     */
    public function delete_instance($instance) {
        global $DB;

        // remove our coursemap entries and groups
        $maps = $DB->get_records('enrol_wisc_coursemap', array('enrolid'=>$instance->id), '', 'id,groupid');
        foreach ($maps as $map) {
            if (!empty($maps->groupid)) {
                groups_delete_group($maps->groupid);
            }
        }
        if (!empty($instance->{enrol_wisc_plugin::groupingidfield})) {
            groups_delete_grouping($instance->{enrol_wisc_plugin::groupingidfield});
        }
        $DB->delete_records('enrol_wisc_coursemap', array('enrolid'=>$instance->id));
        $courseid = $instance->courseid;
        parent::delete_instance($instance);
    }

    /**
     * Restore instance and map settings.
     *
     * @param restore_enrolments_structure_step $step
     * @param stdClass $data
     * @param stdClass $course
     * @param int $oldid
     */
    public function restore_instance(restore_enrolments_structure_step $step, stdClass $data, $course, $oldid) {
        global $DB, $CFG;

        $valid = !empty($data->{self::termfield});
        if (!$valid) {
            $step->set_mapping('enrol', $oldid, 0);
            return;
        }
        if (!empty($data->{self::groupingidfield})) {
            // OK to map to null
            $data->{enrol_wisc_plugin::groupingidfield} = $step->get_mappingid('grouping', $data->{self::groupingidfield}, null);
        }
        if (!empty($data->{self::tarolefield})) {
            // OK to map to null
            $data->{self::tarolefield} = $step->get_mappingid('role', $data->{self::tarolefield}, null);
        }
        if (!empty($data->{self::auditorrolefield})) {
            // OK to map to null
            $data->{self::auditorrolefield} = $step->get_mappingid('role', $data->{self::auditorrolefield}, null);
        }

        // TODO: Do we need to validate coursemap and permissions?
        $instance = $DB->get_record('enrol', array(self::termfield=>$data->{self::termfield}, 'courseid'=>$course->id, 'enrol'=>$this->get_name()));
        if ($instance) {
            $instanceid = $instance->id;
        } else {
            $instanceid = $this->add_instance($course, (array)$data);
            $instance = $DB->get_record('enrol', array('id'=>$instanceid));
        }
        $step->set_mapping('enrol', $oldid, $instanceid);
    }

    /**
     * Restore user enrolment.
     *
     * @param restore_enrolments_structure_step $step
     * @param stdClass $data
     * @param stdClass $instance
     * @param int $oldinstancestatus
     * @param int $userid
     */
    public function restore_user_enrolment(restore_enrolments_structure_step $step, $data, $instance, $userid, $oldinstancestatus) {
        $this->enrol_user($instance, $userid, null, $data->timestart, $data->timeend, $data->status);
    }

    /**
     * Restore role assignment.
     *
     * @param stdClass $instance
     * @param int $roleid
     * @param int $userid
     * @param int $contextid
     */
    public function restore_role_assignment($instance, $roleid, $userid, $contextid) {
        role_assign($roleid, $userid, $contextid, 'enrol_'.$instance->enrol, $instance->id);
    }

    /**
     * Backup execution step hook to annotate grouping and role.
     *
     * @param backup_enrolments_execution_step $step
     * @param stdClass $enrol
     */
    public function  backup_annotate_custom_fields(backup_enrolments_execution_step $step, stdClass $enrol) {
        $step->annotate_id('grouping', $enrol->{self::groupingidfield});
        $step->annotate_id('role', $enrol->{self::tarolefield});
        $step->annotate_id('role', $enrol->{self::auditorrolefield});
    }

    /**
     * For compatibility with UW Moodle <= 2.6.  This is not necessary for 2.8, which has added
     * core support for the backup_annotate_custom_fields method.
     *
     * @param backup_enrolments_execution_step $step
     * @param stdClass $enrol
     */
    public function  backup_execution(backup_enrolments_execution_step $step, stdClass $enrol) {
        $this->backup_annotate_custom_fields($step, $enrol);
    }

    public function is_active_term($termCode) {
        // static request cache.
        static $cachedterms = null;

        if (is_null($cachedterms)) {
            $cachedterms = array();
            $datastore = $this->get_datastore();
            $activeterms = $datastore->getAvailableTerms();
            foreach ($activeterms as $term) {
                $cachedterms[$term->termCode] = true;
            }
        }
        return isset($cachedterms[$termCode]);
    }

    /**
     * Does this plugin allow manual unenrolment of a specific user?
     * Yes, but only if user suspended...
     *
     * @param stdClass $instance course enrol instance
     * @param stdClass $ue record from user_enrolments table
     *
     * @return bool - true means user with 'enrol/xxx:unenrol' may unenrol this user, false means nobody may touch this user enrolment
     */
    public function allow_unenrol_user(stdClass $instance, stdClass $ue) {
        if ($ue->status == ENROL_USER_SUSPENDED) {
            return true;
        }

        return false;
    }

    /**
     * Gets an array of the user enrolment actions
     *
     * @param course_enrolment_manager $manager
     * @param stdClass $ue A user enrolment object
     * @return array An array of user_enrolment_actions
     */
    public function get_user_enrolment_actions(course_enrolment_manager $manager, $ue) {
        $actions = array();
        $context = $manager->get_context();
        $instance = $ue->enrolmentinstance;
        $params = $manager->get_moodlepage()->url->params();
        $params['ue'] = $ue->id;
        if ($this->allow_unenrol_user($instance, $ue) && has_capability('enrol/wisc:unenrol', $context)) {
            $url = new moodle_url('/enrol/unenroluser.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/delete', ''), get_string('unenrol', 'enrol'), $url, array('class'=>'unenrollink', 'rel'=>$ue->id));
        }
        return $actions;
    }

    /**
     * Verify directly in CHUB whether the given pvi is enrolled as a student
     * in the given wisc enrollment instance.
     *
     * @param $instance enrol instance
     * @param $pvi user pvi
     * @return bool|null  boolean result or null if chub data is not available for the term.
     */
    public function is_student_enrolled_in_instance($instance, $pvi) {
        global $DB;
        if ($instance->enrol != 'wisc') {
            throw new coding_exception('enrol instance is not of type WISC');
        }
        $termCode = $instance->{self::termfield};

        $classnumbers = $DB->get_records_menu('enrol_wisc_coursemap',
                                              array('enrolid'=>$instance->id, 'deleted'=>0),
                                              '', 'id, class_number');
        if (empty($classnumbers)) {
            // No enrollments at all.
            return false;
        }

        // Query CHUB
        $datastore = $this->get_datastore();
        $uniqueids = $datastore->getEnrolledClassUniqueIds($termCode, $pvi);

        if (empty($uniqueids)) {
            // No chub enrollments.
            // Either the student isn't enrolled in any classes or we don't have data for the term,
            // or something else is wrong.  CHUB doesn't give us enough information to tell, but we can
            // at last check if the term is active.

            if (!$this->is_active_term($termCode)) {
                return null;
            }

            // Assume student isn't enrolled in anything.  Hopefully chub will generate a SOAP fault on error.
            return false;
        }

        foreach ($uniqueids as $uniqueid) {
            if (in_array($uniqueid->classNumber, $classnumbers)) {
                return true;  // Found enrollment.
            }
        }
        return false;  // Didn't find enrollment.
    }

} // end of class

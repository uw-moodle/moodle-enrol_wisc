<?php

/**
 * Cohort access settings for roster associations
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/enrol/wisc/lib.php');
require_once($CFG->dirroot.'/enrol/wisc/ra_datastore.class.php');
require_once($CFG->dirroot.'/enrol/wisc/updatelib.php');
require_once($CFG->dirroot.'/enrol/wisc/edit_forms.php');

$action = optional_param('action', 'create', PARAM_ALPHA);
$cohortid = optional_param('cohortid', 0, PARAM_INT);
$PAGE->set_course($SITE);

require_login();
$context = context_system::instance();
require_capability('moodle/site:config', $context);

$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_heading(get_string('subjectaccess', 'enrol_wisc'));
$PAGE->set_title(get_string('subjectaccess', 'enrol_wisc'));
$PAGE->set_url('/enrol/wisc/cohort.php');
$PAGE->navbar->add(get_string('editcohort', 'enrol_wisc'));

$cohortlisturl = new moodle_url("/" . $CFG->admin . "/settings.php", array('section' => 'enrolwisccohorts'));

if ($action == 'delete') {
    require_sesskey();
    if ($cohortid > 0) {
        $DB->delete_records('enrol_wisc_access', array('cohortid'=>$cohortid, 'component'=>null));
    } else {
        $DB->delete_records('enrol_wisc_access', array('cohortid'=>null, 'component'=>null));
    }
    redirect($cohortlisturl);
} else if ($action == 'create') {
    // new cohort
    $cohort = null;
    $dbitems = array();
} else if ($cohortid > 0) {
    // existing cohort
    $cohort = $DB->get_record('cohort', array('id'=>$cohortid), 'id,name', MUST_EXIST);
    $dbitems = $DB->get_records('enrol_wisc_access', array('cohortid'=>$cohortid, 'component'=>null), '', 'id,item,type,permission');
} else {
    // special 'cohort' representing all users
    $cohort = new StdClass();
    $cohort->id = 0;
    $cohort->name = get_string('allusers', 'enrol_wisc');
    $dbitems = $DB->get_records_select('enrol_wisc_access', 'cohortid IS NULL AND component IS NULL', array(), '', 'id,item,type,permission');
}

$radatastore = new wisc_ra_datastore();
list ($allsubjects, $allorgs) = $radatastore->getAllSubjectsAndOrgs();

$mform = new enrol_wisc_cohort_form(null, array('cohort'=>$cohort, 'action'=>$action, 'subjects'=>$allsubjects, 'orgs'=>$allorgs));

$defaultdata = array();
foreach ($dbitems as $rec) {
    if ($rec->type == enrol_wisc_plugin::ACCESS_SUBJECT) {
        $defaultdata['subject_'.$rec->item] = $rec->permission;
    } else if ($rec->type == enrol_wisc_plugin::ACCESS_ORG) {
        $defaultdata['org_'.$rec->item] = $rec->permission;
    }
}
$mform->set_data($defaultdata);

if ($mform->is_cancelled()) {
    redirect($cohortlisturl);

} else if ($data = $mform->get_data()) { // no magic quotes
    $existingids = array_keys($dbitems);
    foreach ((array)$data as $key=>$value) {
        if (empty($value)) {
            continue;
        }
        $matches = array();
        if (preg_match('/^subject_(.*)/', $key, $matches)) {
            $type = enrol_wisc_plugin::ACCESS_SUBJECT;
            $item = $matches[1];
            $permission = $value;
        } else if (preg_match('/^org_(.*)/', $key, $matches)) {
            $type = enrol_wisc_plugin::ACCESS_ORG;
            $item = $matches[1];
            $permission = $value;
        } else {
            continue;
        }
        if (!($permission == enrol_wisc_plugin::CREATECOURSE_LIMITED || $permission == enrol_wisc_plugin::CREATECOURSE_FULL)) {
            throw new coding_exception('Unknown value for enrol_wisc_access.permission');
        }
        // for simplicity we just update all records, rather than trying to figure out what changed
        $rec = new stdClass();
        $rec->id   = array_pop($existingids);
        $rec->type = $type;
        $rec->item = $item;
        $rec->permission = $permission;
        $rec->cohortid = ($cohortid == 0)? null : $cohortid;
        if (!is_null($rec->id)) {
            $DB->update_record('enrol_wisc_access', $rec);
        } else {
            $DB->insert_record('enrol_wisc_access', $rec);
        }
    }
    if (!empty($existingids)) {
        $DB->delete_records_list('enrol_wisc_access', 'id', $existingids);
        unset($dbrecords);
    }
    redirect($cohortlisturl);
    exit;
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('cohortpermissions', 'enrol_wisc'));

$mform->display();
echo $OUTPUT->footer();


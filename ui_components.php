<?php

/**
 * Roster mapping UI components
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/ra_datastore.class.php');

abstract class wisc_course_mapper_base implements renderable {
    protected $term;
    protected $pvi;
    protected $urlparams;

    protected $potential;
    protected $current;

    public function __construct($params) {
        global $USER;
        $this->term = $params['term'];
        $this->urlparams = $params['urlparams']; // params that need to be added to the PAGE url

        // create course selectors
        // FIXME  verify account is netid based first
        $params['pvi'] = $USER->idnumber;
        $params['allowdups'] = get_config('enrol_wisc', 'allowdups');
        $this->init_current_selector($params);
        $this->potential = new wisc_course_selector_potential('addselect', $this->current, $params);
    }

    public function process() {
        // Process addcourse button and callback
        if (optional_param('addcourse', false, PARAM_BOOL)) {
            $this->process_addcourse();
        }
        // Process add button
        if (optional_param('add', false, PARAM_BOOL)) {
            require_sesskey();
            $this->process_add();
        }
        // Process remove button
        if (optional_param('remove', false, PARAM_BOOL)) {
            require_sesskey();
            $this->process_remove();
        }
    }

    public function process_add() {
        $classestoadd = $this->potential->get_selected_classes();
        $this->current->add_classes($classestoadd);
        $this->potential->remove_classes($classestoadd);
    }

    public function process_remove() {
        $classestoremove = $this->current->get_selected_classes();
        // Must remove and then add, since potential picker filters based on current picker content.
        $this->current->remove_classes($classestoremove);
        $this->potential->add_classes($classestoremove);
    }

    public function process_addcourse() {
        global $PAGE;
        $courseid = optional_param('newcourseid', false, PARAM_ALPHANUM);
        $subject = optional_param('newsubject', false, PARAM_ALPHANUM);

        if ($courseid && $subject) {
            $this->potential->add_course($courseid, $subject);
        } else {
            // We probably shouldn't pass $urlparams as GET parameters, but doing it this way
            // we keep the coursepicker code separate from the mapper component.
            // The YUI course picker will try to preempt this call anyhow.
            $params = array('term'     => $this->term,
                            'return'   => $PAGE->url->out(false, $this->urlparams));
            redirect(new moodle_url('/enrol/wisc/coursepicker.php', $params));
        }
    }

    public function initialize_js($page) {
        $modules = array('moodle-enrol_wisc-coursepicker', 'moodle-enrol_wisc-coursepicker-skin');
        $function = 'M.enrol_wisc.init_coursepicker';
        $arguments = array(
            'term'=>$this->term,
            'submiturl'=>$page->url->out(false, $this->urlparams));
        $page->requires->yui_module($modules, $function, array($arguments));
    }

    public function get_mstate() {
        $mstate = new stdClass();
        $mstate->pot = $this->potential->get_classes();
        return $mstate;
    }

/*
    public function set_mstate($mstate) {
        if (is_object($mstate)) {
            $this->potential->set_classes($mstate->pot);
        }
    }
*/

    public function get_classes() {
        return $this->current->get_classes();
    }

    public function set_classes($classes) {
        $this->current->set_classes($classes);
    }

    public function get_potential_selector() {
        return $this->potential;
    }

    public function get_current_selector() {
        return $this->current;
    }

    // get term
    public function get_term() {
        return $this->term;
    }

    public function can_approve_mappings() {
        // TODO: we need to check at the category level
        global $USER;
        return  has_capability('enrol/wisc:anycourse', context_system::instance())
            || wisc_can_create_course($USER) === enrol_wisc_plugin::CREATECOURSE_FULL;
    }

    // approve classes
    public function approve_all_classes() {
        $this->current->approve_classes();
        $this->potential->approve_classes();
    }

/// Protected API

    abstract protected function init_current_selector($options);

}

class wisc_course_mapper_create extends wisc_course_mapper_base {

    protected function init_current_selector($options) {
        // create potential course selector
        $options['term'] = $this->term;

        $this->current = new wisc_course_selector_new('addselect', $options);
        $this->set_classes($options['currentclasses']);
    }

}

class wisc_course_selector_base implements renderable {
    protected $term;
    protected $classes;
    protected $selectedcourseId;
    protected $name;

    /**
     * @param string $name control name
     * @param array $options
     */
    public function __construct($name, $options) {
        $this->name = $name;
        $this->term = $options['term'];

        // load the initial list of classes
        $this->set_classes($this->find_classes());
    }

    // add new classes
    public function add_classes($classes) {
        $this->set_classes(array_merge($this->get_classes(), $classes));
    }

    // remove classes
    public function remove_classes($classes) {
        $this->set_classes(wisc_ra_datastore::helper_diff_classes($this->get_classes(), $classes));
    }

    // approve classes
    public function approve_classes() {
        foreach ($this->classes as $class) {
            $class->official = true;
        }
    }

    // set list of classes
    public function set_classes($classes) {
        if (empty($classes)) {
            $classes = array();
        }
        $this->classes = $this->helper_check_classes($classes);
    }

    // get list of all classes
    public function get_classes() {
        return $this->classes;
    }

    // get widget name
    public function get_name() {
        return $this->name;
    }

    // get classes that were selected by the user
    public function get_selected_classes() {
        $selected = optional_param_array($this->name, array(), PARAM_ALPHANUMEXT);
        $classes = array();
        foreach ($this->classes as $class) {
            $key = $this->get_group_key($class);
            if (in_array($key, $selected, true)) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    // add an entire timetable course.
    public function add_course($courseId, $subjectCode) {
        // Any exception here can be fatal
        $classes = $this->helper_get_classes_in_course($courseId, $subjectCode);
        if (!$this->helper_audit_course_before_add($courseId, $classes)) {
            throw new moodle_exception('No permission to add course');
        }
        $this->add_classes($classes);
        $this->selectedcourseId = $courseId; // so that we select it by default

        // Add MeetsWith courses too.
        $othercourses = $this->helper_get_meets_with_courses($courseId, $classes);
        foreach ($othercourses as $relatedcourse) {
            $relatedclasses = $this->helper_get_classes_in_course(
                                        $relatedcourse->courseId,
                                        reset($relatedcourse->subjects));

            if ($this->helper_audit_course_before_add($relatedcourse->courseId, $relatedclasses)) {
                $this->add_classes($relatedclasses);
            } else {
                // Ignore meetswith course if there are permission errors.
            }
        }



    }

    // group classes into courses for display
    public function get_grouped_classes($classes = null) {
        if ($classes === null) {
            $classes = $this->get_classes();
        }
        $groups = array();
        $courses = array();
        foreach ($classes as $class) {
            $courses['S'.$class->courseId]['S'.$class->sectionNumber][$class->sessionCode][] = $class;
        }
        foreach ($courses as $scourseId => $sections) {
            $courseId = substr($scourseId, 1);
            $asection = reset($sections);
            $asession = reset($asection);
            $acourse = reset($asession);
            $course = $this->helper_get_course_object($this->term, $acourse->subjectCode, $courseId, $sections);
            $groupname = sprintf('%s %s %s', $course->subjectstr, $course->catalogNumber, $course->title);

            $items = array();
            foreach ($sections as $ssectionNumber => $sessions) {
                $sectionNumber = substr($ssectionNumber, 1);
                $multisession = count($sessions) > 1;
                foreach ($sessions as $sessionCode => $classesinsection) {
                    $subjects = array();
                    $classNumbers = array();
                    foreach ($classesinsection as $class) {
                        $subjects[] = $class->subjectCode;
                        $classNumbers[] = $class->classNumber;
                    }
                    $aclass = reset($classesinsection);
                    if (!count(array_diff($course->subjects, $subjects))) {
                        $subjectstr = "";
                    } else {
                        // Some crosslists are missing
                        $subjectstrs = array();
                        foreach ($classesinsection as $class) {
                            $subjectstrs[] = $class->subjectShortDescription;
                        }
                        sort($subjectstrs);
                        $subjectstr = join('/', $subjectstrs);
                        $subjectstr = preg_replace('/ /', '', $subjectstr);
                        $subjectstr = "(only $subjectstr)";
                    }
                    if ($multisession) {
                        $sessionstr = ' ('.$sessionCode.')';
                    } else {
                        $sessionstr = '';
                    }
                    $itemname = sprintf("%s %s%s %s %s&#160;&#160;&#160;%s", $aclass->type, $sectionNumber,
                                                          $sessionstr, $subjectstr, $aclass->topic, $aclass->instructor);
                    $item = new stdClass();
                    $item->name = $itemname;
                    $item->id = $this->get_group_key($aclass);
                    $item->sort = $item->id;
                    $item->new = $courseId == $this->selectedcourseId;
                    $this->process_section($item,$classesinsection);
                    $groups[$groupname][] = $item;
                }
            }
        }
        return $groups;
    }


//////  Private API

    // Find classes that should be in the picker initially
    protected function find_classes() {
        return array();
    }

    // Returns class numbers that should be excluded from the picker
    protected function get_excluded_classnumbers() {
        return array();
    }

    // Do processing on each item just before display
    protected function process_section($item, $classes) {
    }

/// Helper functions

    protected function helper_get_classes_in_course($courseId, $subjectCode) {
        $allclasses = array();
        $radatastore = wisc_ra_datastore::get();
        $course = $radatastore->getCourseByCourseId($this->term, $subjectCode, $courseId);
        if ($course) {
            foreach ($course->subjects as $subject) {
                $classes = $radatastore->getClassesByCatalogNumber($this->term, $subject, $course->catalogNumber);
                $allclasses = array_merge($allclasses, $classes);
            }
        }
        return $allclasses;
    }

    protected function helper_get_meets_with_courses($courseid, $classes) {
        $radatastore = wisc_ra_datastore::get();
        $courseids = array();
        $classnumbersdone = array();
        foreach ($classes as $class) {
            if ($class->hasMeetsWith && !isset($classnumbersdone[$class->classNumber])) {
                $relatedclasses = $radatastore->getCrossListedClasses($this->term, $class->classNumber);
                foreach ($relatedclasses as $rclass) {
                    // Meets_with and crosslists are transitive, so no need to call getCrossListedClasses on related classes.
                    $classnumbersdone[$rclass->classNumber] = true;
                    $newcourse = empty($courseids[$rclass->courseId]) && $rclass->courseId != $courseid;
                    if ($newcourse) {
                        $courseids[$rclass->courseId] = $radatastore->getCourseByCourseId(
                                                                $this->term,
                                                                $rclass->subjectCode,
                                                                $rclass->courseId);
                    }
                }
            }
        }
        return array_values($courseids);
    }

    protected function helper_audit_course_before_add($courseId, $classes) {
        $capability = $this->helper_can_add_course($courseId, $classes);
        if ($capability == enrol_wisc_plugin::CREATECOURSE_FULL) {
            // mark every section as official
            foreach ($classes as $class) {
                $class->official = true;
            }
        } else if ($capability == enrol_wisc_plugin::CREATECOURSE_LIMITED) {
            // leave sections unofficial
        } else {
            // no permission.
            return false;
        }
        return true;
    }

    // See what permissions the user has to add these classes.
    protected function helper_can_add_course($courseId, $classes) {
        global $USER;
        global $DB;

        $syscontext = context_system::instance();

        // Verify that the subject is allowed
        $limitedperm = false;          // cohort permissions
        $fullperm = false;
        $limitedpermbyuwrole = false;  // by uw role permissions
        $fullpermbyuwrole = false;

        if (has_capability('enrol/wisc:anysubject', $syscontext)) {
            $fullperm = true;
        } else {
            $subjects = array();
            $orgs = array();
            foreach ($classes as $class) {
                if ($class->courseId != $courseId) {
                    throw new moodle_exception('Programming error: courseId doesn\'t match');
                }
                $subjects[] = $class->subjectCode;
                $orgs[] = $class->academicOrgCode;
            }
            $subjects = array_unique($subjects);
            $orgs = array_unique($orgs);

            // check access table for access to any of the subjects
            list ($subjectsql, $subjectparams) = $DB->get_in_or_equal($subjects, SQL_PARAMS_NAMED);
            list ($orgsql, $orgparams) = $DB->get_in_or_equal($orgs, SQL_PARAMS_NAMED);
            $sql = "SELECT a.id, a.permission, a.cohortid
                      FROM {enrol_wisc_access} a
                 LEFT JOIN {cohort} c ON c.id = a.cohortid AND c.contextid = :sitecontext
                 LEFT JOIN {cohort_members} cm ON cm.cohortid = c.id
                     WHERE (a.cohortid IS NULL OR cm.userid = :userid)
                       AND a.permission > 0
                       AND ((a.type = :orgtype AND a.item $orgsql) OR (a.type = :subjecttype AND a.item $subjectsql))";
            $params = array();
            $params['sitecontext'] = $syscontext->id;
            $params['userid'] = $USER->id;
            $params['orgtype'] = enrol_wisc_plugin::ACCESS_ORG;
            $params['subjecttype'] = enrol_wisc_plugin::ACCESS_SUBJECT;
            $params = array_merge($params, $subjectparams, $orgparams);
            $allows = $DB->get_records_sql($sql, $params);
            foreach ($allows as $allow) {
                if (empty($allow->cohortid)) {
                    // By UW Role cohort
                    if ($allow->permission == enrol_wisc_plugin::CREATECOURSE_FULL) {
                        $fullpermbyuwrole = true;
                    } else if ($allow->permission == enrol_wisc_plugin::CREATECOURSE_LIMITED) {
                        $limitedpermbyuwrole = true;
                    }
                } else {
                    // Any other cohort
                    if ($allow->permission == enrol_wisc_plugin::CREATECOURSE_FULL) {
                        $fullperm = true;
                    } else if ($allow->permission == enrol_wisc_plugin::CREATECOURSE_LIMITED) {
                        $limitedperm = true;
                    }
                }
            }
        }
        // impose restrictions on cabability by uw role
        if (has_capability('enrol/wisc:createcoursebyuwrole', $syscontext)) {
            $uwrolecapability = wisc_uwroles_to_capability($USER);
            if ($uwrolecapability == enrol_wisc_plugin::CREATECOURSE_LIMITED) {
                $limitedpermbyuwrole = $limitedpermbyuwrole || $fullpermbyuwrole;
                $fullpermbyuwrole = false;
            } else if ($uwrolecapability == enrol_wisc_plugin::CREATECOURSE_NONE) {
                $fullpermbyuwrole = false;
                $limitedpermbyuwrole = false;
            }
        } else {
            $fullpermbyuwrole = false;
            $limitedpermbyuwrole = false;
        }
        $limitedperm = $limitedperm || $limitedpermbyuwrole;
        $fullperm = $fullperm || $fullpermbyuwrole;
        $subjectallowed = $fullperm || $limitedperm;

        if ($subjectallowed && has_capability('enrol/wisc:anycourse', $syscontext)) {
            // system level access to all courses, but restricted to allowed subjects
            $fullperm = true;
        }

        if ($fullperm) {
            $result = enrol_wisc_plugin::CREATECOURSE_FULL;
        } else if ($limitedperm) {
            $result = enrol_wisc_plugin::CREATECOURSE_LIMITED;
        } else {
            $result = enrol_wisc_plugin::CREATECOURSE_NONE;
        }
        return $result;
    }

    protected function helper_get_course_object($term, $subjectCode, $courseId, $sections) {
        try {
            $radatastore = wisc_ra_datastore::get();
            $course = $radatastore->getCourseByCourseId($term, $subjectCode, $courseId);
        } catch (Exception $e) {
            // lookup failed, so use what we can from the coursemap data
            $subjectstrs = array();
            $subjects = array();
            $catalogNumber = '';
            foreach ($sections as $sessions) {
                foreach ($sessions as $classes) {
                    foreach ($classes as $class) {
                        $subjectstrs[] = $class->subjectShortDescription;
                        $subjects[] = $class->subjectCode;
                        $catalogNumber = $class->catalogNumber;
                    }
                }
            }
            $subjectstrs = array_unique($subjectstrs);
            $subjects = array_unique($subjects);
            sort($subjectstrs);
            $subjectstr = join('/',$subjectstrs);
            $subjectstr = preg_replace('/ /', '', $subjectstr);
            $course = new stdClass();
            $course->catalogNumber = $catalogNumber;
            $course->title = '';
            $course->subjectstr = $subjectstr;
            $course->subjects = $subjects;
            $course->termCode = $this->term;
        }
        return $course;
    }

    protected function helper_check_classes($allclasses) {
        // remove excluded and duplicate classes
        $excludes = $this->get_excluded_classnumbers();
        $classes = array();
        foreach ($allclasses as $class) {
            if ($class->termCode != $this->term) {
                continue;
            }
            if (!in_array($class->classNumber, $excludes)) {
                $classes[$class->classNumber] = $class;
            }
        }
        return array_values($classes);
    }

    protected function get_group_key($class) {
        return $class->termCode.'_'.$class->courseId.'_'.$class->sectionNumber.'_'.$class->sessionCode;
    }

}

// Selector listing potential associations
class wisc_course_selector_potential extends wisc_course_selector_base {

    protected $pvi;
    protected $allowdups;
    protected $current;
    protected $mstate;

    public function __construct($name, wisc_course_selector_base $current, $options) {
        $this->current = $current;
        if (isset($options['pvi'])) {
            $this->pvi = $options['pvi'];
        }
        if (isset($options['allowdups'])) {
            $this->allowdups = $options['allowdups'];
        }
        if (is_object($options['mstate'])) {
            $this->mstate = $options['mstate'];
        }
        parent::__construct($name, $options);
    }

    protected function find_classes() {
        if ($this->mstate) {
            return $this->mstate->pot;
        } else {
            return $this->helper_get_instructor_classes();
        }
    }

    protected function get_excluded_classnumbers() {
        global $DB;

        $classes = $this->current->get_classes();
        $classnumbers = array();
        foreach ($classes as $class) {
            $classnumbers[] = $class->classNumber;
        }
        return $classnumbers;
    }

    protected function process_section($item, $classes) {
        global $DB;
        $classnumbers = array();
        foreach ($classes as $class) {
            $classnumbers[] = $class->classNumber;
        }
        $params = array('term'=>$this->term, 'deleted'=>0);
        list($insql, $inparams) = $DB->get_in_or_equal($classnumbers, SQL_PARAMS_NAMED, 'classno', true);
        $params = array_merge($params, $inparams);
        $select = "term = :term AND class_number $insql";
        $instanceid = $this->current->get_instanceid();
        if ($instanceid) {
            $select .= " AND enrolid != :enrolid";
            $params['enrolid'] = $instanceid;
        }
        if ($DB->record_exists_select('enrol_wisc_coursemap', $select, $params)) {
            $item->unavailable = true;
            $item->disabled = !$this->allowdups;
        }
    }

    protected function helper_get_instructor_classes() {
        if (empty($this->pvi)) {
            return array();   // no pvi for current user
        }
        $radatastore = $radatastore = wisc_ra_datastore::get();

        // look up the instructor's classes
        try {
            $classes = $radatastore->getClassesByInstructor($this->term, $this->pvi);
        } catch (Exception $e) {
            error_log($e->getMessage());
            $classes = array();
        }

        // Fetch all sections in all courses the instructor is teaching

        $courses = array();
        foreach ($classes as $class) {
            $courses['S'.$class->courseId][] = $class;
        }

        $allclasses = array();
        $completed = array();
        foreach ($courses as $scourseId=>$classes) {
            // loop over all subjectCode/catalogNumber pairs
            // and call getClassesByCatalogNumber()
            foreach ($classes as $class) {
                if (!empty($completed[$class->classNumber])) {
                    continue;
                }
                if ($class->type == "IND") {
                    // don't fetch all sections in "IND" classes
                    $result = array($class);
                } else {
                    try {
                        $result = $radatastore->getClassesByCatalogNumber($this->term,
                                                                          $class->subjectCode,
                                                                          $class->catalogNumber);
                    } catch (Exception $e) {
                        error_log($this->errorlogtag.$e->getMessage());
                        $result = array();
                    }
                }

                $allclasses = array_merge($allclasses, $result);
                foreach ($result as $class) {
                    $completed[$class->classNumber] = true;
                }

            }
        }
        // mark classes that the user is officially teaching
        foreach ($allclasses as $class) {
            $class->official = true;
        }
        return $allclasses;
    }
}

// Selector listing new associations for coursecreator
class wisc_course_selector_new extends wisc_course_selector_base {

    protected $instanceid; // set when editing an enrol instance.

    public function __construct($name, $options) {
        parent::__construct($name, $options);

        if (isset($options['instanceid'])) {
            $this->instanceid = $options['instanceid'];
        }
    }

    public function get_grouped_adds_and_removes() {
        global $DB;
        $changes = new stdClass();

        $radatastore = wisc_ra_datastore::get();
        $classes = $this->classes;
        if (!$this->instanceid) {
            // New instance / class
            $changes->adds = $classes;
            $changes->removes = array();
            $changes->approves = array();
        } else {
            // Existing instance
            $dbclasses = array();
            $maps = $DB->get_records('enrol_wisc_coursemap', array('enrolid'=>$this->instanceid, 'deleted'=>0));
            foreach ($maps as $map) {
                $dbclasses[] = $radatastore->getClassFromCoursemap($map);
            }
            $adds = wisc_ra_datastore::helper_diff_classes($classes, $dbclasses);
            $removes = wisc_ra_datastore::helper_diff_classes($dbclasses, $classes);
            $approves = wisc_ra_datastore::helper_get_approved_classes($classes, $dbclasses);

            $changes->adds = $this->get_grouped_classes($adds);
            $changes->removes = $this->get_grouped_classes($removes);
            $changes->approves = $this->get_grouped_classes($approves);
        }
        return $changes;
    }

    public function get_instanceid() {
        return $this->instanceid;
    }

    protected function process_section($item, $classes) {
        // Mark pending sections
        $ispending = false;
        foreach($classes as $class) {
            $ispending = $ispending || empty($class->official);
        }
        $item->pending = $ispending;
    }

}

class wisc_course_subject_selector {
    protected $term;

    public function __construct($term) {
       $this->term = $term;
    }

    protected function load_subjects() {
       try {
           $radatastore = wisc_ra_datastore::get();
           $subjects = $radatastore->getSubjectsMenu($this->term);
       } catch (Exception $e) {
           error_log($e->getMessage());
           $subjects = array();
       }
       asort($subjects);
       return $subjects;
    }

    protected function load_subject_courses($subject) {
       try {
           $radatastore = wisc_ra_datastore::get();
           $courses = $radatastore->getSubjectCoursesMenu($this->term, $subject);
           asort($courses);
       } catch (Exception $e) {
           error_log($e->getMessage());
           $courses = array();
       }
       asort($courses);
       return $courses;
    }
}


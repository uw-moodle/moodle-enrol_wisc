<?php

/**
 * Course creator UI stage class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/edit_forms.php');

abstract class enrol_wisc_stage_base {

    protected $ui;
    protected $cc;
    protected $params;

    public function __construct(enrol_wisc_create_ui $ui, array $params=null) {
        $this->ui = $ui;
        $this->cc = $ui->get_controller();
        $this->params = $params;
        if (!is_array($this->params)) {
            $this->params = array();
        }
        $this->params['createid'] = $ui->get_createid();
    }

    public function get_params() {
        return $this->params;
    }

    public function initialize_js($page) {
        enrol_wisc_create_ui_helper::initialize_confirm_js($page, 'confirmcancel', '.confirmcancel');
    }

    public function process() {
        if ($this->skip_stage()) {
            return 0;
        }
        $this->init_stage();
        return $this->process_stage();
    }

    public function skip_stage() {
        return false;
    }

    protected function init_stage() {
    }

    abstract protected function process_stage();
    abstract public function display();
}

class enrol_wisc_stage_term extends enrol_wisc_stage_base {
    protected $mform;

    protected function init_stage() {
        $this->mform = new enrol_wisc_term_form($this->ui, $this->get_params());
        $this->mform->set_data(array('term'=>$this->cc->get_term()));
    }

    public function process_stage() {
        $mform = $this->mform;
        if ($mform->is_cancelled()) {
            $this->ui->cancel_process();
        }
        $data = $mform->get_data();
        if ($data) {
            require_sesskey();
            $changes = 0;
            $cc = $this->cc;
            if ($cc->get_term($data->term) != $data->term) {
                $cc->set_term($data->term);

                // changing term resets state
                $cc->set_classes(array());
                $cc->set_course_mapper_state(null);
                $cc->set_settings(null);
                $cc->set_configured(false);
                $changes++;
            }
            // Redirect to another site if necessary, based on term selection
            $this->check_for_redirect();

            // Return the number of changes the user made
            return $changes;
        } else {
            return false;
        }
    }

    public function initialize_js($page) {
        // no need for confirm on cancel
    }

    public function display() {
        global $PAGE;
        $renderer = $PAGE->get_renderer('enrol_wisc');
        echo $renderer->render_creatorintro();
        $this->mform->display();
    }

    // Protected API

    /**
     * Redirect to a different site for term, if necessary
     *
     * This function calls a function "enrol_wisc_course_creation_redirect_hook($termcode)"
     * which can be defined in config.php.  See comments below this function.
     *
     * @return moodle_url | false
     */
    protected function check_for_redirect() {
        if (!function_exists('enrol_wisc_course_creation_redirect_hook')) {
            return;
        }
        $term = $this->cc->get_term();
        list ($url, $message) = enrol_wisc_course_creation_redirect_hook($term);

        if ($url) {
            // We're redirecting to another site.  See ya!
            if (!is_object($url)) {
                $url = new moodle_url($url);
            }
            $url->param('term', $term);
            notice($message, $url);
        }

    }

//   /**
//    * Get url to redirect to based on term selection.  Define this in config.php as needed.
//    *
//    * The return value should be false for no redirection, or an array of the form
//    * array(string|moodle_url $url, string $message)
//    *
//    * @param string $termcode
//    * @return false | array
//    */
//    function enrol_wisc_course_creation_redirect_hook($termcode) {
//        if ($termcode >= 1152) {
//            return array('https://ay14-15.moodle.wisc.edu/prod/enrol/wisc/create.php',
//                         'Academic year 2014-2015 classes are hosted on a new version of moodle.');
//        }
//        return false;
//    }


}

class enrol_wisc_stage_mapping extends enrol_wisc_stage_base {
    protected $mapper;

    protected function init_stage() {
        $params = array('term'       =>$this->cc->get_term(),
                        'urlparams'  =>$this->get_params(),
                        'instanceid' =>$this->cc->get_instanceid(),
                        'mstate'     =>$this->cc->get_course_mapper_state(),
                        'currentclasses' => $this->cc->get_classes(),
        );
        $this->mapper = new wisc_course_mapper_create($params);
    }

    public function skip_stage() {
        if (!$this->cc->get_term()) {
            return true;
        }
        return false;
    }

    public function process_stage() {
        $form_check = optional_param('_wisc_mapper_form', false, PARAM_BOOL)
                   || optional_param('addcourse', false, PARAM_BOOL);
        if (!$form_check) {
            return false;
        }

        if (optional_param('cancel', false, PARAM_BOOL)) {
            $this->ui->cancel_process();
        }
        // process approve all button
        $canapprove = $this->mapper->can_approve_mappings() && $this->cc->get_controllermode() == enrol_wisc_controller::TYPE_EDIT;
        if (optional_param('approve', false, PARAM_BOOL) && $canapprove) {
            require_sesskey();

            // approve classes in the UI
            $this->mapper->approve_all_classes();
        }

        // process mapper form
        $outcome = $this->mapper->process();

        // save mapper state
        $mstate = $this->mapper->get_mstate();
        $this->cc->set_course_mapper_state($mstate);
        $classes = $this->mapper->get_classes();

        $oldclasses = $this->cc->get_classes();
        $oldclassnumbers = array();
        foreach ($oldclasses as $class) {
            $oldclassnumbers[] = $class->classNumber;
        }
        $newclassnumbers = array();
        foreach ($classes as $class) {
            $newclassnumbers[] = $class->classNumber;
        }
        $adds = array_diff($newclassnumbers, $oldclassnumbers);
        $deletes = array_diff($oldclassnumbers, $newclassnumbers);
        $changes = count($adds) + count($deletes);

        if ($changes) {
            // reset settings
            $this->cc->set_settings(null);
            $this->cc->set_configured(false);
        }

        $this->cc->set_classes($classes);

        // check if done with stage
        $submitted = optional_param('next', false, PARAM_ALPHA);
        if ($submitted) {
            require_sesskey();
            return $changes;
        }
        return false;
    }

    public function display() {
        global $PAGE;
        $renderer = $PAGE->get_renderer('enrol_wisc');
        if ($this->cc->get_controllermode() == enrol_wisc_controller::TYPE_EDIT) {
            $previous = false;
        } else {
            $previous = true;
        }
        echo $renderer->render_wisc_course_mapper_base($this->mapper, $PAGE->url, $this->get_params(), $previous);
    }

    public function initialize_js($page) {
        // initialize mapper js
        $this->mapper->initialize_js($page);

        $classes = $this->cc->get_classes();
        if (empty($classes)) {
            enrol_wisc_create_ui_helper::initialize_confirm_js($page, 'confirmemptymapping', 'input.nextbutton');
        }
        parent::initialize_js($page);
    }

    protected function helper_classes_are_equal($classes1, $classes2) {
        if (count($classes1) != count($classes2)) {
            return false;
        }
        $ids1 = array();
        $ids2 = array();
        foreach ($classes1 as $class) {
            $ids1[] = $class->termCode.'_'.$class->classNumber;
        }
        foreach ($classes2 as $class) {
            $ids2[] = $class->termCode.'_'.$class->classNumber;
        }
        if (count(array_diff($ids1, $ids2)) == 0) {
            return true;
        } else {
            return false;
        }
    }
}

class enrol_wisc_stage_import extends enrol_wisc_stage_base {

    protected $archvieid;
    protected $search;
    protected $state;
    protected $skipstage = false;

    protected function init_stage() {
        global $PAGE;
        $this->archiveid = $this->cc->get_archiveid();

        // Skip archive stage if local/archive plugin is missing
        if (!class_exists('archive_course_search')) {
            $this->skipstage = true;
            return;
        }
        if (optional_param('cancel', false, PARAM_BOOL)) {
            $this->ui->cancel_process();
        }

        // initialize state
        $state = $this->cc->get_import_state();
        if (!isset($state->searchstate)) {
            $state->searchstate = new stdClass();
        }
        if (!isset($state->wantimport)) {
            $state->wantimport = null;
        }
        $this->state = $state;
        $params = $this->get_params();
        $params['_wisc_import_form'] = 1;
        $params['sesskey'] = sesskey();
        $this->search = new archive_course_search(array('url'=>$PAGE->url, 'pageparams'=>$params, 'state'=>$state->searchstate));
    }

    public function skip_stage() {
        if ($this->cc->get_controllermode() != enrol_wisc_controller::TYPE_COURSE) {
            return true;
        }
        return false;
    }

    public function process_stage() {
        if ($this->skipstage) {
            return 0;
        }

        // see if user wants to import
        $state = $this->state;
        $form_check_choice = optional_param('_wisc_import_choice_form', false, PARAM_BOOL);
        $form_check_import = optional_param('_wisc_import_form', false, PARAM_BOOL);
        if (!$form_check_choice && !$form_check_import) {
            if ($this->cc->get_archiveid()) {
                $state->wantimport = true;
            } else {
                $state->wantimport = null; // clear previous choice
            }
        }
        if (!$form_check_import && is_null($state->wantimport)) {
            if (!$form_check_choice) {
                return false; // need to diplay form
            }
            if (optional_param('next', false, PARAM_ALPHA)) {
                $state->wantimport = true;
                $this->cc->set_import_state($state);
            }
            if (optional_param('skipstage', false, PARAM_ALPHA)) {
                $state->wantimport = false;
                $this->cc->set_import_state($state);
            }
        }
        if ($state->wantimport === false) {
            // we're skipping import
            $this->cc->set_archiveid(null);
            return 0;
        }

        // process archive import
        if (!$form_check_import) {
            return false;
        }
        $archiveid = optional_param('importid', false, PARAM_INT);
        $submitted = optional_param('next', false, PARAM_ALPHA);
        if (!$submitted) {
            $archiveid = false;
        }
        $skipstage = optional_param('skipstage', false, PARAM_ALPHA);
        if ($skipstage) {
            $state->wantimport = false;
            $this->cc->set_import_state($state);
            $archiveid = false;
        }

        // save search state
        $state->searchstate = $this->search->get_state();
        $this->cc->set_import_state($state);
        require_sesskey();
        $changes = 0;
        if ($archiveid !== $this->archiveid) {
            $this->cc->set_archiveid($archiveid);
            $changes++;
        }

        if ($archiveid || $skipstage) {
            return $changes;
        }
        return false;
    }

    public function display() {
        global $PAGE;
        $renderer = $PAGE->get_renderer('enrol_wisc');
        if (empty($this->state->wantimport)) {
            echo $renderer->render_import_choice($PAGE->url, $this->get_params());
        } else {
            echo $renderer->render_import_course_selector($this->search, $PAGE->url, $this->get_params(), $this->archiveid);
        }
    }
}

class enrol_wisc_stage_course_settings extends enrol_wisc_stage_base {
    protected $mform;
    protected $editoroptions; // For summary editor

    protected function init_stage() {
        global $CFG;
        $settings = $this->cc->get_settings();

        $catlist = enrol_wisc_create_ui_helper::get_category_list($this->ui);
        if (empty($catlist)) {
            throw new moodle_exception('No create course permissions in any course category.');
        }
        if (!$this->cc->get_classes()) {
            // No classes to base a category default on
            // so force the user to select a category manually by adding a "Choose..." item.
            $catlist = array(''=>get_string('choosedots')) + $catlist;
            $defaultcategory = '';
        } else {
            // We have a reasonable default, so use the first category as default
            $defaultcategory = current(array_keys($catlist));
        }
        if (is_null($settings)) {
            // defaults
            $course = enrol_wisc_controller_helper::create_course_defaults($this->cc);

            $settings = new stdClass();
            $settings->category    = (string)$defaultcategory; // cast to string, as quickforms doesn't seem to handle mixed integers/strings in a select properly
            $settings->fullname    = $course->fullname;
            $settings->shortname   = $course->shortname;
            $settings->summary     = $course->summary;
            $settings->summaryformat = $course->summaryformat;
            $settings->format      = $course->format;
            $settings->numsections = $course->numsections;
            $settings->startdate   = $course->startdate;
            $settings->visible     = $course->visible;

            $settings->enrol_guest_status_0 = get_config('enrol_guest', 'status');
            $settings->tarole   = get_config('enrol_wisc', 'tarole');
            $settings->auditorrole   = get_config('enrol_wisc', 'auditorrole');
            $this->cc->set_settings($settings);
            $this->cc->set_configured(true);
        }
        $this->settings = $settings;

        $edit = optional_param('editsettings', false, PARAM_BOOL)
                || optional_param('editsettingsbtn', false, PARAM_BOOL);
        if (empty($settings->fullname) || empty($settings->shortname) || empty($settings->category)) {
            $edit = true;
        }

        // Prepare the editor.
        // Don't allow files, because we don't have a course context in which to store them.
        $this->editoroptions = array('maxfiles' => 0, 'trusttext'=>false, 'noclean'=>true);
        $settings = file_prepare_standard_editor($settings, 'summary', $this->editoroptions, null, 'course', 'summary', null);

        $params = array('settings'=>$settings, 'catlist'=>$catlist, 'edit'=>$edit, 'editoroptions'=>$this->editoroptions);
        $this->mform = new enrol_wisc_create_course_settings_form($this->ui, $this->get_params(), null, $params);
    }

    public function process_stage() {
        $mform = $this->mform;
        if ($mform->is_cancelled()) {
            $this->ui->cancel_process();
        }
        $data = $mform->get_data();
        if ($data) {
            require_sesskey();
            $changes = 0;
            $settings = $this->cc->get_settings();
            $data = file_postupdate_standard_editor($data, 'summary', $this->editoroptions, null, 'course', 'summary', 0);

            $fields = array('category', 'fullname', 'shortname', 'summary', 'format',
                            'numsections', 'startdate', 'visible', 'enrol_guest_status_0', 'tarole', 'auditorrole');
            foreach ((array) $settings as $field=>$value) {
                if ($value !== $data->{$field}) {
                    $settings->{$field} = $data->{$field};
                    $changes++;
                }
            }
            if ($changes) {
                $this->cc->set_settings($settings);
            }
            // Return the number of changes the user made
            return $changes;
        } else {
            return false;
        }
    }

    public function display() {
        $this->mform->display();
    }
}
/*
class enrol_wisc_stage_instance_settings extends enrol_wisc_stage_base {
    protected $mform;

    protected function init_stage() {
        $settings = $this->cc->get_settings();
        if (is_null($settings)) {
            // defaults
            $settings = new stdClass();
            $settings->tarole   = get_config('enrol_wisc', 'tarole');
            $this->cc->set_settings($settings);
        }
        $this->cc->set_configured(true);
        $this->settings = $settings;
        $this->mform = new enrol_wisc_create_instance_settings_form($this->ui, $this->get_params());
        $this->mform->set_data(array('tarole'=>$settings->tarole));
    }

    public function process_stage() {
        $mform = $this->mform;
        if ($mform->is_cancelled()) {
            $this->ui->cancel_process();
        }
        $data = $mform->get_data();
        if ($data) {
            require_sesskey();
            $changes = 0;
            $settings = $this->cc->get_settings();
            if ($settings->tarole != $data->tarole) {
                $settings->tarole = $data->tarole;
                $this->cc->set_settings($settings);
                $changes++;
            }
            // Return the number of changes the user made
            return $changes;
        } else {
            return false;
        }
    }

    public function display() {
        $this->mform->display();
    }
}
*/

abstract class enrol_wisc_stage_review_base extends enrol_wisc_stage_base {
    protected $mform;

    abstract protected function init_form($params, $data);

    protected function init_stage() {
        global $DB, $PAGE;

        $options = array();
        $options['term'] = $this->cc->get_term();
        $options['canaddanything'] = has_capability('enrol/wisc:anycourse', context_system::instance());
        $options['instanceid'] = $this->cc->get_instanceid();
        $current = new wisc_course_selector_new('addselect', $options);
        $current->set_classes($this->cc->get_classes());
        $renderer = $PAGE->get_renderer('enrol_wisc');

        $adds = null;
        $removes = null;
        if ($this->cc->get_controllermode() == enrol_wisc_controller::TYPE_EDIT) {
            $params['mappinghtml'] = $renderer->render_wisc_course_selector_summary_changes($current);
        } else {
            $params['mappinghtml'] = $renderer->render_wisc_course_selector_summary($current);
        }

        $archiveid = $this->cc->get_archiveid();
        if (!empty($archiveid)) {
            $acourse = $DB->get_record('archive_course', array('id'=>$archiveid));
            if (!$acourse) {
                throw new exception('Can\'t find archive course');
            }
            $params['archive'] = $acourse->coursename;
            $params['archivedate'] = $acourse->archivedate;
        } else {
            $params['archive'] = false;
            $params['archivedate'] = false;
        }
        $params['settings'] = $this->cc->get_settings();

        // restore previous settings
        $data = array();
        $notify = $this->cc->get_notify();
        if (!is_null($notify)) {
            $savednotify = array_flip(explode(',', $notify));
            $data['notify'] = !empty($notify);
            foreach ($savednotify as $userid => $name) {
                if ($savednotify[$userid]) {
                    $data['notify_'.$userid] = 1;
                } else {
                    $data['notify_'.$userid] = 0;
                }
            }
        }
        $data['message'] = $this->cc->get_message();

        $this->init_form($params, $data);


    }

    public function process_stage() {
        $mform = $this->mform;
        if ($mform->is_cancelled()) {
            $this->ui->cancel_process();
        }
        $data = $mform->get_data();
        if ($data) {
            $changes = 0;
            $changes += $this->process_data($data);
            if (empty($data->notify)) {
                $notify = '';
            } else {
                $notifyusers = array();
                foreach ($data as $fieldname => $value) {
                    if (strpos($fieldname, 'notify_') !== 0) {
                        continue;
                    }
                    list($ignored, $userid) = explode('_', $fieldname);
                    $notifyusers[] = $userid;
                }
                $notify = implode(',', $notifyusers);
            }
            if ($this->cc->get_notify() !== $notify) {
                $this->cc->set_notify($notify);
                $changes++;
            }
            if ($this->cc->get_message() !== $data->message) {
                $this->cc->set_message($data->message);
                $changes++;
            }

            $this->ui->set_creation_options(!empty($data->backgroundcreate), !empty($data->customimport));
            return $changes;
        } else {
            return false;
        }
    }

    protected function process_data($data) {
        return 0;
    }

    public function display() {
        $this->mform->display();
    }

}

class enrol_wisc_stage_review_course extends enrol_wisc_stage_review_base {

    protected function init_form($params, $data) {
        $settings = $this->cc->get_settings();
        $params['message'] = get_string('templateemailcourse', 'enrol_wisc', array('fullname'=>$settings->fullname));

        $this->mform = new enrol_wisc_create_review_course_form($this->ui, $this->get_params(), null, $params);
        $this->mform->set_data($data);
    }
}

class enrol_wisc_stage_review_instance extends enrol_wisc_stage_review_base {

    protected function init_form($params, $data) {
        global $DB;

        $settings = $this->cc->get_settings();
        if (is_null($settings)) {
            // defaults
            $settings = new stdClass();
            $settings->tarole   = get_config('enrol_wisc', 'tarole');
            $settings->auditorrole   = get_config('enrol_wisc', 'studentrole');
            $this->cc->set_settings($settings);
        }
        $this->cc->set_configured(true);
        $data['tarole'] = $settings->tarole;
        $data['auditorrole'] = $settings->auditorrole;

        $fullname = $DB->get_field('course', 'fullname', array('id'=>$this->cc->get_courseid()));
        $params['message'] = get_string('templateemailinstance', 'enrol_wisc', array('fullname'=>$fullname));

        $this->mform = new enrol_wisc_create_review_instance_form($this->ui, $this->get_params(), null, $params);
        $this->mform->set_data($data);
    }

    protected function process_data($data) {
        $changes = 0;
        $settings = $this->cc->get_settings();
        if ($settings->tarole != $data->tarole) {
            $settings->tarole = $data->tarole;
            $this->cc->set_settings($settings);
            $changes++;
        }
        if ($settings->auditorrole != $data->auditorrole) {
            $settings->auditorrole = $data->auditorrole;
            $this->cc->set_settings($settings);
            $changes++;
        }
        return $changes;
    }
}

class enrol_wisc_stage_complete extends enrol_wisc_stage_base {

    public function process_stage() {
        return false;
    }

    public function display() {
        global $OUTPUT;
        if ($this->cc->get_controllermode() == enrol_wisc_controller::TYPE_COURSE) {
            $type = 'course';
        } else {
            $type = 'instance';
        }
        if ($this->cc->get_status() <= enrol_wisc_controller::STATUS_EXECUTING) {
            // estimate time in minutes.  This is a very rough upperbound.
            $archiveid = $this->cc->get_archiveid();
            $crondelay = 5; // Maximum delay before we expect cron to execute the restore.
            $timeestimate = 5; // enrollments shouldn't take long
            if ($archiveid) {
                $newtimeestimate = local_archive_estimate_restore_time($archiveid);
                if ($newtimeestimate > $timeestimate) {
                    $timeestimate = $newtimeestimate;
                }
            }
            $message = get_string('createpending'.$type, 'enrol_wisc', $timeestimate + $crondelay);
            $class = 'notifysuccess';
        } else if ($this->cc->get_status() == enrol_wisc_controller::STATUS_FINISHED_OK) {
            $message = get_string('createsuccess'.$type, 'enrol_wisc');
            $class = 'notifysuccess';
        } else {
            if ($this->cc->get_courseid()) {
                $message = get_string('createerror'.$type, 'enrol_wisc', nl2br($this->cc->get_errors()));
            } else {
                $message = get_string('createerrornocourse', 'enrol_wisc', nl2br($this->cc->get_errors()));
            }
            $class = 'notifyproblem';
        }
        $courseid = $this->cc->get_courseid();
        if (!is_null($courseid)) {
            $url = new moodle_url('/course/view.php', array('id'=>$courseid));
        } else {
            $url = new moodle_url('/');
        }
        $html  = '';
        $html .= $OUTPUT->box_start();
        $html .= $OUTPUT->notification($message, $class);
        $html .= $OUTPUT->continue_button($url);
        $html .= $OUTPUT->box_end();

        return $html;
    }

}
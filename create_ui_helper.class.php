<?php

/**
 * Course creator UI helper class
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

abstract class enrol_wisc_create_ui_helper {

    public static function get_tarole_menu() {
        global $DB;
        $teacherrole = get_config('enrol_wisc', 'teacherrole');
        $roles = get_default_enrol_roles(context_system::instance());
        asort($roles);
        $allowassign = $DB->get_records_menu('role_allow_assign', array('roleid'=>$teacherrole), '', 'id,allowassign');

        $menu = array();
        $menu[0] = get_string('none');

        // filter by assignable roles
        foreach ($roles as $id=>$name) {
            if (array_search($id, $allowassign) !== false) {
                $menu[$id] = $name;
            }
        }
        return $menu;
    }

    public static function get_auditorrole_menu() {
        global $DB;
        $teacherrole = get_config('enrol_wisc', 'teacherrole');
        $roles = $DB->get_records_list('role', 'shortname', array('student','invitedguest','student_hidden'), 'name', 'id, name');
        $default = get_config('enrol_wisc', 'auditorrole');
        if ($default && !isset($roles[$default])) {
          $roles[$default] = $DB->get_record('role', array('id'=>$default), 'id, name');
        }
        $allowassign = $DB->get_records_menu('role_allow_assign', array('roleid'=>$teacherrole), '', 'id,allowassign');

        $menu = array();
        $menu[0] = get_string('none');

        // filter by assignable roles
        foreach ($roles as $id=>$role) {
            if (array_search($id, $allowassign) !== false) {
                $menu[$id] = $role->name;
            }
        }
        return $menu;
    }

    /**
     * Builds the term selector menu
     *
     * @return array consisting of $termmenu, $defaultterm, $allterms
     *
     */
    public static function get_term_menu(enrol_wisc_create_ui $ui) {
        global $DB;

        $cc = $ui->get_controller();

        // Any exception here is fatal, so let moodle handle it for now
        $datastore = new \enrol_wisc\local\chub\chub_datasource();
        $availableterms = array();
        foreach ($datastore->getAvailableTerms() as $term) {
            $availableterms[$term->termCode] = $term;
        }
        $allterms = array();
        foreach ($datastore->getAvailableAndFutureTerms() as $term) {
            $allterms[$term->termCode] = $term;
        }

        // Admins can see any term enabled on site or "other"
        $listallterms = has_capability('moodle/site:config', context_system::instance());

        // Get allowed terms from site config
        $configuredtermsetting = get_config('enrol_wisc', 'creatorterms');
        if ($configuredtermsetting === '') {
            $configuredterms = array();
        } else {
            $configuredterms = explode(',', $configuredtermsetting);
        }

        if ($cc->get_controllermode() == enrol_wisc_controller::TYPE_INSTANCE) {
            $courseid = $cc->get_courseid();
            $usedterms = $DB->get_records_menu('enrol', array('courseid'=>$courseid, 'enrol'=>'wisc'), '', 'id,'.enrol_wisc_plugin::termfield);
            $usedterms = array_flip($usedterms);
        } else {
            $usedterms = array();
        }

        $futureterms = array();
        $termmenu = array();
        $now = time();
        $onemonthago = $now - (28 * 24 * 60 * 60); // to decide default term
        foreach ($configuredterms as $termcode) {
            if ($termcode == 0) {
                continue; // "other" courses handled below
            }
            // Get term info, if available
            if (isset($allterms[$termcode])) {
                $termbegindate = $allterms[$termcode]->beginDate;
                $termenddate = $allterms[$termcode]->endDate;
            } else {
                $term = null;
                // Use termcode as date.  It's in the past, and ordered by term.
                $termbegindate = $termcode;
                $termenddate = $termcode;
            }

            // If adding enrollment method to existing class, see if term is already used.
            if (isset($usedterms[$termcode])) {
                continue;
            }
            $isadminonly = $termenddate < $now;
            if (!$isadminonly || $listallterms) {
                $displayname = \enrol_wisc\local\chub\timetable_util::get_term_name($termcode);
                $availableterm = isset($availableterms[$termcode]);
                if (!$availableterm) {
                    $displayname .= ' '.get_string('norosterdata', 'enrol_wisc');
                }
                if ($isadminonly) {
                    $displayname .= ' '.get_string('siteadminonly', 'enrol_wisc');
                }
                $termmenu[$termcode] = $displayname;

                $oddterm = ($termcode % 2 == 1); // odd term codes are never the default (e.g. 'Winter' term)

                // keep track of future terms so that we can pick a reasonable default
                if (!$isadminonly && !$oddterm && $termbegindate > $onemonthago && $termenddate > $now) {
                    $futureterms[] = $termcode;
                }
            }
        }

        // Add "other" selection if needed
        $includeother = $cc->get_controllermode() == enrol_wisc_controller::TYPE_COURSE
                  &&  (in_array(0, $configuredterms) || $listallterms);
        if ($includeother) {
            $termmenu[0] = get_string('otherterm', 'enrol_wisc');
            if (!in_array(0, $configuredterms)) {
                $termmenu[0] .= get_string('siteadminonly', 'enrol_wisc');
            }
        }

        if (!empty($futureterms)) {
            $defaultterm = min($futureterms);
        } else {
            $defaultterm = max(array_keys($termmenu));
        }

        ksort($termmenu);
        $termmenu = array_reverse($termmenu, true);

        return array($termmenu, $defaultterm, $availableterms);
    }

    /**
     * Get list of additional categories which the user has manager access to
     *
     * @param enrol_wisc_create_ui $ui
     */
    public static function get_manager_categories_list(enrol_wisc_create_ui $ui) {
        global $DB;

        $coursecats = coursecat::make_categories_list(array('enrol/wisc:createcourseincategory', 'enrol/wisc:config'));

        // Restrict search to categories with no idnumber, since we don't want to duplicate categories matching timetable items.
        // This isn't a very good way to differentiate, but it'll work in most cases.
        // TODO Find a better way to do this.
        $nonchubcats = $DB->get_records_select('course_categories', 'idnumber IS NULL OR idnumber = \'\' ', array(), '', 'id');

        return array_intersect_key($coursecats, $nonchubcats);
    }

    public static function get_category_list(enrol_wisc_create_ui $ui) {
        global $DB;
        global $CFG;

        $cc = $ui->get_controller();

        if (!get_config("enrol_wisc", "autocategory")) {
            // Get the list of all categories sorted in a nice way
            $validcatlist = array();
            $parentlist = array();
            make_categories_list($validcatlist, $parentlist, 'enrol/wisc:createcourseincategory');
            unset($parentlist);
        } else {
            // Create the category tree, including categories that don't exist yet if autocreatecategory is enabled
            $cattree = enrol_wisc_controller_helper::get_category_menu($cc);
            $validcatlist = $cattree->make_categories_list();

            // Add any other categories that we have manager access in
            $managercatlist = self::get_manager_categories_list($ui);

            if ($cc->get_classes()) {
                // If we have classes, put matching CHUB categories first, as they are likely accurate.
                $validcatlist = $validcatlist + $managercatlist;
            } else {
                // If we don't have classes, put the manager access categories before the enormous list of potential categories.
                $validcatlist = $managercatlist + $validcatlist;
            }
        }

        // if no categories found, use the default category
        if (empty($validcatlist)) {
            // still no match, so use the configured default category
            $defaultcatid = get_config("enrol_wisc", "category");
            $validcatlist[(string)$defaultcatid] = $DB->get_field('course_categories', 'name', array('id'=>$defaultcatid));
        }

        return $validcatlist;
    }


    public static function initialize_confirm_js($page, $title, $selector) {
        $config = new stdClass;
        $config->title = get_string($title, 'enrol_wisc');
        $config->question = get_string($title.'question', 'enrol_wisc');
        $config->yesLabel = get_string($title.'yes', 'enrol_wisc');
        $config->noLabel = get_string($title.'no', 'enrol_wisc');
        $config->selector = $selector;
        $page->requires->yui_module('moodle-enrol_wisc-confirm', 'M.enrol_wisc.watch_buttons', array($config));
    }
}

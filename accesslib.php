<?php

/**
 * Course creation access functions
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/enrol/wisc/lib.php');


/**
 * Check if a user is allowed to create a UW course.  This can either be because the moodle capabilities allow it,
 * or because the uwRoles allow it via the plugin configuration.
 *
 * @return int enrol_wisc_plugin::CREATECOURSE_NONE|CREATECOURSE_LIMITED|CREATECOURSE_FULL
 */
function wisc_can_create_course($user = null, $context = null) {
    global $USER;
    if (is_null($user)) {
        $user = $USER;
    }
    if (is_null($context)) {
        $context = context_system::instance();
    }
    $has_createcourse = has_capability('enrol/wisc:createcourse', $context, $user);
    $has_anycourse = has_capability('enrol/wisc:anycourse', $context, $user);
    if ($has_createcourse && $has_anycourse) {
        $capability = enrol_wisc_plugin::CREATECOURSE_FULL;
    } else if ($has_createcourse) {
        $capability = enrol_wisc_plugin::CREATECOURSE_LIMITED;
    } else {
        $capability = enrol_wisc_plugin::CREATECOURSE_NONE;
    }

    if (has_capability('enrol/wisc:createcoursebyuwrole', $context, $user)) {
        $uwroles_capability = wisc_uwroles_to_capability($user);
    } else {
        $uwroles_capability = enrol_wisc_plugin::CREATECOURSE_NONE;
    }

    if ($capability == enrol_wisc_plugin::CREATECOURSE_FULL || $uwroles_capability == enrol_wisc_plugin::CREATECOURSE_FULL) {
        $result = enrol_wisc_plugin::CREATECOURSE_FULL;
    } else if ($capability == enrol_wisc_plugin::CREATECOURSE_LIMITED || $uwroles_capability == enrol_wisc_plugin::CREATECOURSE_LIMITED) {
        $result = enrol_wisc_plugin::CREATECOURSE_LIMITED;
    } else {
        $result = enrol_wisc_plugin::CREATECOURSE_NONE;
    }

    return $result;

}

function wisc_uwroles_to_capability($user) {
    $has_createcourse = false;
    $has_anycourse = false;
    // lookup ISIS roles
    if ($user->auth === get_config('local_wiscservices', 'authtype')) {
        if (!isset($user->profile)) {
            profile_load_custom_fields($user);
        }
        if (isset($user->profile['uwRoles'])) {
            $roles = explode(',', $user->profile['uwRoles']);
            $alluwroles = enrol_wisc_plugin::get_coursecreator_uwroles();

            foreach ($roles as $uwrole) {
                if (!isset($alluwroles[$uwrole])) {
                    continue; // not a role we care about
                }
                $setting = get_config('enrol_wisc', 'uwrole_'.$uwrole);
                if ($setting === enrol_wisc_plugin::CREATECOURSE_LIMITED) {
                    $has_createcourse = true;
                } else if ($setting === enrol_wisc_plugin::CREATECOURSE_FULL) {
                    $has_createcourse = true;
                    $has_anycourse = true;
                }
            }
        }
    }
    if ($has_createcourse && $has_anycourse) {
        $result = enrol_wisc_plugin::CREATECOURSE_FULL;
    } else if ($has_createcourse) {
        $result = enrol_wisc_plugin::CREATECOURSE_LIMITED;
    } else {
        $result = enrol_wisc_plugin::CREATECOURSE_NONE;
    }
    return $result;
}

/**
 * Add course creator setting to menu.  This is called from the local/wisc_local plugin.
 *   We need this hook since our usual settings file is only read for admins, while course creators can be anyone.
 *
 * @param object $ADMIN
 */
function wisc_settings_local_hook($ADMIN) {
    global $CFG;
    if (!enrol_is_enabled('wisc')) {
        return;
    }
    if (wisc_can_create_course()) {
        $ADMIN->add('courses', new admin_externalpage('wisccreate',
                                                      get_string('createcourse','enrol_wisc'),
                                                      "$CFG->wwwroot/enrol/wisc/create.php",
                                                      array('enrol/wisc:createcoursebyuwrole','enrol/wisc:createcourse')));
    }
}
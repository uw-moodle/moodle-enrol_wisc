<?php
/**
 * Wisc enrollment backup class.
 *
 * This is currently only used on a patched version of moodle which includes pluggable enrollment backup.
 *
 * @author Matt Petro
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/lib.php');

/**
 * Provides the information to backup wisc enrol instances
 */
class backup_enrol_wisc_plugin extends backup_enrol_plugin {

    /**
     * Returns the coursemap information to attach to wisc element
     */
    protected function define_enrol_plugin_structure() {

        // Define the virtual plugin element with the condition to fulfill
        $plugin = $this->get_plugin_element(null, '../../enrol', 'wisc');

        // Create one standard named plugin element (the visible container)
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());

        // connect the visible container ASAP
        $plugin->add_child($pluginwrapper);

        $coursemaps = new backup_nested_element('coursemaps');

        // Now create the enrol own structures
        $coursemap = new backup_nested_element('coursemap', array('id'), array(
                'term', 'subject_code', 'subject',
                'catalog_number', 'section_number',
                'session_code', 'class_number', 'isis_course_id', 'type', 'groupid',
                'approved', 'deleted', 'timemodified', 'modifierid'));

        // Now the own coursemap tree
        $pluginwrapper->add_child($coursemaps);
        $coursemaps->add_child($coursemap);

        // set source to populate the data
        $coursemap->set_source_table('enrol_wisc_coursemap',
                array('enrolid'  => backup::VAR_PARENTID,
                      'courseid' => backup::VAR_COURSEID));

        // Annotate ids
        $coursemap->annotate_ids('group', 'groupid');

        return $plugin;
    }
}

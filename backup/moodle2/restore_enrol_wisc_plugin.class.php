<?php
/**
 * Wisc enrollment restore class.
 *
 * This is currently only used on a patched version of moodle which includes pluggable enrollment backup.
 *
 * @author Matt Petro
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/lib.php');

/**
 * Provides the information to backup wisc enrol instances
 */
class restore_enrol_wisc_plugin extends restore_enrol_plugin {

    public function define_enrol_plugin_structure() {
        return array(
                new restore_path_element('coursemap', $this->get_pathfor('/coursemaps/coursemap')),
        );
    }

    /**
     * Process the coursemaps element
     */
    public function process_coursemap($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $enrolid = $this->get_new_parentid('enrol');

        if (!$enrolid) {
            return; // Enrol instance was not restored
        }
        $type = $DB->get_field('enrol', 'enrol', array('id'=>$enrolid));
        if ($type !== 'wisc') {
            return; // Enrol was likely converted to manual
        }
        $data->enrolid = $enrolid;
        $data->courseid = $this->task->get_courseid();
        $data->groupid = $this->get_mappingid('group', $data->groupid);
        $newitemid = $DB->insert_record('enrol_wisc_coursemap', $data);
    }
}

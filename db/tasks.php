<?php
/**
 * WISC enrolment plugin scheduled tasks.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$tasks =
array(
    array(
            'classname' => 'enrol_wisc\task\cleanup',
            'blocking' => 0,
            'minute' => '*/5',
            'hour' => '*',
            'day' => '*',
            'dayofweek' => '*',
            'month' => '*'
    ),
    array(
            'classname' => 'enrol_wisc\task\send_approval_emails',
            'blocking' => 0,
            'minute' => '*/5',
            'hour' => '*',
            'day' => '*',
            'dayofweek' => '*',
            'month' => '*'
    )
);
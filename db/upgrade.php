<?php  //$Id: upgrade.php,v 1.1.8.1 2008/05/01 20:39:47 skodak Exp $

// This file keeps track of upgrades to
// the choice module
//
// Sometimes, changes between versions involve
// alterations to database structures and other
// major things that may break installations.
//
// The upgrade function in this file will attempt
// to perform all the necessary actions to upgrade
// your older installtion to the current version.
//
// If there's something it cannot do itself, it
// will tell you what you need to do.
//
// The commands in here will all be database-neutral,
// using the functions defined in lib/ddllib.php
global $CFG;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/enrol/wisc/updatelib.php');

function xmldb_enrol_wisc_upgrade($oldversion=0) {
    global $CFG, $DB, $OUTPUT;

    $dbman = $DB->get_manager();

    $result = true;

    //===== 2.0 upgrade line ======//

    if ($oldversion < 2010082400) {
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('class_number');
        if (!$dbman->field_exists($table, $field)) {
            $field->setattributes(XMLDB_TYPE_INTEGER, '5', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, 0, 'session_code');
            $dbman->add_field($table, $field);

            $coursemaps = $DB->get_records('enrol_wisc_coursemap', null, '', 'id,lmsid');
            foreach ($coursemaps as $coursemap) {
                list($term, $classnumber) = preg_split("/_/",$coursemap->lmsid);
                if (empty($classnumber)) {
                    $classnumber = 0; // weird
                }
                $DB->set_field('enrol_wisc_coursemap', 'class_number', $classnumber, array('id'=>$coursemap->id));
            }
            $index = new xmldb_index('lmsid_ix', XMLDB_INDEX_UNIQUE, array('lmsid'));
            $dbman->drop_index($table, $index);
            $index = new xmldb_index('idnumber_ix', XMLDB_INDEX_NOTUNIQUE, array('courseid','term','subject_code','catalog_number','section_number'));
            $dbman->drop_index($table, $index);

            $field = new xmldb_field('lmsid');
            $dbman->drop_field($table, $field);
            $field = new xmldb_field('imsrole_learner');
            $dbman->drop_field($table, $field);
            $field = new xmldb_field('imsrole_instructor');
            $dbman->drop_field($table, $field);
            $field = new xmldb_field('imsrole_ta');
            $dbman->drop_field($table, $field);
            $field = new xmldb_field('imssync_learner');
            $dbman->drop_field($table, $field);
            $field = new xmldb_field('imssync_instructor');
            $dbman->drop_field($table, $field);
            $field = new xmldb_field('imssync_ta');
            $dbman->drop_field($table, $field);

            $index = new xmldb_index('class_number', XMLDB_INDEX_NOTUNIQUE, array('term','class_number'));
            $dbman->add_index($table, $index);
            $index = new xmldb_index('catalog_info', XMLDB_INDEX_NOTUNIQUE, array('term','subject_code','catalog_number','section_number'));
            $dbman->add_index($table, $index);
            $key = new xmldb_key('courseid', XMLDB_KEY_FOREIGN, array('courseid'), 'course', array('id'));
            $dbman->add_key($table, $key);
        }
        upgrade_plugin_savepoint(true, 2010082400, 'enrol', 'wisc');
    }

    if ($oldversion < 2010090800) {
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('groupid');
        if (!$dbman->field_exists($table, $field)) {
            $field->setattributes(XMLDB_TYPE_INTEGER, '10', null, null, null, null, null, null, 'class_number');
            $dbman->add_field($table, $field);
            $key = new xmldb_key('groupid', XMLDB_KEY_FOREIGN, array('groupid'), 'groups', array('id'));
            $dbman->add_key($table, $key);
        }
        upgrade_plugin_savepoint(true, 2010090800, 'enrol', 'wisc');
    }

    if ($oldversion < 2010092200) {
        // Define field approved to be added to enrol_wisc_coursemap
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('approved', XMLDB_TYPE_INTEGER, '1', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '1', 'groupid');

        // Conditionally launch add field approved
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field enrolid to be added to enrol_wisc_coursemap
        $field = new xmldb_field('enrolid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0', 'courseid');

        // Conditionally launch add field enrolid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define key enrolid (foreign) to be added to enrol_wisc_coursemap
        $key = new xmldb_key('enrolid', XMLDB_KEY_FOREIGN, array('enrolid'), 'enrol', array('id'));

        // Launch add key enrolid
        $dbman->add_key($table, $key);

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2010092200, 'enrol', 'wisc');
    }

    if ($oldversion < 2011012701) {

        // Define field timemodified to be added to enrol_wisc_coursemap
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0', 'approved');

        // Conditionally launch add field timemodified
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field modifierid to be added to enrol_wisc_coursemap
        $field = new xmldb_field('modifierid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0', 'timemodified');

        // Conditionally launch add field modifierid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2011012701, 'enrol', 'wisc');
    }

    if ($oldversion < 2011041901) {

        // update coursemap table enrolid field
        $sql = "UPDATE {enrol_wisc_coursemap} m
                    JOIN ( SELECT e.courseid, MIN(e.id) AS id
                           FROM {enrol} e
                           WHERE e.enrol='wisc'
                           GROUP BY e.courseid ) g
                    ON m.courseid = g.courseid
                    SET m.enrolid = g.id
                    WHERE m.enrolid=0";
        $DB->execute($sql);

        // add any wisc enrol instances that are missing.
        // This will be the case if there is no actual enrolment during the upgrade
        $sql = "SELECT DISTINCT courseid FROM {enrol_wisc_coursemap} WHERE enrolid = 0";
        $courseids = $DB->get_fieldset_sql($sql);

        $enrol = enrol_get_plugin('wisc');  // it's a little weird to instantiate this here, but we need to create new wisc enrol isntances
        foreach ($courseids as $courseid) {
            $course = $DB->get_record('course', array('id'=>$courseid));
            if (empty($course)) {
                continue;
            }
            $enrolid = $enrol->add_instance($course);
            $DB->set_field('enrol_wisc_coursemap', 'enrolid', $enrolid, array('courseid'=>$courseid, 'enrolid'=>0));
        }

        // update term field
        // this only works for classes that map to a single term, but that should be the case
        // for all 1.9 courses.
        $termfield = enrol_wisc_plugin::termfield;
        $sql = "UPDATE {enrol} e
                    JOIN ( SELECT m.enrolid, MAX(m.term) AS term
                           FROM {enrol_wisc_coursemap} m
                           GROUP BY m.enrolid ) g
                    ON e.id = g.enrolid
                    SET e.$termfield = g.term
                    WHERE e.$termfield IS NULL
                    AND e.enrol='wisc'";
        $DB->execute($sql);

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2011041901, 'enrol', 'wisc');
    }

    if ($oldversion < 2011041902) {

        // mark old section groups since we'll make new ones
        $sql = "UPDATE {groups} g
                    JOIN {groupings_groups} gs
                    ON gs.groupid=g.id
                    JOIN {groupings} s
                    ON gs.groupingid = s.id
                    SET g.name = CONCAT(g.name, ' (saved)')
                    WHERE s.name='Sections'
                    AND g.name NOT LIKE '%saved%'";
        $DB->execute($sql);

        $sql = "UPDATE {groupings} s
                    SET s.name = 'Sections (saved)'
                    WHERE s.name='Sections'";
        $DB->execute($sql);

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2011041902, 'enrol', 'wisc');
    }

    if ($oldversion < 2012021400) {

        // Define field subject to be added to enrol_wisc_coursemap
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('subject', XMLDB_TYPE_CHAR, '10', null, null, null, null, 'subject_code');

        // Conditionally launch add field subject
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field isis_course_id to be added to enrol_wisc_coursemap
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('isis_course_id', XMLDB_TYPE_CHAR, '6', null, null, null, null, 'class_number');

        // Conditionally launch add field isis_course_id
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field type to be added to enrol_wisc_coursemap
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('type', XMLDB_TYPE_CHAR, '3', null, null, null, null, 'isis_course_id');

        // Conditionally launch add field type
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012021400, 'enrol', 'wisc');
    }

    if ($result && $oldversion < 2012021401) {

        // attempt to populate new coursemap fields
        echo $OUTPUT->notification("Updating coursemap entries. This might take a while...");
        try {
            $datastore = new \enrol_wisc\local\chub\chub_datasource();
            $terms = $datastore->getAvailableTerms();
            $termcodes = array();
            foreach ($terms as $term) {
                $termcodes[] = $term->termCode;
            }
            if (!empty($termcodes)) {
                $maps = $DB->get_recordset_list('enrol_wisc_coursemap', 'term', $termcodes);
                foreach ($maps as $map) {
                    $needsupdate = is_null($map->subject) || is_null($map->isis_course_id) || is_null($map->type);
                    if (!$needsupdate) {
                        continue;
                    }
                    try {
                        $class = $datastore->getClassByClassNumber($map->term, $map->class_number);
                        $update = new stdClass();
                        $update->id = $map->id;
                        $update->subject = $class->subject->shortDescription;
                        $update->isis_course_id = $class->courseId;
                        $update->type = $class->type;
                        $DB->update_record('enrol_wisc_coursemap', $update);
                    } catch (Exception $e) {
                        // ignore exceptions for individual courses
                        echo $OUTPUT->notification("Error fetching course $map->term_code,  $map->class_number : ".$e->getMessage());
                    }
                }
            }

        } catch(Exception $e) {
            // Likely the datastore isn't setup right
            echo $OUTPUT->notification("Failed: ". $e->getMessage());
            $result = false;
        }

        if ($result) {
            // wisc savepoint reached
            upgrade_plugin_savepoint(true, 2012021401, 'enrol', 'wisc');
        }
    }

    if ($oldversion < 2012042300) {

        // Define table enrol_wisc_controllers to be created
        $table = new xmldb_table('enrol_wisc_controllers');

        // Adding fields to table enrol_wisc_controllers
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('controllerid', XMLDB_TYPE_CHAR, '32', null, XMLDB_NOTNULL, null, null);
        $table->add_field('controllermode', XMLDB_TYPE_INTEGER, '4', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '4', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null);
        $table->add_field('pid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('term', XMLDB_TYPE_CHAR, '4', null, null, null, null);
        $table->add_field('classes', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('settings', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('archiveid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('uistate', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('errors', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');

        // Adding keys to table enrol_wisc_controllers
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table enrol_wisc_controllers
        $table->add_index('controllerid', XMLDB_INDEX_UNIQUE, array('controllerid'));

        // Conditionally launch create table for enrol_wisc_controllers
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012042300, 'enrol', 'wisc');
    }

    if ($oldversion < 2012042700) {

        // Define field notify to be added to enrol_wisc_controllers
        $table = new xmldb_table('enrol_wisc_controllers');
        $field = new xmldb_field('notify', XMLDB_TYPE_INTEGER, '4', null, null, null, null, 'archiveid');

        // Conditionally launch add field notify
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012042700, 'enrol', 'wisc');
    }

    if ($oldversion < 2012050400) {

        // Define field instanceid to be added to enrol_wisc_controllers
        $table = new xmldb_table('enrol_wisc_controllers');
        $field = new xmldb_field('instanceid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null, 'courseid');

        // Conditionally launch add field instanceid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012050400, 'enrol', 'wisc');
    }

    if ($oldversion < 2012051502) {

        // Define field deleted to be added to enrol_wisc_coursemap
        $table = new xmldb_table('enrol_wisc_coursemap');
        $field = new xmldb_field('deleted', XMLDB_TYPE_INTEGER, '1', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0', 'approved');

        // Conditionally launch add field deleted
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012051502, 'enrol', 'wisc');
    }

    if ($oldversion < 2012052100) {

        // Changing type of field notify on table enrol_wisc_controllers to text
        $table = new xmldb_table('enrol_wisc_controllers');
        $field = new xmldb_field('notify', XMLDB_TYPE_TEXT, 'small', null, null, null, null, 'archiveid');

        // Launch change of type for field notify
        $dbman->change_field_type($table, $field);

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012052100, 'enrol', 'wisc');
    }

    if ($oldversion < 2012052200) {

        // Define field message to be added to enrol_wisc_controllers
        $table = new xmldb_table('enrol_wisc_controllers');
        $field = new xmldb_field('message', XMLDB_TYPE_TEXT, 'medium', null, null, null, null, 'notify');

        // Conditionally launch add field message
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012052200, 'enrol', 'wisc');
    }

    if ($oldversion < 2012061200) {

        // update soap url to version 1.2
        $soapurl = get_config('enrol_wisc', 'soapurl');
        if ($soapurl) {
            $soapurl = preg_replace('/chub-ws-1\.[01]/', 'chub-ws-1.2', $soapurl);
            set_config('soapurl', $soapurl, 'enrol_wisc');
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012061200, 'enrol', 'wisc');
    }

    if ($oldversion < 2012110900) {

        // Define table enrol_wisc_access to be created
        $table = new xmldb_table('enrol_wisc_access');

        // Adding fields to table enrol_wisc_access
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('item', XMLDB_TYPE_CHAR, '12', null, XMLDB_NOTNULL, null, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table enrol_wisc_access
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('cohortid', XMLDB_KEY_FOREIGN, array('cohortid'), 'cohort', array('id'));

        // Conditionally launch create table for enrol_wisc_access
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012110900, 'enrol', 'wisc');
    }

    if ($oldversion < 2012111300) {

        // Define field permission to be added to enrol_wisc_access
        $table = new xmldb_table('enrol_wisc_access');
        $field = new xmldb_field('permission', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, '0', 'type');

        // Conditionally launch add field permission
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2012111300, 'enrol', 'wisc');
    }

    if ($oldversion < 2014062301) {

        // We now require CHUB >= 1.4, so try to upgrade the saved soap url
        $soapurl = get_config('enrol_wisc', 'soapurl');
        $soapurl = preg_replace('/chub-ws-1\.[123]/', 'chub-ws-1.4', $soapurl);
        set_config('soapurl', $soapurl, 'enrol_wisc');

        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2014062301, 'enrol', 'wisc');
    }

    if ($oldversion < 2014062302) {

        // Define field component to be added to enrol_wisc_access.
        $table = new xmldb_table('enrol_wisc_access');
        $field = new xmldb_field('component', XMLDB_TYPE_CHAR, '100', null, null, null, null, 'permission');

        // Conditionally launch add field component.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Wisc savepoint reached.
        upgrade_plugin_savepoint(true, 2014062302, 'enrol', 'wisc');
    }

    if ($oldversion < 2015041600) {

        // Try to update config to the new ESB url
        $soapurl = get_config('enrol_wisc', 'soapurl');
        $soapurl = preg_replace('/\.services\.wisc\.edu\/CHUB/', '.services.wisc.edu/esbv2/CHUB', $soapurl);
        set_config('soapurl', $soapurl, 'enrol_wisc');
        // wisc savepoint reached
        upgrade_plugin_savepoint(true, 2015041600, 'enrol', 'wisc');
    }

    if ($oldversion < 2015042000) {

        // Update group configuration to use enrol_wisc component.

        // The former convention was that groups_members.timeadded == 0 indicates
        // enrol_wisc ownership of the assignment.  This code switches that convention
        // to use the component 'enrol_wisc'.
        $sql = 'UPDATE {groups_members} gm
                   SET gm.timeadded = ?, gm.component = \'enrol_wisc\'
                 WHERE gm.timeadded = 0
                   AND gm.component = \'\'
                   AND gm.groupid IN (SELECT groupid FROM {enrol_wisc_coursemap})';
        $DB->execute($sql, array(time()));
        upgrade_plugin_savepoint(true, 2015042000, 'enrol', 'wisc');
    }

    if ($oldversion < 2015111200) {

        // Define field messagesent to be added to enrol_wisc_controllers.
        $table = new xmldb_table('enrol_wisc_controllers');
        $field = new xmldb_field('messagesent', XMLDB_TYPE_INTEGER, '4', null, null, null, '0', 'message');

        // Conditionally launch add field messagesent.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
            // Mark all existing controllers as sent.
            $DB->set_field('enrol_wisc_controllers', 'messagesent', 1, array());
        }

        // Wisc savepoint reached.
        upgrade_plugin_savepoint(true, 2015111200, 'enrol', 'wisc');
    }


    // check for course originator role
    // if it doesn't exist, create it and grant our default
    // permissions to it.
    wisc_check_courseoriginator();

    // create the temporary course creator category
    wisc_get_temp_category();

    return $result;
}

?>

<?php
/******************************************************************************
* WISC Webservice - Web Services Definitions
*
* The webservices definitions for the WISC Services plugin.
*
* Author: Nick Koeppen
******************************************************************************/

defined('MOODLE_INTERNAL') || die();

$functions = array(
    'enrol_wisc_course_pages' => array(
        'classname'   		=> 'enrol_wisc_external',
        'methodname'  		=> 'course_pages',
        'classpath'   		=> 'enrol/wisc/externallib.php',
        'description' 		=> 'Retrieve Moodle page information a given course.',
        'type'        		=> 'read',
        'capabilities'		=> '',
    ),
    'enrol_wisc_user_courses' => array(
        'classname'   		=> 'enrol_wisc_external',
        'methodname'  		=> 'user_courses',
        'classpath'   		=> 'enrol/wisc/externallib.php',
        'description' 		=> 'Get list of courses for a given username.',
        'type'        		=> 'read',
        'capabilities'		=> 'moodle/course:viewparticipants',
    ),
    'enrol_wisc_roster_change_event' => array(
        'classname'   		=> 'enrol_wisc_external',
        'methodname'  		=> 'roster_change_event',
        'classpath'   		=> 'enrol/wisc/externallib.php',
        'description' 		=> 'Notify enrol method of a roster change event.',
        'type'        		=> 'write',
        'capabilities'		=> 'enrol/wisc:pushrosterchange',
    ),
);

$services = array(
        'UW WISC enrollment events' => array(
                'functions' => array ('enrol_wisc_roster_change_event'),
                'requiredcapability' => '',
                'restrictedusers' => 1,
                'enabled'=>1,
        ),
        'ENGR course pages service' => array(
                'functions' => array ('enrol_wisc_course_pages'),
                'requiredcapability' => '',
                'restrictedusers' => 1,
                'enabled'=>1,
        ),
        'UW Course dashboard service' => array(
                'functions' => array ('enrol_wisc_user_courses'),
                'requiredcapability' => '',
                'restrictedusers' => 1,
                'enabled'=>1,
        ),
);
<?php

/**
 * WISC enrolment plugin event handlers
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/* List of events thrown from WISC enrollments

enrol_wisc_coursemap_added
enrol_wisc_coursemap_deleted
enrol_wisc_coursemap_approved
*/


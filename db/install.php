<?php

/**
 * WISC enrolment plugin installation.
 *
 * @package    enrol_wisc
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later

 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/enrol/wisc/updatelib.php');

function xmldb_enrol_wisc_install() {
    global $CFG, $DB;

    // check for course originator role
    // if it doesn't exist, create it and grant our default
    // permissions to it.

    if (!PHPUNIT_TEST) {
        wisc_check_courseoriginator();
    }
    // create the temporary course creator category
    wisc_get_temp_category();
}

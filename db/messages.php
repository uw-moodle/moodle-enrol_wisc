<?php

defined('MOODLE_INTERNAL') || die();

$messageproviders = array (

/// Course ready notifications
        'courseready' => array (
                'defaults' => array(
                        'popup' => MESSAGE_PERMITTED,
                        'email' => MESSAGE_FORCED + MESSAGE_DEFAULT_LOGGEDIN + MESSAGE_DEFAULT_LOGGEDOFF,
                ),
        ),

);
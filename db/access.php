<?php

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    'enrol/wisc:config' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW,
        )
    ),
    'enrol/wisc:enrolconfig' => array(
        'riskbitmask' => RISK_SPAM | RISK_PERSONAL,
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSECAT,
        'archetypes' => array(
            'manager' => CAP_ALLOW,
        )
    ),
    'enrol/wisc:anycourse' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW,
            'coursecreator' => CAP_ALLOW,
        )
    ),
    'enrol/wisc:anysubject' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
                'manager' => CAP_ALLOW,
        )
    ),
    'enrol/wisc:createcourse' => array(
        'riskbitmask' => RISK_SPAM | RISK_PERSONAL,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW,
            'coursecreator' => CAP_ALLOW,
        )
    ),
    'enrol/wisc:createcoursebyuwrole' => array(
        'riskbitmask' => RISK_SPAM | RISK_PERSONAL,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'user' => CAP_ALLOW,
        )
    ),
    'enrol/wisc:createcourseincategory' => array(
            'riskbitmask' => RISK_SPAM,
            'captype' => 'write',
            'contextlevel' => CONTEXT_COURSECAT,
            'archetypes' => array(
                    'manager' => CAP_ALLOW,
                    'coursecreator' => CAP_ALLOW,
            )
    ),
    'enrol/wisc:pushrosterchange' => array(
            'riskbitmask' => RISK_DATALOSS | RISK_PERSONAL,
            'captype' => 'write',
            'contextlevel' => CONTEXT_COURSE,
            'archetypes' => array(
            )
    ),
    // For removing suspended users created by sync process.
    'enrol/wisc:unenrol' => array(
            'captype' => 'write',
            'contextlevel' => CONTEXT_COURSE,
            'archetypes' => array(
                'editingteacher' => CAP_ALLOW,
                'manager' => CAP_ALLOW,
            )
    ),
);
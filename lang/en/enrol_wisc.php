<?php
$string['access_org_key'] = '<b>All courses</b>';
$string['access_subject_key'] = '{$a}';
$string['addcourse'] = 'Add course';
$string['addenrolment'] = 'Add WISC enrollments';
$string['admincategoryname'] = 'WISC';
$string['allowdups'] = 'Enable a single timetable course to be mapped to multiple moodle courses.';
$string['allowdups_key'] = 'Allow duplicate associations';
$string['allusers'] = '<i>All course creators</i>';
$string['alreadyinusename'] = '*{$a}';
$string['alreadyinuse_title'] = 'Already in use';
$string['approvalemail'] = 'Enter an email address to receive notifications of pending roster associations.  We also include this address in emails as a support contact.';
$string['approvalemail_key'] = 'Approval email';
$string['approvalemail_message'] = "Roster association requested for the course {\$a->course}\n\nRequestor: {\$a->user} ({\$a->email})\nUW roles : {\$a->roles}\n\nOther instructors enrolled in the course:\n\n{\$a->teachers}\n\nURL: {\$a->url}\n";
$string['approvalemail_subject'] = '[{$a->site}]: UW Roster Association Request';
$string['approve'] = 'Approve associations';
$string['archivedate'] = "Archive date";
$string['archive'] = "Import from archive";
$string['archivename'] = "Archive course";
$string['askimportbox'] = "Import course content from a previous course?";
$string['associations'] = 'Associations in this course';
$string['associations_help'] = 'This box lists UW classes that are associated with this course.  If any of the sections are listed as "PENDING", then the association is awaiting approval before the students can be added.';
$string['auditorrole'] = "Registered auditors";
$string['auditorrole_key'] = "Auditor role";
$string['autocategory_key'] = 'Automatic categories';
$string['autocategory'] = 'Try to automatically match courses to course category based on timetable information. We use the category idnumber field to do match either the patterns "deptcode_termcode" or "termcode".  For non-timetable courses, use "misc" for termcode.';
$string['autocreatecategory_key'] = 'Automatically create categories';
$string['autocreatecategory'] = 'If Automatic Categories is enabled, then create missing course categories as they are needed.';
$string['backgroundcreate'] = 'Run in background';
$string['backgroundcreate_help'] = 'When selected, the course will be created in the background and you will receive an email when it is complete.  Disabling this will mean the course creation happens while you wait.  Note that a large course creation can take more than 10 minutes to complete.';
$string['buildcategories'] = 'Build categories';
$string['buildcategoriesmenu'] = 'WISC categories';
$string['buildcategorydone']    = 'Finished building categories';
$string['buildcategory'] = 'WISC course categories';
$string['cachedef_externalcourses'] = 'This caches external courses that a user is enrolled in.';
$string['category'] = 'Default category for new courses.';
$string['categoryinfo']    = '<p>This page generates categories for UW timetable courses.  The layout is Organization/Department/Term.</p><p> For example: Engineering -> Engineering Physics -> Fall 2012-2013</p>';
$string['category_key'] = 'Default category';
$string['classnotfound'] = '-- Not in datasource --';
$string['cleanuptask'] = 'Cleanup task for course creation jobs';
$string['cohort_key'] = 'Cohort';
$string['cohortoverview'] = '<p>This page controls which subjects (departments) a system-level cohort has access to inside the course creator. The special category <i>All course creators</i> represents all users who can see the "Add a UW Course" menu item, either by UW Roles, or moodle permissions.</p><p>The default configuration for subjects is to restrict access to only admins and site managers.  For cross-listed classes, access to any one of the crosslisted subjects is sufficient to have access.</p><p>With approval means that a user can create any course, but the courses will require approval from a site administrator unless the person is directly teaching the class.  In the case that multiple permissions apply to a subject, the union of all permissions applies.  So, having access to a College implies access to create all courses in every subject in the college, regardless of the subject settings.</p><p>Access via the "All course creators" cohort can be further restricted by UW Role configuration in the wisc enrollment settings.</p> ';
$string['cohortpermissions'] = 'Cohort permissions';
$string['confirmcancel'] = 'Cancel course creation';
$string['confirmcancelno'] = 'Go back';
$string['confirmcancelquestion'] = 'Are you sure you wish to cancel? Any information you have entered will be lost.';
$string['confirmcancelyes'] = 'Yes';
$string['confirmemptymappingno'] = 'No';
$string['confirmemptymapping'] = 'No timetable mapping';
$string['confirmemptymappingquestion'] = 'This course has no timetable sections mapped to it.  To add a section, select an item on the right and click the "Add" button. <br /> Are you sure you want to continue?';
$string['confirmemptymappingyes'] = 'Yes';
$string['continuetoimport'] = 'Redirecting to course import wizard.';
$string['controlleralreadyexecuted'] = 'Controller already executed';
$string['controllernotready'] = 'Controller not complete';
$string['course'] = 'Course';
$string['coursecreationsuffix'] = " [[creation in progress]]";
$string['coursecreatorroles_help'] = 'This section defines which UW roles are allowed to create new UW courses.  A user with "with approval" course creation capability is allowed to create any class, however if there is no official record in the timetable linking the user to the class then the moodle class will need approval before students are added.  Approval requests are sent to the email address listed below the roles.<br /><br />Alternatively, to grant UW course creation rights to a specific user, make the user a moodle "Course Creator" at the system level.';
$string['coursecreatorroles_settings'] = 'Default course creation capabilities';
$string['coursecreator_settings'] = 'New course settings';
$string['coursecreator'] = 'UW Madison Course Creator';
$string['event_coursemap_added'] = 'UW section added';
$string['event_coursemap_approved'] = 'UW Sections approved';
$string['event_coursemap_deleted'] = 'UW section deleted';
$string['coursepickerbtn'] = 'Search for a course';
$string['createcourse'] = 'Create a new UW course';
$string['createcourse_full'] = 'Allow';
$string['createcourse_limited'] = 'With Approval';
$string['createcourse_none'] = 'None';
$string['create'] = 'Create course';
$string['createemailsubjectcourse'] = "Your UW Madison Moodle course is ready!";
$string['createemailsubjectinstance'] = "Class enrollment updated";
$string['createerrorcourse'] = 'The course has been created, however there were error(s):<br /><br />{$a}';
$string['createerroremailsubjectcourse'] = "Error while creating your course.";
$string['createerroremailsubjectinstance'] = "Error while updating enrollment.";
$string['createerrorinstance'] = 'There were error(s) updating class enrollment:<br /><br />{$a}';
$string['createerrornocourse'] = 'An error occured:{$a}';
$string['createerrorsmallinstance'] = 'Error updating enrollment for \'{$a->fullname}\'';
$string['createfailedsmallcourse'] = 'Failed to create \'{$a->fullname}\'';
$string['createfailedsmallinstance'] = 'Failed to update enrollment in course \'{$a->fullname}\'';
$string['creategrp'] =  "Created group {\$a->name} course {\$a->course} ({\$a->courseid})";
$string['createnewcourse'] = "Create course";
$string['createnewinstance'] = "Add enrollment";
$string['creatependingcourse'] = 'The course is being created now and we\'ll send you an email when it is ready for use.   This might take up to {$a} minutes.';
$string['creatependinginstance'] = 'Class enrollment is being updated and we\'ll send you an email when it is ready for use.   This might take up to {$a} minutes.';
$string['createerroremailinstance'] = 'An error occured while updating enrollment for
\'{$a->fullname}\'.

Errors:

{$a->errors}

If you require assistance, please contact {$a->supportemail}

To access your course, go to {$a->url}';
$string['createfailedemailcourse'] = 'We were unable to create the course
   \'{$a->fullname}\'
due to the following errors:

{$a->errors}

Moodle administrators have been notified about the problem and will likely contact you to get it resolved.  If you require assistance, please contact {$a->supportemail}';
$string['createfailedemailinstance'] = 'We were unable to update enrollment in the course
   \'{$a->fullname}\'
due to the following errors:

{$a->errors}

If you require assistance, please contact {$a->supportemail}';
$string['createsubjects'] = 'Allowed subjects';
$string['createsuccesscourse'] = 'The course has been created and is ready for use.';
$string['createsuccessemailinstance'] = 'This is an email confirmation that the UW enrollment for


\'{$a->fullname}\'

has been updated.

To access your course, go to {$a->url}';
$string['createsuccessinstance'] = 'Class enrollment has been updated.';
$string['createsuccesssmallcourse'] = 'Course \'{$a->fullname}\' created';
$string['createsuccesssmallinstance'] = 'Enrollment for \'{$a->fullname}\' has been updated';
$string['createuialreadyexecuted'] = "Course creation already started";
$string['creatorintro'] = 'Introduction';
$string['creatorintro_desc'] = 'Introduction to be shown on the course creator first page.';
$string['creatorterms'] = 'Course creator terms';
$string['creatorterms_desc'] = 'Restrict which terms are possible in the course creator.  This list is further filtered in the course creator by removing past terms for non-admin users.';
$string['customimport'] = 'Customize archive import';
$string['customimport_help'] = 'Customize the import process to select a subset of course materials.  This is not available for background course creations.';
$string['dontsync'] = '-- No role --';
$string['editcohort'] = 'Edit cohort';
$string['editcoursesettings'] = 'Change these settings';
$string['enrolment_settings'] =  "Enrollment settings";
$string['enrolnotenabled'] = 'WISC enrollments not enabled.';
$string['enroluserenable'] =  "Enabled enrollment for userid {\$a->userid} course {\$a->course} ({\$a->courseid})";
$string['enroluser'] =  "Enrolled userid {\$a->userid} course {\$a->course} ({\$a->courseid})";
$string['errorduplicatemap'] = 'Section already added to the course.';
$string['errorwrongterm'] = 'Class has invalid term.';
$string['extremovednodrop'] =  'No drop record found for user {$a->userid} course {$a->courseid} "{$a->course}" (CHUB: {$a->useridnumber} {$a->term} class_numbers:{$a->classnumbers}), preserving enrollment';
$string['extremovedroleunassign'] =  "Removed role {\$a->roleid} for userid {\$a->userid} course {\$a->course} ({\$a->courseid})";
$string['extremovedsuspend'] =  "Disabled enrollment for userid {\$a->userid} course {\$a->course} ({\$a->courseid})";
$string['extremovedsuspendnoroles'] =  "Disabled enrollment and removed roles for userid {\$a->userid} course {\$a->course} ({\$a->courseid})";
$string['extremovedunenrol'] =  "Unenroll userid {\$a->userid} course {\$a->course} ({\$a->courseid})";
$string['ferparole_key'] = 'Student role (FERPA) ';
$string['ferparole'] = 'Students in a course that have elected to control disclosure of personal information.';
$string['ignorehiddencourses'] = 'If enabled students will not be enrolled on courses that are set to be unavailable to students.';
$string['ignorehiddencourses_key'] = 'Skip enrollments in hidden courses';
$string['instanceauditorrole_help'] = "Role an official auditor is given in the class.";
$string['instanceauditorrole'] = "Auditor role";
$string['instancetarole_help'] = "Role a TA is given in the class.  Some TA's may receive full teacher rights regardless of this setting, due to limitations in the UW timetable information.
<br /><br />
The most common roles are:
<ul>
<li><b>Instructor:</b> Access to do anything in the course.</li>
<li><b>Guest instructor:</b> Access to participate in the class and moderate student content, but may not alter activities or access the gradebook directly.</li>
<li><b>Grader:</b> Same access as Guest instructor, but also has direct access to the gradebook.</li>
</ul>";
$string['instancetarole'] = "TA role";
$string['managecohortsmenu'] = 'Cohort permissions';
$string['managecohorts'] = 'Permissions for creating UW roster associations';
$string['mappingstoadd'] = "Courses to add";
$string['mappingstoapprove'] = "Courses to approve";
$string['mappingstoremove'] = "Courses to remove (students will be unenrolled)";
$string['maxconcurrency'] = 'Maximum number of course creations that can run at the same time.  When this limit is passed, the course creations will be retried at a later cron run. Enter 0 for no restriction.';
$string['maxconcurrency_key'] = 'Max concurrency';
$string['messageprovider:courseready'] = 'Confirmation when a UW course is ready';
$string['misccategoryname'] = 'Miscellaneous';
$string['missingcategory'] = 'Missing category';
$string['missingsummary'] = 'Missing summary';
$string['nochanges'] = 'No changes';
$string['nocoursecreatoraccess'] = 'You don\'t have access to create a new course.';
$string['nocourses'] = 'No courses found';
$string['norosterdata'] = '(no timetable data)';
$string['noschools'] = "No schools found";
$string['nosubjects'] = "No departments found";
$string['noterms'] = "No terms available for this course.";
$string['notifications'] = "Notifications";
$string['notifymessage'] = "Message";
$string['notifyspecifiedusers'] = "<strong>Send notification to:</strong>";
$string['notifyteachers'] = "Notify course teachers";
$string['nowiscservices'] = '<h4>The local/wiscservices plugin must be installed to use this plugin</h4>';
$string['offlinecreate'] = 'Create new UW courses in the background so that the user doesn\'t have to wait for lengthy enrollment and import jobs.  When the course is ready, we send an email and moodle message to the user.';
$string['offlinecreate_key'] = 'Background course creation';
$string['otherorgs'] = 'Other org units';
$string['otherorgs_desc'] = 'List non-CHUB units which should correspond to top-level categories for course creation.  Format is newline-separated lines of the form "org_code:display_name".  The org_code should start with a letter and be different from the standard UW org codes, and is used as the category idnumber.';
$string['otherrole'] = 'Instructors with no ISIS role code.  It is up to departments to set the ISIS instructor role, and often it will be blank.';
$string['otherrole_key'] = 'Other Instructors';
$string['otherterm'] = 'Other';
$string['pending_help'] = 'Sections marked as PENDING require approval before students will be added to the course.  Usually this means the system cannot determine an official teaching relationship between the person that configures this course and the corresponding UW section.';
$string['pending'] = 'PENDING';
$string['phppath_key'] = 'Php executable';
$string['phppath'] = 'Path to the CLI php executable (for background course creation)';
$string['phpsoap_noextension'] = '<h4>The PHP SOAP extension must be enabled to use this plugin</h4>';
$string['pluginname_desc'] = '<p>This plugin syncs roster data with the University of Wisconsin - Madison CHUB service.  To use this plugin, you must have an account with the CHUB service as well as authorization to obtain roster data for the classes that will be in moodle.</p>';
$string['pluginname'] = 'WISC enrollments';
$string['potentialassocs'] = '{$a} Course Catalog';
$string['potentialassocsbox'] = 'Course Catalog';
$string['potentialassocsbox_help'] = 'This box contains a list of UW classes that can be added to your course. Initially the box contains those courses where you are the instructor of record for one of the sections.<br />The "Search for a course" button can be used to add other courses to this list.';
$string['processgroup'] =  "  Group {\$a->name} ({\$a->count})\n";
$string['resortcategories'] = 'Update sort order';
$string['roles'] = 'Course role mappings';
$string['rostermapping'] = 'Enrollment';
$string['schoolcollege'] = "School or college";
$string['sectionpending_key'] = '* PENDING';
$string['sectionpendingname'] = '*{$a}';
$string['sectionpending_title'] = 'PENDING';
$string['sectionunavailablename'] = '*{$a}';
$string['selectaterm'] = 'Select a term';
$string['selectmenu'] = ' Select...';
$string['selectterms']    = 'Terms (If none selected, current term is used)';
$string['sendapprovalemailstask'] = 'Send approval emails for roster mappings';
$string['server_settings'] = 'UW CHUB Server settings';
$string['siteadminonly'] = ' - admin only';
$string['skipimport'] = "Skip import";
$string['skippingterm'] = "Term {\$a} hasn't changed since last sync.\n";
$string['skipthisstage'] = "Skip this stage";
$string['soappass_key'] = 'CHUB Password';
$string['soapurl_key'] = 'CHUB Server URL';
$string['soapuser_key'] = 'CHUB Username';
$string['stage1'] = "Term";
$string['stage2'] = "Timetable";
$string['stage3'] = "Settings";
$string['stage4'] = "Import";
$string['stage5'] = "Confirm";
$string['stage6'] = "Complete";
$string['statealreadyexecuted'] = "Creation already executed.  Please restart creation process.";
$string['studentrole_key'] = 'Student role';
$string['studentrole'] = 'Students in a course';
$string['studentsoncreate'] = 'If disabled, students will be imported the next time the moodle cron runs. This improves the interface speed when courses are created.';
$string['studentsoncreate_key'] = 'Import Student Accounts on course Creation';
$string['subjectaccess'] = "UW Subject access";
$string['subject'] = 'Department';
$string['synccourse'] = "==Syncing course {\$a->id} '{\$a->shortname}', {\$a->instance}==\n";
$string['tarole'] = 'Instructors with ISIS role code 7 (Graduate Assistants)';
$string['tarole_key'] = 'TA role';
$string['teacherrole'] = 'Instructors with ISIS role codes 1-6 (Faculty, Emeriti, Lecturers)';
$string['teacherrole_key'] = 'Faculty role';
$string['tempcategorydescription'] = 'Temporary category to hold UW courses while they are being created';
$string['tempcategory_key'] = 'Temporary category';
$string['tempcategoryname'] = 'UW course creator temp';
$string['tempcategory'] = 'Temporary category for courses being created.  This should be a hidden category.';
$string['templateemailcourse'] = 'Congratulations! Your UW Madison Moodle course

   \'{$a->fullname}\'

has been created. Here is a handy link to access the course:

   {URL}

Your course is currently hidden from students to give you a chance to make changes before students see it.

The UW Madison Moodle Knowledge Base https://kb.moodle.wisc.edu is full of helpful documentation to get you started. If you have any other questions, your UW Madison Moodle Support person can be found here: https://kb.wisc.edu/moodle/page.php?id=9515';
$string['templateemailinstance'] = 'This is a confirmation email that the UW enrollment for

\'{$a->fullname}\'

has been updated.

To access your course, go to {URL}';
$string['term'] = 'Term';
$string['term_desc'] = '&nbsp;&nbsp;<b>If your term is not listed here, please see the instructions above.</b>';
$string['term_help'] = '<p>Select the term associated with this class.</p><p>If <em>"other"</em> is chosen, then this class will not be associated with a semester.</p><p>Terms marked with <em>"no timetable data"</em> can be used, but this indicates that roster data is not yet available.  A timetable association can be added later when available.</p>';
$string['uialreadyexecuted'] = "Course creation already executed";
$string['uinotconfigured'] = "Course not configured";
$string['unenrolaction_key'] = 'External unenroll action';
$string['unenrolaction'] = 'Select action to carry out when a student drops a course.  This setting only applies to students that drop the course after the course begins.  Students that drop before the course begins are always unenrolled' ;
$string['unknownenrolstatus'] = 'Invalid enrol status.';
$string['updatecategories'] = 'Update existing categories';
$string['updateinstance'] = "Update enrollment";
$string['wisc:anycourse'] = 'Fully associate any timetable course within allowed subjects, bypassing \'pending\' association constraints';
$string['wisc:anysubject'] = 'Allow access to associate courses in any subject';
$string['wisc:config'] = 'Edit course roster associations';
$string['wisc:createcoursebyuwrole'] = 'Create a new UW course if ISIS roles allow';
$string['wisc:createcourse'] = 'Create a new UW course';
$string['wisc:createcourseincategory'] = 'Place a new course in a given category';
$string['wisc:enrolconfig'] = 'Add or delete wisc enrollment instances in a course';
$string['wisc:pushrosterchange'] = 'Send a WISC roster change notification ';
$string['wisc:unenrol'] = 'Completely unenroll course users who have dropped the class in ISIS.';
$string['wiscinfobox'] = 'Use this page to associate university roster data with the course.<br /><br />  The column on the right initially lists courses that you are officially teaching, and other courses can be added by clicking on the "Search for a course" button.  Any combination of sections from the right box can be added to the left box by selecting them and clicking the "Add" button.';
$string['withapproval'] = '(With approval)';

// External User Course setting
$string['usercoursedsn_heading'] = 'External Course Configuration';

$string['usercoursesdsn_setting'] = 'External Course DSN';
$string['usercoursesdsn_setting_desc'] = 'External course DSN configuration<br />' .
                                            'Each line represents an external data source and must be entered in this manner:<br />' .
                                            '&nbsp;&nbsp;&nbsp;&nbsp;WSDL URL;Username;Password<br /><br />' .
                                            'With no spaces inbetween the semi-colons and information.  Username and Password are optional if ' .
                                            'the WSDL uses a token in its url';



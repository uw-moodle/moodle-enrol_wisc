<?php
/**
 * Unit tests for TimetableDataStore
 *
 * These tests require that appropriate test data is provided in the moodle config.php file.
 *
 * Sample data:
 *
 * $CFG->enrol_wisc_test = (object) array (
 *  'term'           => "1112",
 *  'subject'        => "320",
 *  'catalogNumber'  => "352",
 *  'sectionNumber'  => "001",
 *  'student_pvi' => "UW0000000",  // for GetEnrolledClasses() test
 *  'teacher_pvi' => "UW0000000"   // for GetClassesByInstructor() test
 * );
 *
 */

defined('MOODLE_INTERNAL') || die();

// Make sure the code being tested is accessible.

class test_datastore extends UnitTestCase {

    public $data;  // test data from $CFG->enrol_wisc_test

    public function __construct() {
        global $CFG;

        $this->data = $CFG->enrol_wisc_test;
    }

    public function setUp() {
        if (empty($this->data)) {
            throw new Exception('NO TEST DATA.  Timetable tests require $CFG->wisc_test_data to be set in config.php');
        }
        $this->ds = new \enrol_wisc\local\chub\chub_datasource();
    }

    public function tearDown() {
        unset ($this->ds);
    }

    public function test_getTermLastUpdated() {
        $date = $this->ds->getTermLastUpdated($this->data->term);
        $this->assertTrue($date > 0, "getTermLastUpdated");
    }

    public function test_getAvailableAndFutureTerms() {
        $terms = $this->ds->getAvailableAndFutureTerms();
        $this->assertTrue(count($terms) > 0, "getAvailableAndFutureTerms");
    }

    public function test_getAvailableTerms() {
        $terms = $this->ds->getAvailableTerms();
        $this->assertTrue(count($terms) > 0, "getAvailableTerms");
    }

    public function test_getSubjectsInTerm() {
        $subjects = $this->ds->getSubjectsInTerm($this->data->term);
        $this->assertTrue(count($subjects) > 0, "getSubjectsInTerm");

        // Test error handling

        $subjects = $this->ds->getSubjectsInTerm('99');
        $this->assertTrue(count($subjects) == 0, "getSubjectsInTerm DNE");
    }

    public function test_getCourse() {
        $course = $this->ds->getCourseByCatalogNumber($this->data->term, $this->data->subject,
                                            $this->data->catalogNumber, true);
        $this->assertTrue($course && count($course->subjects) >= 1, "getCourseByCatalogNumber w/ XL");
        $course = $this->ds->getCourseByCatalogNumber($this->data->term, $this->data->subject,
                                            $this->data->catalogNumber, false);
        $this->assertTrue($course && count($course->subjects) == 1, "getCourseByCatalogNumber w/o XL");


        $course2 = $this->ds->getCourseByCourseId($this->data->term, $this->data->subject, $course->courseId, true);
        $this->assertTrue($course2 && count($course2->subjects) >= 1, "getCourseByCourseId w/ XL");

        $course2 = $this->ds->getCourseByCourseId($this->data->term, $this->data->subject, $course->courseId, false);
        $this->assertTrue($course2 && count($course2->subjects) == 1, "getCourseByCourseId w/o XL");

        $allcourses = $this->ds->getCoursesBySubject($this->data->term, $this->data->subject);
        $this->assertTrue($allcourses && count($allcourses) > 1, "getCoursesBySubject");

        // Test error handling

        $caughtFault = false;
        try {
            $nocourse = $this->ds->getCourseByCatalogNumber($this->data->term, $this->data->subject, '999999', false);
        } catch (SoapFault $e) {
            $caughtFault = true;
        }
        $this->assertTrue($caughtFault, "getCourseByCatalogNumber DNE");

        $caughtFault = false;
        try {
            $nocourse = $this->ds->getCourseByCourseId($this->data->term, $this->data->subject, '999999', false);
        } catch (SoapFault $e) {
            $caughtFault = true;
        }
        $this->assertTrue($caughtFault, "getCourseByCourseId DNE");
    }

    public function test_getClass() {
        $classes = $this->ds->getClassesByCatalogNumber($this->data->term, $this->data->subject,
                                            $this->data->catalogNumber);
        $this->assertTrue(count($classes) > 0, "getClassesByCatalogNumber");

        $classNumber = 0;
        foreach ($classes as $class) {
            if ($class->sectionNumber == $this->data->sectionNumber) {
                $classNumber = $class->classNumber;
            }
        }

        $class = $this->ds->getClassByClassNumber($this->data->term, $classNumber);
        $this->assertTrue(!empty($class), "getClassByClassNumber");

        $roster = $this->ds->getClassRoster($this->data->term, $classNumber);
        $this->assertTrue(count($roster->students) > 0 && count($roster->instructors) > 0, "getClassRoster");

        $dropped = $this->ds->getStudentsThatHaveDropped($this->data->term, $classNumber);
        $this->assertTrue(is_array($dropped), "getStudentsThatHaveDropped");

        $instructors = $this->ds->getInstructors($this->data->term, $classNumber);
        $this->assertTrue(count($instructors) > 0, "getInstructors");

        // Test error handling

        $noclasses = $this->ds->getClassesByCatalogNumber($this->data->term, $this->data->subject, '9999');
        $this->assertTrue(count($noclasses) == 0, "getClassesByCatalogNumber DNE");

        $caughtFault = false;
        try {
            $noroster = $this->ds->getClassRoster($this->data->term, '999999');
        } catch (SoapFault $e) {
            $caughtFault = true;
        }
        $this->assertTrue($caughtFault, "getClassRoster DNE");

        $caughtFault = false;
        try {
            $noinstructors = $this->ds->getInstructors($this->data->term, '999999');
        } catch (SoapFault $e) {
            $caughtFault = true;
        }
        $this->assertTrue($caughtFault, "getInstructors DNE");
    }

    public function test_getEnrolledClass() {
        $classes = $this->ds->getEnrolledClasses($this->data->term, $this->data->student_pvi);
        $this->assertTrue(count($classes) >= 0, "getEnrolledClasses");

        // Test error handling
        $noclasses = $this->ds->getEnrolledClasses($this->data->term, '0');
        $this->assertTrue(count($noclasses) == 0, "getEnrolledClasses DNE");
    }

    public function test_getClassesByInstructor() {
        $affiliations = $this->ds->getClassesByInstructor($this->data->term, $this->data->teacher_pvi);
        $this->assertTrue(count($affiliations) >= 0, "getClassesByInstructor");

        // Test error handling
        $noclasses = $this->ds->getClassesByInstructor($this->data->term, '0');
        $this->assertTrue(count($noclasses) == 0, "getClassesByInstructorId DNE");
    }
}
?>